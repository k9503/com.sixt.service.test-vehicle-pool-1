# TEST-VEHICLE-POOL Service #

Maintained by:
Raimund Traichel (ONE QA team)

The service provides test vehicles for different types of vehicles for manual and automated
testing.

The service provides the following functionality:

# VEHICLE POOL FUNCTIONS #
* Create a vehicle pool
* Update a vehicle pool
* Get all vehicle pools
* Get vehicle pool data
* Delete a vehicle pool
* Clear all vehicles from a vehicle pool
* Get SAC vehicle from the vehicle pool
* Get RAC vehicle from the vehicle pool
* Get RAC & Telematic vehicle from the vehicle pool
* Get Fastlane vehicle from the vehicle pool
* Get number of vehicles in pool (AVAILABLE / BLOCKED)

# MANUAL VEHICLE CREATE / DELETE FUNCTIONS #
* Create Fastlane test vehicles for manual testing
* Create RAC & Telematic test vehicles for manual testing
* Create SAC test vehicles for manual testing
* Create SAC vehicle simulator test vehicles for manual testing
* Delete manual test vehicles
* Lookup vehicles
* Get available SAC vehicles by location
* Add telematic unit to existing RAC vehicle

# SCHEDULER FUNCTIONS #
## Automatic (re-)creation of test vehicles in a pool for automated testing ##
* Fastlane Vehicle Creation Scheduler
* RAC Vehicle Creation Scheduler
* RAC Telematic Vehicle Creation Scheduler
* SAC Vehicle Creation Scheduler
* SAC Performance Vehicle Creation Scheduler
## Automatic cleanup of not longer used / blocked vehicles from the DB and deletion of the vehicles ##
* Vehicle Deletion Scheduler
* Vehicle Cleanup Scheduler
* RAC Vehicle Cleanup Scheduler

# TEST VEHICLE DATA #
* Vehicle type
* Static data
* Dynamic data
* Test case id
* Blocked flag
* Blocked time
* Creation time
* Vehicle pool id
* Test location

# Service Dependency Documentation #

```
ServiceDescription: "Provides vehicle pools and test vehicles for manual and automation testing
  RpcCalls:
    - Branch
    - Dynamic-vehicle-data
    - Fleet
    - Journey
    - One-fake-vehicles
    - One-vehicle-availability
    - One-vehicle-maintenance
    - Zone
  EventsConsumed:
    - NONE
  EventsPublished (via Events API):
    - FleetAssignmentStarted
    - FleetAssignmentEnded
    - FleetOpsUsageStarted
    - FleetOpsUsageEnded
    - FleetVehicleCreated
  ExternalCalls:
    - Some Sixt Classic Cobra / Fleet endpoints (from LIB)
    - Integration Fleet API
  Infrastructure:
    - PostgreSQL
```

# Service Method TestVehiclePool.CreateManSacVehicle #

## Request ##

```
{
    "test_location": "MUNICH",
    "vehicle_fuel_type": "GASOLINE",
    "keyless": true,
    "transmission": "AUTOMATIC"
}
```

## Response ##
```
{
    "vehicle_id": "5467376e-d00a-4d47-8419-81c47c8855f3",
    "vin": "FAKESCM8901234567"
}
```

# Service Method TestVehiclePool.CreateManSacSimulatorVehicle #

## Request ##

```
{
    "test_location": "MUNICH",
    "vehicle_fuel_type": "GASOLINE",
    "keyless": true,
    "transmission": "AUTOMATIC"
}
```

## Response ##
```
{
    "vehicle_id": "5467376e-d00a-4d47-8419-81c47c8855f3",
    "vin": "VSIMSAC8901234567"
}
```

# Service Method TestVehiclePool.CreateManRacTelematicVehicle #

## Request ##

```
{
    "branch_id": 1,
    "acriss_code": "LDAR",
    "vehicle_fuel_type": "GASOLINE",
    "winter_tyres": true
}
```

## Response ##
```
{
    "vehicle_id": "5467376e-d00a-4d47-8419-81c47c8855f3",
    "branch_id": 1,
    "country": "DE",
    "acriss_code": "LDAR",
    "license_plate": "M - TS 1000"
}
```

# Service Method TestVehiclePool.CreateManFastlaneVehicle #

## Request ##

```
{
    "branch_id": 1,
    "acriss_code": "LDAR",
    "vehicle_fuel_type": "GASOLINE",
    "winter_tyres": true
}
```

## Response ##
```
{
    "vehicle_id": "5467376e-d00a-4d47-8419-81c47c8855f3",
    "branch_id": 1,
    "country": "DE",
    "acriss_code": "LDAR",
    "license_plate": "M - TS 1000"
}
```

# Service Method TestVehiclePool.DeleteManVehicle #

## Request ##

```
{
    "vehicle_id": "5467376e-d00a-4d47-8419-81c47c8855f3"
}
```

## Response ##
```
{}
```

# Service Method TestVehiclePool.AddTelematicManRacVehicle #

## Request ##

```
{
    "vehicle_id": "5467376e-d00a-4d47-8419-81c47c8855f3"
}
```

## Response ##
```
{}
```

# Service Method TestVehiclePool.LookupVehicle #

## Request ##

```
{
    "vehicle_id": "5467376e-d00a-4d47-8419-81c47c8855f3",
}
```

## Response ##
```
{
    "vin": "FAKESCALM4BSZA61J",
    "vehicle_type": "SAC_AUTOMATION",
    "tc_id": "TestCase1",
    "acriss_code": "LDAR",
    "branch_id": 5,
    "vehicle_pool_id": "9089376e-d00a-4d47-8419-81c47c887844"
}
```

# Service Method TestVehiclePool.GetAvailableSacVehiclesByLocation #

## Request ##

```
{
    "test_location": "MUNICH"
}
```

## Response ##
```
{
    "available_vehicles": [
        {
            "vin": "FAKESCMNBCYVYUARM",
            "vehicle_id": "e2e682cc-0939-493f-8ef6-58f6c50165c8",
            "acriss_code": "LDAR"
        },
        {
            "vin": "FAKESCM7VAREBU5NK",
            "vehicle_id": "e2e36fe4-7895-434d-80a0-c38fa92709ad",
            "acriss_code": "LDAR"
        }
}
```

# Service Method TestVehiclePool.CreateVehiclePool #

## Request ##

```
{
    "vehicle_pool": {
        "id": "TEST-POOL-1",
        "vehicle_type": "SAC_AUTOMATION",
        "tenant": "SIXT",
        "branch_id": 0,
        "acriss_code": "LDAR",
        "pool_size": 10,
        "clean_after_days": 2
        "central_position": {
            "lat": 47.2,
            "lon": 11.2
        },
        "fuel_type": "GASOLINE"
    }
}
```

## Response ##
```
{
    "vehicle_pool": {
        "id": "TEST-POOL-1",
        "vehicle_type": "SAC_AUTOMATION",
        "tenant": "SIXT",
        "branch_id": 0,
        "acriss_code": "LDAR",
        "pool_size": 10,
        "clean_after_days": 2
        "central_position": {
            "lat": 47.2,
            "lon": 11.2
        },
        "fuel_type": "GASOLINE"
    }
}
```

# Service Method TestVehiclePool.UpdateVehiclePool #

## Request ##

```
{
    "vehicle_pool": {
        "id": "TEST-POOL-1",
        "vehicle_type": "SAC_AUTOMATION",
        "tenant": "SIXT",
        "branch_id": 0,
        "acriss_code": "LDAR",
        "pool_size": 10,
        "clean_after_days": 2
        "central_position": {
            "lat": 47.2,
            "lon": 11.2
        },
        "fuel_type": "GASOLINE"
    }
}
```

## Response ##
```
{
    "vehicle_pool": {
        "id": "TEST-POOL-1",
        "vehicle_type": "SAC_AUTOMATION",
        "tenant": "SIXT",
        "branch_id": 0,
        "acriss_code": "LDAR",
        "pool_size": 10,
        "clean_after_days": 2
        "central_position": {
            "lat": 47.2,
            "lon": 11.2
        },
        "fuel_type": "GASOLINE"
    }
}
```

# Service Method TestVehiclePool.DeleteVehiclePool #

## Request ##

```
{
    "vehicle_pool_id": "SAC_AUTOMATION_SIXT_MOBILE_APP"
}
```

## Response ##
```
{}
```

# Service Method TestVehiclePool.GetAllVehiclePools #

## Request ##

```
{}
```

## Response ##
```
{
    "vehice_pool": [
        {
        "id": "SAC_AUTOMATION_SIXT_MOBILE_APP",
        "vehicle_type": "SAC_AUTOMATION",
        "tenant": "SIXT_MOBILE_APP",
        "acriss_code": "LDAR",
        "pool_size": 50,
        "clean_after_days": 2,
        "central_position": {
            "lat": 12.930266,
            "lon": 77.69147
        },
        "fuel_type": "GASOLINE"
    ]
}
```

# Service Method TestVehiclePool.GetVehiclePool #

## Request ##

```
{
    "vehicle_pool_id": "SAC_AUTOMATION_SIXT_MOBILE_APP"
}
```

## Response ##
```
{
    "vehicle_pool": {
        "id": "SAC_AUTOMATION_SIXT_MOBILE_APP",
        "vehicle_type": "SAC_AUTOMATION",
        "tenant": "SIXT_MOBILE_APP",
        "acriss_code": "LDAR",
        "pool_size": 50,
        "clean_after_days": 2,
        "central_position": {
            "lat": 12.930266,
            "lon": 77.69147
        },
        "fuel_type": "GASOLINE"
    }
}
```

# Service Method TestVehiclePool.GetNumVehiclesInPool #

## Request ##

```
{
    "vehicle_pool_id": "SAC_AUTOMATION_SIXT",
    "vehicle_state": "AVAILABLE / BLOCKED"
}
```

## Response ##
```
{
    "num_vehicles": "190"
}
```

# Service Method TestVehiclePool.GetSacVehicleFromPool #

## Request ##

```
{
    "tc_id": "TestCase1",
    "tenant": "SIXT",
}
```

## Response ##
```
{
    "vehicle_id": "e2e43c95-a841-4924-aa2d-6bf65c650661",
    "vin": "FAKESCALM4BSZA61J"
}
```

# Service Method TestVehiclePool.GetFastlaneVehicleFromPool #

## Request ##

```
{
	"branch_id": 1,
	"acriss_code": "LDAR",
    "tc_id": "TestCase1"
}
```

## Response ##
```
{
    "vehicle_id": "e2e43c95-a841-4924-aa2d-6bf65c650661",
    "vin": "FAKEFLALM4BSZA61J",
    "internal_number": "123345678"
}
```

# Service Method TestVehiclePool.GetRacTelematicVehicleFromPool #

## Request ##

```
{
	"branch_id": 1,
	"acriss_code": "LDAR",
    "tc_id": "TestCase1"
}
```

## Response ##
```
{
    "vehicle_id": "e2e43c95-a841-4924-aa2d-6bf65c650661",
    "vin": "FAKERTALM4BSZA61J",
    "internal_number": "123345678"
}
```

# Service Method TestVehiclePool.GetRacVehicleFromPool #

## Request ##

```
{
	"branch_id": 1,
	"acriss_code": "LDAR",
    "tc_id": "TestCase1"
}
```

## Response ##
```
{
    "vehicle_id": "e2e43c95-a841-4924-aa2d-6bf65c650661",
    "vin": "FAKERTALM4BSZA61J",
    "internal_number": "123345678"
}
```

# Service Method TestVehiclePool.ClearVehiclePool #

## Request ##

```
{
	"vehicle_pool_id": "68743c95-a841-4924-aa2d-6bf65c650334"
}
```

## Response ##
```
{}
```