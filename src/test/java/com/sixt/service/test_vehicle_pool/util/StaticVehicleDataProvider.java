package com.sixt.service.test_vehicle_pool.util;

import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.test_vehicle_pool.api.VehicleFuelType;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.*;
import com.sixt.service.test_vehicle_pool.application.dto.provider.*;
import com.sixt.service.test_vehicle_pool.utils.UUIDCreator;

public class StaticVehicleDataProvider {

    public static VehicleV2 getStaticVehicleData() {
        final String color = Color.PINK.getColorCode();
        final String imageUrl = Model.VW_GOLF.getImageUrl();

        final VehicleV2.Basic vehicleBasic = VehicleBasicProvider.createBasicAutomation(VehicleType.SAC_AUTOMATION, "LDAR", VehicleFuelType.GASOLINE);
        return VehicleV2.newBuilder()
                .setVehicleId(UUIDCreator.createRandomE2EUUID())
                .setBasic(vehicleBasic)
                .setExterior(VehicleExteriorProvider.createExterior(color, imageUrl))
                .setTechnical(VehicleTechnicalSpecProvider.createTechnicalSpec(FuelType.getEnum(vehicleBasic.getFuelType()),
                        FuelType.getEnum(vehicleBasic.getFuelType2())))
                .setInterior(VehicleInteriorProvider.createDefaultInterior())
                .setRegistration(VehicleRegistrationProvider.createDefaultRegistration())
                .setContract(VehicleContractProvider.createDefaultContract())
                .setEquipment(VehicleEquipmentProvider.createEquipment(true, FuelType.getEnum(vehicleBasic.getFuelType())))
                .setInvoice(VehicleInvoiceProvider.createDefaultInvoice())
                .setLeasing(VehicleLeasingProvider.createDefaultLeasing())
                .build();
    }
}
