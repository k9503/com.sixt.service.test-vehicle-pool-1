package com.sixt.service.test_vehicle_pool.application;

import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.util.StaticVehicleDataProvider;
import com.sixt.service.test_vehicle_pool.util.TestVehicleProvider;
import okhttp3.MediaType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.shaded.org.apache.commons.lang.StringUtils;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import java.util.UUID;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {EventApi.class})
@ActiveProfiles(value = "test")
public class EventApiTest {

    @MockBean
    private HttpClientBase client;

    @MockBean
    private IdentityApi identityApi;

    @Autowired
    private EventApi eventApi;

    private static final VehicleType VEHICLE_TYPE = VehicleType.SAC_AUTOMATION;
    private static final String VEHICLE_ID = UUID.randomUUID().toString();
    private static final String VIN = "RANDOMVIN01234567";
    private static final String TOKEN = "DummyToken";
    private static final int BRANCH_ID = 0;
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;
    private static final String VEHICLE_POOL_ID = UUID.randomUUID().toString();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void publishFleetVehicleCreatedEvent_Success() throws Exception {
        VehicleV2 fleetVehicle = StaticVehicleDataProvider.getStaticVehicleData();

        when(client.post(eq("http://test-host/v1/event/publish"), any(), any(), eq(MediaType.get("application/json; charset=utf-8")))).thenReturn("");
        when(identityApi.getToken(anyString(), anyString(), anyString())).thenReturn(TOKEN);

        eventApi.publishFleetVehicleCreatedEvent(fleetVehicle);

        verify(client, times(1)).post(eq("http://test-host/v1/event/publish"), argThat(publishedEvent -> isCorrectEventPublished(publishedEvent, "FleetVehicleCreated")),
                any(), eq(MediaType.get("application/json; charset=utf-8")));
    }

    @Test
    public void publishFleetOpsUsageStartedEvent_Success() throws Exception {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);

        when(client.post(eq("http://test-host/v1/event/publish"), any(), any(), eq(MediaType.get("application/json; charset=utf-8")))).thenReturn("");
        when(identityApi.getToken(anyString(), anyString(), anyString())).thenReturn(TOKEN);

        eventApi.publishFleetOpsUsageStartedEvent(testVehicle);

        verify(client, times(1)).post(eq("http://test-host/v1/event/publish"), argThat(publishedEvent -> isCorrectEventPublished(publishedEvent, "FleetOpsUsageStarted")),
                any(), eq(MediaType.get("application/json; charset=utf-8")));
    }

    @Test
    public void publishFleetOpsUsageEndedEvent_Success() throws Exception {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);

        when(client.post(eq("http://test-host/v1/event/publish"), any(), any(), eq(MediaType.get("application/json; charset=utf-8")))).thenReturn("");
        when(identityApi.getToken(anyString(), anyString(), anyString())).thenReturn(TOKEN);

        eventApi.publishFleetOpsUsageEndedEvent(testVehicle);

        verify(client, times(1)).post(eq("http://test-host/v1/event/publish"), argThat(publishedEvent -> isCorrectEventPublished(publishedEvent, "FleetOpsUsageEnded")),
                any(), eq(MediaType.get("application/json; charset=utf-8")));
    }

    @Test
    public void publishFleetAssignmentStartedEvent_Success() throws Exception {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        String fleetAssignmentId = "Id1";

        when(client.post(eq("http://test-host/v1/event/publish"), any(), any(), eq(MediaType.get("application/json; charset=utf-8")))).thenReturn("");
        when(identityApi.getToken(anyString(), anyString(), anyString())).thenReturn(TOKEN);

        eventApi.publishFleetAssignmentStartedEvent(testVehicle, fleetAssignmentId);

        verify(client, times(1)).post(eq("http://test-host/v1/event/publish"), argThat(publishedEvent -> isCorrectEventPublished(publishedEvent, "FleetAssignmentStarted")),
                any(), eq(MediaType.get("application/json; charset=utf-8")));
    }

    @Test
    public void publishFleetAssignmentStartedDefleetEvent_Success() throws Exception {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        String fleetAssignmentId = "Id1";

        when(client.post(eq("http://test-host/v1/event/publish"), any(), any(), eq(MediaType.get("application/json; charset=utf-8")))).thenReturn("");
        when(identityApi.getToken(anyString(), anyString(), anyString())).thenReturn(TOKEN);

        eventApi.publishFleetAssignmentStartedDefleetEvent(testVehicle, fleetAssignmentId);

        verify(client, times(1)).post(eq("http://test-host/v1/event/publish"), argThat(publishedEvent -> isCorrectEventPublished(publishedEvent, "FleetAssignmentStarted")),
                any(), eq(MediaType.get("application/json; charset=utf-8")));
    }

    @Test
    public void publishFleetAssignmentEndedEvent_Success() throws Exception {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        String fleetAssignmentId = "Id1";

        when(client.post(eq("http://test-host/v1/event/publish"), any(), any(), eq(MediaType.get("application/json; charset=utf-8")))).thenReturn("");
        when(identityApi.getToken(anyString(), anyString(), anyString())).thenReturn(TOKEN);

        eventApi.publishFleetAssignmentEndedEvent(testVehicle, fleetAssignmentId);

        verify(client, times(1)).post(eq("http://test-host/v1/event/publish"), argThat(publishedEvent -> isCorrectEventPublished(publishedEvent, "FleetAssignmentEnded")),
                any(), eq(MediaType.get("application/json; charset=utf-8")));
    }

    private boolean isCorrectEventPublished(String publishedEvent, String expectedEventName) {
        return StringUtils.contains(publishedEvent, "\"name\":\"" + expectedEventName + "\"");
    }

}
