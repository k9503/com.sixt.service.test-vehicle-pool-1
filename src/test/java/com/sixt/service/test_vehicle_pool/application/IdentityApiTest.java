package com.sixt.service.test_vehicle_pool.application;

import com.google.gson.JsonObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {IdentityApi.class})
@ActiveProfiles(value = "test")
public class IdentityApiTest {

    @MockBean
    private HttpClientBase client;

    @Autowired
    private IdentityApi identityApi;

    private final String clientId = "DummyClientId";
    private final String clientSecret = "DummyClientSecret";
    private final String realm = "One";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getToken_Success() throws Exception {
        String token = "TestToken####Test";
        JsonObject jsonTokenResponse = new JsonObject();
        jsonTokenResponse.addProperty("access_token", token);
        jsonTokenResponse.addProperty("expires_in", "30000");

        when(client.post(eq("http://identity-test-host/auth/realms/One/protocol/openid-connect/token"), any(), any())).thenReturn(jsonTokenResponse.toString());

        String returnedToken = identityApi.getToken(realm, clientId, clientSecret);
        assertThat(returnedToken).isEqualTo(token);
        verify(client, times(1)).post(eq("http://identity-test-host/auth/realms/One/protocol/openid-connect/token"), any(), any());
    }

    @Test
    public void getToken_Fail_TokenBlank() throws Exception {
        String token = "";
        JsonObject jsonTokenResponse = new JsonObject();
        jsonTokenResponse.addProperty("access_token", token);
        jsonTokenResponse.addProperty("expires_in", "30000");

        when(client.post(eq("http://identity-test-host/auth/realms/One/protocol/openid-connect/token"), any(), any())).thenReturn(jsonTokenResponse.toString());

        String returnedToken = identityApi.getToken(realm, clientId, clientSecret);
        assertThat(returnedToken).isNull();
        verify(client, times(1)).post(eq("http://identity-test-host/auth/realms/One/protocol/openid-connect/token"), any(), any());
    }

    @Test
    public void getToken_Fail_Exception() throws Exception {
        when(client.post(eq("http://identity-test-host/auth/realms/One/protocol/openid-connect/token"), any(), any()))
                .thenThrow(new Exception("Couldn't connect to identity provider"));

        String returnedToken = identityApi.getToken(realm, clientId, clientSecret);
        assertThat(returnedToken).isNull();
        verify(client, times(1)).post(eq("http://identity-test-host/auth/realms/One/protocol/openid-connect/token"), any(), any());
    }

}
