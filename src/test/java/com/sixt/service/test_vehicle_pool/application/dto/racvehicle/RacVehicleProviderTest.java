package com.sixt.service.test_vehicle_pool.application.dto.racvehicle;

import com.sixt.service.test_vehicle_pool.api.VehicleFuelType;

import com.sixt.testdatamanagement.rac.vehicle.creation.config.TestEnvironment;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.Vehicle;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.aut.VehicleAut;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.de.VehicleDe;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.es.VehicleEs;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.fr.VehicleFr;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.it.VehicleIt;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.nl.VehicleNl;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.uk.VehicleUk;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.us.VehicleUs;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class RacVehicleProviderTest {

    private final String vin = "FAKE1234567890123";
    private final String licensePlate = "M-TE 1223";
    private final TestEnvironment testEnvironment = TestEnvironment.STAGE;

    @Test
    public void provideVehicle_DE_Success() {
        Vehicle vehicle = RacVehicleProvider.provideVehicle(testEnvironment, Country.DE, vin, licensePlate, AcrissCode.CLAR,
                VehicleFuelType.GASOLINE, true, false, false);
        assertThat(vehicle).isNotNull();
        VehicleDe vehicleDE = (VehicleDe) vehicle;
        assertThat(vehicleDE.getAttributes().getVin().getValue()).isEqualTo(vin);
        assertThat(vehicleDE.getAttributes().getLicencePlate().getValue()).isEqualTo(licensePlate);
        // TODO: Mapping not yet implemented in lib
        //assertThat(vehicleDE.getAttributes().getFueltype().getValue()).isEqualTo(VehicleFuelType.GASOLINE);
        assertThat(vehicleDE.getAttributes().getMetaWinterSuitableTyres().isValue()).isEqualTo(true);
        assertThat(vehicleDE.getAttributes().getDrivenWheels().getValue()).isEqualTo("F");
        assertThat(vehicleDE.getAttributes().getMetaDrawBar().isValue()).isEqualTo(false);
    }

    @Test
    public void provideVehicle_NL_Success() {
        Vehicle vehicle = RacVehicleProvider.provideVehicle(testEnvironment, Country.NL, vin, licensePlate, AcrissCode.FWAR,
                VehicleFuelType.GASOLINE, true, true, true);
        assertThat(vehicle).isNotNull();
        // TODO: Replace VehicleIT with VehicleNL when available in lib
        VehicleNl vehicleNL = (VehicleNl) vehicle;
        assertThat(vehicleNL.getAttributes().getVin().getValue()).isEqualTo(vin);
        assertThat(vehicleNL.getAttributes().getLicencePlate().getValue()).isEqualTo(licensePlate);
        assertThat(vehicleNL.getAttributes().getDrivenWheels().getValue()).isEqualTo("4");
        assertThat(vehicleNL.getAttributes().getMetaDrawBar().isValue()).isEqualTo(true);
    }

    @Test
    public void provideVehicle_IT_Success() {
        Vehicle vehicle = RacVehicleProvider.provideVehicle(testEnvironment, Country.IT, vin, licensePlate, AcrissCode.ILAR,
                VehicleFuelType.GASOLINE, true, false, false);
        assertThat(vehicle).isNotNull();
        VehicleIt vehicleIT = (VehicleIt) vehicle;
        assertThat(vehicleIT.getAttributes().getVin().getValue()).isEqualTo(vin);
        assertThat(vehicleIT.getAttributes().getLicencePlate().getValue()).isEqualTo(licensePlate);
        assertThat(vehicleIT.getAttributes().getDrivenWheels().getValue()).isEqualTo("F");
        assertThat(vehicleIT.getAttributes().getMetaDrawBar().isValue()).isEqualTo(false);
    }

    @Test
    public void provideVehicle_US_Success() {
        Vehicle vehicle = RacVehicleProvider.provideVehicle(testEnvironment, Country.US, vin, licensePlate, AcrissCode.ECAR,
                VehicleFuelType.GASOLINE, true, false, false);
        assertThat(vehicle).isNotNull();
        VehicleUs vehicleUS = (VehicleUs) vehicle;
        assertThat(vehicleUS.getAttributes().getVin().getValue()).isEqualTo(vin);
        assertThat(vehicleUS.getAttributes().getLicencePlate().getValue()).isEqualTo(licensePlate);
        // TODO: Mapping not yet implemented in lib
        //assertThat(vehicleUS.getAttributes().getFueltype().getValue()).isEqualTo(VehicleFuelType.GASOLINE);
        assertThat(vehicleUS.getAttributes().getMetaWinterSuitableTyres().isValue()).isEqualTo(true);
        assertThat(vehicleUS.getAttributes().getDrivenWheels().getValue()).isEqualTo("F");
        assertThat(vehicleUS.getAttributes().getMetaDrawBar().isValue()).isEqualTo(false);
    }

    @Test
    public void provideVehicle_ES_Success() {
        Vehicle vehicle = RacVehicleProvider.provideVehicle(testEnvironment, Country.ES, vin, licensePlate, AcrissCode.CDMR,
                VehicleFuelType.GASOLINE, true, true, true);
        assertThat(vehicle).isNotNull();
        VehicleEs vehicleES = (VehicleEs) vehicle;
        assertThat(vehicleES.getAttributes().getVin().getValue()).isEqualTo(vin);
        assertThat(vehicleES.getAttributes().getLicencePlate().getValue()).isEqualTo(licensePlate);
        assertThat(vehicleES.getAttributes().getDrivenWheels().getValue()).isEqualTo("4");
        assertThat(vehicleES.getAttributes().getMetaDrawBar().isValue()).isEqualTo(true);
    }

    @Test
    public void provideVehicle_FR_Success() {
        Vehicle vehicle = RacVehicleProvider.provideVehicle(testEnvironment, Country.FR, vin, licensePlate, AcrissCode.CDMR,
                VehicleFuelType.GASOLINE, true, true, true);
        assertThat(vehicle).isNotNull();
        VehicleFr vehicleFR = (VehicleFr) vehicle;
        assertThat(vehicleFR.getAttributes().getVin().getValue()).isEqualTo(vin);
        assertThat(vehicleFR.getAttributes().getLicencePlate().getValue()).isEqualTo(licensePlate);
        assertThat(vehicleFR.getAttributes().getDrivenWheels().getValue()).isEqualTo("4");
        // MetaDrawBar replacement for FR currently not supported in rac-vehicle-creation lib
        //assertThat(vehicleFR.getAttributes().getMetaDrawBar().isValue()).isEqualTo(true);
    }

    @Test
    public void provideVehicle_UK_Success() {
        Vehicle vehicle = RacVehicleProvider.provideVehicle(testEnvironment, Country.UK, vin, licensePlate, AcrissCode.CDMR,
                VehicleFuelType.GASOLINE, true, true, true);
        assertThat(vehicle).isNotNull();
        VehicleUk vehicleUK = (VehicleUk) vehicle;
        assertThat(vehicleUK.getAttributes().getVin().getValue()).isEqualTo(vin);
        assertThat(vehicleUK.getAttributes().getLicencePlate().getValue()).isEqualTo(licensePlate);
        assertThat(vehicleUK.getAttributes().getDrivenWheels().getValue()).isEqualTo("4");
    }

    @Test
    public void provideVehicle_AT_Success() {
        Vehicle vehicle = RacVehicleProvider.provideVehicle(testEnvironment, Country.AT, vin, licensePlate, AcrissCode.CDMR,
                VehicleFuelType.GASOLINE, true, true, true);
        assertThat(vehicle).isNotNull();
        VehicleAut vehicleAT = (VehicleAut) vehicle;
        assertThat(vehicleAT.getAttributes().getVin().getValue()).isEqualTo(vin);
        assertThat(vehicleAT.getAttributes().getLicencePlate().getValue()).isEqualTo(licensePlate);
        assertThat(vehicleAT.getAttributes().getDrivenWheels().getValue()).isEqualTo("4");
    }

}