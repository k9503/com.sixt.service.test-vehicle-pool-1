package com.sixt.service.test_vehicle_pool.application;

import okhttp3.MediaType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {IntegrationFleetApi.class})
@ActiveProfiles(value = "test")
public class IntegrationFleetApiTest {

    @MockBean
    private HttpClientBaseTlsV1 client;

    @Autowired
    private IntegrationFleetApi integrationFleetApi;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addTelematicUnit_Success() throws Exception {
        final String vehicleId = UUID.randomUUID().toString();
        final String vin = "RANDOMVIN01234567";

        when(client.post(eq("http://test-host/fleet/vehicleApi/v1/test/vehicle/vin/" + vin + "/setTelematicStatus"), any(), any(), eq(MediaType.get("application/json; charset=utf-8")))).thenReturn("");

        integrationFleetApi.addTelematicUnit(vehicleId, vin);

        verify(client, times(1)).post(eq("http://test-host/fleet/vehicleApi/v1/test/vehicle/vin/" + vin + "/setTelematicStatus"), any(), any(), eq(MediaType.get("application/json; charset=utf-8")));
    }

}
