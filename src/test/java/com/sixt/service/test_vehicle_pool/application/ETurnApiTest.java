package com.sixt.service.test_vehicle_pool.application;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {EturnApi.class})
@ActiveProfiles(value = "test")
public class ETurnApiTest {

    @MockBean
    private HttpClientBaseTlsV1 client;

    @Autowired
    private EturnApi eturnApi;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void eturnCheckIn_Success() throws Exception {
        String vehicleId = UUID.randomUUID().toString();
        String licensePlate = "M-TST 1234";

        eturnApi.checkIn(vehicleId, licensePlate);
        verify(client, times(1)).getQueryParams(eq("https://test-host/php/eturnws/movement.create"), any(), any());
    }

}
