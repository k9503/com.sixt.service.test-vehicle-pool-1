package com.sixt.service.test_vehicle_pool.application;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.sixt.service.branch.api.BranchOuterClass;
import com.sixt.service.salesboost_branch_vehicle_availability.api.Dynamic;
import com.sixt.service.salesboost_branch_vehicle_availability.api.VehicleDetailResponse;
import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.application.dto.Transmission;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.BasicRacVehicle;
import com.sixt.service.test_vehicle_pool.domain.*;
import com.sixt.service.test_vehicle_pool.domain.vehiclecreation.FastlaneVehicleCreator;
import com.sixt.service.test_vehicle_pool.domain.vehiclecreation.RacTelematicVehicleCreator;
import com.sixt.service.test_vehicle_pool.domain.vehiclecreation.SacVehicleCreator;
import com.sixt.service.test_vehicle_pool.domain.vehicledeletion.VehicleCleaner;
import com.sixt.service.test_vehicle_pool.infrastructure.SalesboostBranchVehicleAvailabilityServiceProxy;
import com.sixt.service.test_vehicle_pool.infrastructure.db.VehiclePOJPARepository;
import com.sixt.service.test_vehicle_pool.infrastructure.db.VehiclePoolPOJPARepository;
import com.sixt.service.test_vehicle_pool.util.TestVehicleProvider;
import com.sixt.service.test_vehicle_pool.util.VehiclePoolProvider;
import com.sixt.service.test_vehicle_pool.utils.Sleeper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.mockito.Mockito.*;


public class TestVehiclePoolServiceTest {

    @Mock
    private SacVehicleCreator sacVehicleCreator;

    @Mock
    private RacTelematicVehicleCreator racTelematicVehicleCreator;

    @Mock
    private FastlaneVehicleCreator fastlaneVehicleCreator;

    @Mock
    private VehicleCleaner vehicleCleaner;

    @Mock
    private VehiclePOJPARepository vehiclePORepository;

    @Mock
    private VehiclePoolPOJPARepository vehiclePoolPORepository;

    @Mock
    private TestVehicleDataCollector testVehicleDataCollector;

    @Mock
    private SalesboostBranchVehicleAvailabilityServiceProxy salesboostBranchVehicleAvailabilityServiceProxy;

    private ExecutorService executor = Executors.newFixedThreadPool(5, new ThreadFactoryBuilder().setNameFormat("async-executor-%d").build());

    private TestVehiclePoolService testVehiclePoolService;

    private static final VehicleType VEHICLE_TYPE = VehicleType.SAC_AUTOMATION;
    private static final String VEHICLE_ID = UUID.randomUUID().toString();
    private static final String VIN = "RANDOMVIN01234567";
    private static final String LICENSE_PLATE = "M- TS 1000";
    private static final int BRANCH_ID = 11;
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;
    private static final String VEHICLE_POOL_ID = UUID.randomUUID().toString();
    private static final Tenant TENANT = Tenant.SIXT;
    private static final Transmission TRANSMISSION = Transmission.AUTOMATIC;
    private static final VehicleFuelType VEHICLE_FUEL_TYPE = VehicleFuelType.GASOLINE;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        testVehiclePoolService = new TestVehiclePoolService(sacVehicleCreator, racTelematicVehicleCreator,
                fastlaneVehicleCreator, vehicleCleaner, vehiclePORepository, vehiclePoolPORepository, testVehicleDataCollector,
                salesboostBranchVehicleAvailabilityServiceProxy, executor);
    }

    @Test
    public void getVehicleFromPool_BranchAcriss_Rac_Success() {
        VehicleType vehicleType = VehicleType.RAC_AUTOMATION;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, vehicleType, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        String testCaseId = "TestCaseGetFromPool";

        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);

        when(vehiclePORepository.findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                BRANCH_ID, ACRISS_CODE.name())).thenReturn(Optional.of(vehiclePO));
        when(testVehicleDataCollector.getCurrentTestVehicleStaticData(VEHICLE_ID)).thenReturn(testVehicle);

        Dynamic dynamic = Dynamic.newBuilder()
                .setVehicleAvailabilityStatus("AVAILABLE").build();
        VehicleDetailResponse vehicleDetailResponse = VehicleDetailResponse.newBuilder()
                .setDynamic(dynamic).build();
        when(salesboostBranchVehicleAvailabilityServiceProxy.getStatusByVehicleId(VEHICLE_ID)).thenReturn(vehicleDetailResponse);

        TestVehicle testUserFromPool = testVehiclePoolService.getVehicleFromPool(testVehicle.getVehicleType(), BRANCH_ID,
                ACRISS_CODE, testCaseId);

        verify(vehiclePORepository, times(1)).findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                BRANCH_ID, ACRISS_CODE.name());
        verify(vehiclePORepository, times(1)).save(vehiclePO);
        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleStaticData(VEHICLE_ID);
        verify(salesboostBranchVehicleAvailabilityServiceProxy, times(1)).getStatusByVehicleId(VEHICLE_ID);
        assertThat(testUserFromPool.getStaticVehicleData().getVehicleId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void getVehicleFromPool_BranchAcriss_Fastlane_Success() {
        VehicleType vehicleType = VehicleType.FASTLANE_AUTOMATION;
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, vehicleType, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        String testCaseId = "TestCaseGetFromPool";

        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);

        when(vehiclePORepository.findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                BRANCH_ID, ACRISS_CODE.name())).thenReturn(Optional.of(vehiclePO));
        when(testVehicleDataCollector.getCurrentTestVehicleStaticData(VEHICLE_ID)).thenReturn(testVehicle);

        TestVehicle testUserFromPool = testVehiclePoolService.getVehicleFromPool(testVehicle.getVehicleType(), BRANCH_ID,
                ACRISS_CODE, testCaseId);

        verify(vehiclePORepository, times(1)).findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                BRANCH_ID, ACRISS_CODE.name());
        verify(vehiclePORepository, times(1)).save(vehiclePO);
        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleStaticData(VEHICLE_ID);
        verify(salesboostBranchVehicleAvailabilityServiceProxy, never()).getStatusByVehicleId(VEHICLE_ID);
        assertThat(testUserFromPool.getStaticVehicleData().getVehicleId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void getVehicleFromPool_BranchAcriss_Rac_NoVehicleFound() {
        VehicleType vehicleType = VehicleType.RAC_AUTOMATION;
        String testCaseId = "TestCaseGetFromPool";

        when(vehiclePORepository.findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                BRANCH_ID, ACRISS_CODE.name())).thenReturn(Optional.empty());

        OutOfTestVehicleException exception = catchThrowableOfType(
                () -> testVehiclePoolService.getVehicleFromPool(vehicleType, BRANCH_ID, ACRISS_CODE, testCaseId),
                OutOfTestVehicleException.class);
        assertThat(exception).isNotNull();

        verify(vehiclePORepository, times(1)).findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                BRANCH_ID, ACRISS_CODE.name());
        verify(vehiclePORepository, never()).save(any());
        verify(salesboostBranchVehicleAvailabilityServiceProxy, never()).getStatusByVehicleId(any());
        verify(testVehicleDataCollector, never()).getCurrentTestVehicleStaticData(VEHICLE_ID);
    }

    @Test
    public void getVehicleFromPool_BranchAcriss_Fastlane_NoVehicleFound() {
        VehicleType vehicleType = VehicleType.FASTLANE_AUTOMATION;
        String testCaseId = "TestCaseGetFromPool";

        when(vehiclePORepository.findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                BRANCH_ID, ACRISS_CODE.name())).thenReturn(Optional.empty());

        OutOfTestVehicleException exception = catchThrowableOfType(
                () -> testVehiclePoolService.getVehicleFromPool(vehicleType, BRANCH_ID, ACRISS_CODE, testCaseId),
                OutOfTestVehicleException.class);
        assertThat(exception).isNotNull();

        verify(vehiclePORepository, times(1)).findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                BRANCH_ID, ACRISS_CODE.name());
        verify(vehiclePORepository, never()).save(any());
        verify(salesboostBranchVehicleAvailabilityServiceProxy, never()).getStatusByVehicleId(any());
        verify(testVehicleDataCollector, never()).getCurrentTestVehicleData(VEHICLE_ID);
    }

    @Test
    public void getVehicleFromPool_BranchAcriss_Rac_FirstFoundVehicleNotAvailableInClassic() {
        VehicleType vehicleType = VehicleType.RAC_AUTOMATION;
        String testCaseId = "TestCaseGetFromPool";

        VehiclePO vehiclePO1 = VehiclePO.createNew(VEHICLE_ID);

        String vehicleId2 = UUID.randomUUID().toString();
        String vin2 = "TESTVIN2";
        TestVehicle testVehicle2 = TestVehicleProvider.getTestVehicle(vehicleId2, vin2, vehicleType, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        VehiclePO vehiclePO2 = VehiclePO.createNew(vehicleId2);

        when(vehiclePORepository.findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                BRANCH_ID, ACRISS_CODE.name())).thenReturn(Optional.of(vehiclePO1)).thenReturn(Optional.of(vehiclePO2));
        when(testVehicleDataCollector.getCurrentTestVehicleStaticData(vehicleId2)).thenReturn(testVehicle2);

        Dynamic dynamic1 = Dynamic.newBuilder()
                .setVehicleAvailabilityStatus("NOT_AVAILABLE").build();
        VehicleDetailResponse vehicleDetailResponse1 = VehicleDetailResponse.newBuilder()
                .setDynamic(dynamic1).build();
        when(salesboostBranchVehicleAvailabilityServiceProxy.getStatusByVehicleId(VEHICLE_ID)).thenReturn(vehicleDetailResponse1);

        Dynamic dynamic2 = Dynamic.newBuilder()
                .setVehicleAvailabilityStatus("AVAILABLE").build();
        VehicleDetailResponse vehicleDetailResponse2 = VehicleDetailResponse.newBuilder()
                .setDynamic(dynamic2).build();
        when(salesboostBranchVehicleAvailabilityServiceProxy.getStatusByVehicleId(vehicleId2)).thenReturn(vehicleDetailResponse2);

        TestVehicle testUserFromPool = testVehiclePoolService.getVehicleFromPool(vehicleType, BRANCH_ID,
                ACRISS_CODE, testCaseId);

        verify(vehiclePORepository, times(2)).findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                BRANCH_ID, ACRISS_CODE.name());
        verify(vehiclePORepository, times(1)).save(vehiclePO2);
        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleStaticData(vehicleId2);
        verify(salesboostBranchVehicleAvailabilityServiceProxy, times(2)).getStatusByVehicleId(any());
        assertThat(testUserFromPool.getStaticVehicleData().getVehicleId()).isEqualTo(vehicleId2);
    }

    @Test
    public void getVehicleFromPool_BranchAcriss_Rac_NoVehicleAvailableInClassic() {
        VehicleType vehicleType = VehicleType.RAC_AUTOMATION;
        String testCaseId = "TestCaseGetFromPool";

        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);

        when(vehiclePORepository.findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                BRANCH_ID, ACRISS_CODE.name())).thenReturn(Optional.of(vehiclePO)).thenReturn(Optional.empty());

        Dynamic dynamic = Dynamic.newBuilder()
                .setVehicleAvailabilityStatus("NOT_AVAILABLE").build();
        VehicleDetailResponse vehicleDetailResponse = VehicleDetailResponse.newBuilder()
                .setDynamic(dynamic).build();
        when(salesboostBranchVehicleAvailabilityServiceProxy.getStatusByVehicleId(VEHICLE_ID)).thenReturn(vehicleDetailResponse);

        OutOfTestVehicleException exception = catchThrowableOfType(
                () -> testVehiclePoolService.getVehicleFromPool(vehicleType, BRANCH_ID, ACRISS_CODE, testCaseId),
                OutOfTestVehicleException.class);
        assertThat(exception).isNotNull();

        verify(vehiclePORepository, times(2)).findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(),
                BRANCH_ID, ACRISS_CODE.name());
        verify(vehiclePORepository, times(1)).save(any());
        verify(testVehicleDataCollector, never()).getCurrentTestVehicleStaticData(any());
        verify(salesboostBranchVehicleAvailabilityServiceProxy, times(1)).getStatusByVehicleId(any());
    }

    @Test
    public void getVehicleFromPool_Success() {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(TENANT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);
        VehiclePoolPO vehiclePoolPO = VehiclePoolPO.createNew(vehiclePool.getId());
        vehiclePoolPO.newPool(vehiclePool);

        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                vehiclePool.getId());
        String testCaseId = "TestCaseGetFromPool";
        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);

        when(vehiclePoolPORepository.findByTenantAndVehicleType(TENANT.name(), VEHICLE_TYPE.name())).thenReturn(Optional.of(vehiclePoolPO));
        when(vehiclePORepository.findFirstByVehicleTypeAndVehiclePoolIdAndBlockedFalseOrderByCreationTimeAsc(VEHICLE_TYPE.name(), vehiclePool.getId()))
                .thenReturn(Optional.of(vehiclePO));
        when(testVehicleDataCollector.getCurrentTestVehicleStaticData(VEHICLE_ID)).thenReturn(testVehicle);

        TestVehicle testUserFromPool = testVehiclePoolService.getVehicleFromPool(testVehicle.getVehicleType(), testCaseId, TENANT);

        verify(vehiclePoolPORepository, times(1)).findByTenantAndVehicleType(TENANT.name(), VEHICLE_TYPE.name());
        verify(vehiclePORepository, times(1)).save(vehiclePO);
        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleStaticData(VEHICLE_ID);
        assertThat(testUserFromPool.getStaticVehicleData().getVehicleId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void getVehicleFromPool_NoVehicleFound() {
        VehicleType vehicleType = VehicleType.SAC_AUTOMATION;
        String testCaseId = "TestCaseGetFromPool";

        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(TENANT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);
        VehiclePoolPO vehiclePoolPO = VehiclePoolPO.createNew(vehiclePool.getId());
        vehiclePoolPO.newPool(vehiclePool);

        when(vehiclePoolPORepository.findByTenantAndVehicleType(TENANT.name(), vehicleType.name())).thenReturn(Optional.of(vehiclePoolPO));
        when(vehiclePORepository.findFirstByVehicleTypeAndVehiclePoolIdAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(), vehiclePool.getId()))
                .thenReturn(Optional.empty());

        OutOfTestVehicleException exception = catchThrowableOfType(
                () -> testVehiclePoolService.getVehicleFromPool(vehicleType, testCaseId, TENANT),
                OutOfTestVehicleException.class);
        assertThat(exception).isNotNull();

        verify(vehiclePoolPORepository, times(1)).findByTenantAndVehicleType(TENANT.name(), vehicleType.name());
        verify(vehiclePORepository, times(1)).findFirstByVehicleTypeAndVehiclePoolIdAndBlockedFalseOrderByCreationTimeAsc(vehicleType.name(), vehiclePool.getId());
        verify(vehiclePORepository, never()).save(any());
        verify(testVehicleDataCollector, never()).getCurrentTestVehicleStaticData(any());
    }

    @Test
    public void getVehicleFromPool_PoolForTenantNotFound() {
        VehicleType vehicleType = VehicleType.SAC_AUTOMATION;
        String testCaseId = "TestCaseGetFromPool";

        when(vehiclePoolPORepository.findByTenantAndVehicleType(TENANT.name(), vehicleType.name())).thenReturn(Optional.empty());

        UndefinedTenantException exception = catchThrowableOfType(
                () -> testVehiclePoolService.getVehicleFromPool(vehicleType, testCaseId, TENANT),
                UndefinedTenantException.class);
        assertThat(exception).isNotNull();

        verify(vehiclePoolPORepository, times(1)).findByTenantAndVehicleType(TENANT.name(), vehicleType.name());
        verify(vehiclePORepository, never()).findFirstByVehicleTypeAndVehiclePoolIdAndBlockedFalseOrderByCreationTimeAsc(anyString(), anyString());
        verify(vehiclePORepository, never()).save(any());
        verify(testVehicleDataCollector, never()).getCurrentTestVehicleStaticData(any());
    }

    @Test
    public void getNumAvailableVehiclesInPool_Success() {
        long availVehicles = 10;
        when(vehiclePORepository.countByVehiclePoolIdAndBlockedFalse(VEHICLE_POOL_ID)).thenReturn(availVehicles);

        long numAvailableVehiclesInPool = testVehiclePoolService.getNumAvailableVehiclesInPool(VEHICLE_POOL_ID);

        verify(vehiclePORepository, times(1)).countByVehiclePoolIdAndBlockedFalse(VEHICLE_POOL_ID);
        assertThat(numAvailableVehiclesInPool).isEqualTo(availVehicles);
    }

    @Test
    public void getNumBlockedVehiclesInPool_Success() {
        long blockedVehicles = 10;
        when(vehiclePORepository.countByVehiclePoolIdAndBlockedTrue(VEHICLE_POOL_ID)).thenReturn(blockedVehicles);

        long numBlockedVehiclesInPool = testVehiclePoolService.getNumBlockedVehiclesInPool(VEHICLE_POOL_ID);

        verify(vehiclePORepository, times(1)).countByVehiclePoolIdAndBlockedTrue(VEHICLE_POOL_ID);
        assertThat(numBlockedVehiclesInPool).isEqualTo(blockedVehicles);
    }

    @Test
    public void lookupVehicleFromPool_Success() {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);

        when(vehiclePORepository.findById(VEHICLE_ID)).thenReturn(Optional.of(vehiclePO));
        when(testVehicleDataCollector.getCurrentTestVehicleData(VEHICLE_ID)).thenReturn(testVehicle);

        TestVehicle testVehicleFromPool = testVehiclePoolService.lookupVehicleFromPool(VEHICLE_ID);

        verify(vehiclePORepository, times(1)).findById(VEHICLE_ID);
        verify(testVehicleDataCollector, times(1)).getCurrentTestVehicleData(VEHICLE_ID);
        assertThat(testVehicleFromPool.getStaticVehicleData().getVehicleId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void lookupVehicleFromPool_VehicleNotFound() {
        String vehicleId = "NotFoundVehicleId";
        when(vehiclePORepository.findById(vehicleId)).thenReturn(Optional.empty());

        TestVehicle testVehicleFromPool = testVehiclePoolService.lookupVehicleFromPool(vehicleId);

        verify(vehiclePORepository, times(1)).findById(vehicleId);
        verify(testVehicleDataCollector, never()).getCurrentTestVehicleData(vehicleId);

        assertThat(testVehicleFromPool).isNull();
    }

    @Test
    public void asyncCreateManSacVehicle_Success() {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        testVehicle.setCreationTime(Instant.now());
        TestLocation testLocation = TestLocation.MUNICH;
        VehicleFuelType vehicleFuelType = VehicleFuelType.GASOLINE;
        boolean isKeyless = true;

        when(sacVehicleCreator.createSacVehicleManual(VEHICLE_TYPE, VEHICLE_ID, VIN, testLocation, vehicleFuelType, isKeyless, TRANSMISSION)).thenReturn(testVehicle);

        CompletableFuture completableFuture = testVehiclePoolService.asyncCreateManSacVehicle(VEHICLE_TYPE, VEHICLE_ID, VIN, testLocation, vehicleFuelType, isKeyless, TRANSMISSION);
        waitUntilAsyncCallDone(completableFuture);

        verify(sacVehicleCreator, times(1)).createSacVehicleManual(VEHICLE_TYPE, VEHICLE_ID, VIN, testLocation, vehicleFuelType, isKeyless, TRANSMISSION);
        verify(vehiclePORepository, times(1)).save(any(VehiclePO.class));
    }

    @Test
    public void asyncCreateManSacVehicle_VehicleNotCreated() {
        TestLocation testLocation = TestLocation.MUNICH;
        VehicleFuelType vehicleFuelType = VehicleFuelType.GASOLINE;
        boolean isKeyless = true;

        when(sacVehicleCreator.createSacVehicleManual(VEHICLE_TYPE, VEHICLE_ID, VIN, testLocation, vehicleFuelType, isKeyless, TRANSMISSION)).thenReturn(null);

        CompletableFuture completableFuture = testVehiclePoolService.asyncCreateManSacVehicle(VEHICLE_TYPE, VEHICLE_ID, VIN, testLocation, vehicleFuelType, isKeyless, TRANSMISSION);
        waitUntilAsyncCallDone(completableFuture);

        Sleeper.sleepForSeconds(1);

        verify(sacVehicleCreator, times(1)).createSacVehicleManual(VEHICLE_TYPE, VEHICLE_ID, VIN, testLocation, vehicleFuelType, isKeyless, TRANSMISSION);
        verify(vehiclePORepository, never()).save(any(VehiclePO.class));
    }

    @Test
    public void asyncCreateManRacTelematicVehicle_Success() {
        VehicleType vehicleType = VehicleType.RAC_TELEMATIC_AUTOMATION;

        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, vehicleType, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        testVehicle.setCreationTime(Instant.now());
        VehicleFuelType vehicleFuelType = VehicleFuelType.GASOLINE;
        boolean withWinterTyres = true;

        BasicRacVehicle basicRacVehicle = new BasicRacVehicle(VEHICLE_ID, 1, BranchOuterClass.BranchObject.newBuilder().build(), LICENSE_PLATE);

        when(racTelematicVehicleCreator.createRacTelematicVehicleManualBasic(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres)).thenReturn(basicRacVehicle);
        when(racTelematicVehicleCreator.finishCreateRacTelematicVehicleManual(basicRacVehicle)).thenReturn(testVehicle);

        BasicRacVehicle createdBasicRacVehicle = testVehiclePoolService.asyncCreateManRacTelematicVehicle(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres);
        assertThat(createdBasicRacVehicle.getVehicleId()).isEqualTo(VEHICLE_ID);
        assertThat(createdBasicRacVehicle.getLicensePlate()).isEqualTo(LICENSE_PLATE);

        Sleeper.sleepForSeconds(2);

        verify(racTelematicVehicleCreator, times(1)).createRacTelematicVehicleManualBasic(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres);
        verify(racTelematicVehicleCreator, times(1)).finishCreateRacTelematicVehicleManual(basicRacVehicle);
        verify(vehiclePORepository, times(1)).save(any(VehiclePO.class));
    }

    @Test
    public void asyncCreateManRacTelematicVehicle_BasicRacVehicleNotCreated() {
        VehicleFuelType vehicleFuelType = VehicleFuelType.GASOLINE;
        boolean withWinterTyres = true;

        when(racTelematicVehicleCreator.createRacTelematicVehicleManualBasic(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres)).thenReturn(null);

        BasicRacVehicle createdBasicRacVehicle = testVehiclePoolService.asyncCreateManRacTelematicVehicle(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres);
        assertThat(createdBasicRacVehicle).isNull();

        verify(racTelematicVehicleCreator, times(1)).createRacTelematicVehicleManualBasic(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres);
        verify(racTelematicVehicleCreator, never()).finishCreateRacTelematicVehicleManual(any());
        verify(vehiclePORepository, never()).save(any(VehiclePO.class));
    }

    @Test
    public void asyncCreateManRacTelematicVehicle_FinishCreationRacVehicleFailed() {
        VehicleFuelType vehicleFuelType = VehicleFuelType.GASOLINE;
        boolean withWinterTyres = true;

        BasicRacVehicle basicRacVehicle = new BasicRacVehicle(VEHICLE_ID, 1, BranchOuterClass.BranchObject.newBuilder().build(), LICENSE_PLATE);

        when(racTelematicVehicleCreator.createRacTelematicVehicleManualBasic(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres)).thenReturn(basicRacVehicle);
        when(racTelematicVehicleCreator.finishCreateRacTelematicVehicleManual(basicRacVehicle)).thenReturn(null);

        BasicRacVehicle createdBasicRacVehicle = testVehiclePoolService.asyncCreateManRacTelematicVehicle(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres);
        assertThat(createdBasicRacVehicle.getVehicleId()).isEqualTo(VEHICLE_ID);
        assertThat(createdBasicRacVehicle.getLicensePlate()).isEqualTo(LICENSE_PLATE);

        Sleeper.sleepForSeconds(2);

        verify(racTelematicVehicleCreator, times(1)).createRacTelematicVehicleManualBasic(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres);
        verify(racTelematicVehicleCreator, times(1)).finishCreateRacTelematicVehicleManual(basicRacVehicle);
        verify(vehiclePORepository, never()).save(any(VehiclePO.class));
    }

    @Test
    public void asyncCreateManFastlaneVehicle_Success() {
        VehicleType vehicleType = VehicleType.FASTLANE_AUTOMATION;

        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, vehicleType, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        testVehicle.setCreationTime(Instant.now());
        VehicleFuelType vehicleFuelType = VehicleFuelType.GASOLINE;
        boolean withWinterTyres = true;
        boolean fourWheels = true;
        boolean trailerCoupling = true;

        BasicRacVehicle basicRacVehicle = new BasicRacVehicle(VEHICLE_ID, 1, BranchOuterClass.BranchObject.newBuilder().build(), LICENSE_PLATE);

        when(fastlaneVehicleCreator.createFastlaneVehicleManualBasic(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres,
                fourWheels, trailerCoupling)).thenReturn(basicRacVehicle);
        when(fastlaneVehicleCreator.finishCreateFastlaneVehicleManual(basicRacVehicle)).thenReturn(testVehicle);

        BasicRacVehicle createdBasicRacVehicle = testVehiclePoolService.asyncCreateManFastlaneVehicle(BRANCH_ID, ACRISS_CODE, vehicleFuelType,
                withWinterTyres, fourWheels, trailerCoupling);
        assertThat(createdBasicRacVehicle.getVehicleId()).isEqualTo(VEHICLE_ID);
        assertThat(createdBasicRacVehicle.getLicensePlate()).isEqualTo(LICENSE_PLATE);

        Sleeper.sleepForSeconds(2);

        verify(fastlaneVehicleCreator, times(1)).createFastlaneVehicleManualBasic(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres,
                fourWheels, trailerCoupling);
        verify(fastlaneVehicleCreator, times(1)).finishCreateFastlaneVehicleManual(basicRacVehicle);
        verify(vehiclePORepository, times(1)).save(any(VehiclePO.class));
    }

    @Test
    public void asyncCreateManFastlaneVehicle_BasicRacVehicleNotCreated() {
        VehicleFuelType vehicleFuelType = VehicleFuelType.GASOLINE;
        boolean withWinterTyres = true;
        boolean fourWheels = true;
        boolean trailerCoupling = true;

        when(fastlaneVehicleCreator.createFastlaneVehicleManualBasic(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres,
                fourWheels, trailerCoupling)).thenReturn(null);

        BasicRacVehicle createdBasicRacVehicle = testVehiclePoolService.asyncCreateManFastlaneVehicle(BRANCH_ID, ACRISS_CODE, vehicleFuelType,
                withWinterTyres, fourWheels, trailerCoupling);
        assertThat(createdBasicRacVehicle).isNull();

        verify(fastlaneVehicleCreator, times(1)).createFastlaneVehicleManualBasic(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres,
                fourWheels, trailerCoupling);
        verify(fastlaneVehicleCreator, never()).finishCreateFastlaneVehicleManual(any());
        verify(vehiclePORepository, never()).save(any(VehiclePO.class));
    }


    @Test
    public void asyncCreateManFastlaneVehicle_FinishCreationRacVehicleFailed() {
        VehicleFuelType vehicleFuelType = VehicleFuelType.GASOLINE;
        boolean withWinterTyres = true;
        boolean fourWheels = true;
        boolean trailerCoupling = true;

        BasicRacVehicle basicRacVehicle = new BasicRacVehicle(VEHICLE_ID, 1, BranchOuterClass.BranchObject.newBuilder().build(), LICENSE_PLATE);

        when(fastlaneVehicleCreator.createFastlaneVehicleManualBasic(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres,
                fourWheels, trailerCoupling)).thenReturn(basicRacVehicle);
        when(fastlaneVehicleCreator.finishCreateFastlaneVehicleManual(basicRacVehicle)).thenReturn(null);

        BasicRacVehicle createdBasicRacVehicle = testVehiclePoolService.asyncCreateManFastlaneVehicle(BRANCH_ID, ACRISS_CODE, vehicleFuelType,
                withWinterTyres, fourWheels, trailerCoupling);
        assertThat(createdBasicRacVehicle.getVehicleId()).isEqualTo(VEHICLE_ID);
        assertThat(createdBasicRacVehicle.getLicensePlate()).isEqualTo(LICENSE_PLATE);

        Sleeper.sleepForSeconds(2);

        verify(fastlaneVehicleCreator, times(1)).createFastlaneVehicleManualBasic(BRANCH_ID, ACRISS_CODE, vehicleFuelType, withWinterTyres,
                fourWheels, trailerCoupling);
        verify(fastlaneVehicleCreator, times(1)).finishCreateFastlaneVehicleManual(basicRacVehicle);
        verify(vehiclePORepository, never()).save(any(VehiclePO.class));
    }

    @Test
    public void deleteManualVehicle_Success() {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);
        vehiclePO.newVehicle(testVehicle);

        when(vehiclePORepository.findById(VEHICLE_ID)).thenReturn(Optional.of(vehiclePO));
        when(vehicleCleaner.deleteVehicle(VEHICLE_TYPE, VEHICLE_ID)).thenReturn(true);

        boolean successDelete = testVehiclePoolService.deleteManualVehicle(VEHICLE_ID);

        assertThat(successDelete).isTrue();
        verify(vehiclePORepository, times(1)).findById(VEHICLE_ID);
        verify(vehicleCleaner, times(1)).deleteVehicle(VEHICLE_TYPE, VEHICLE_ID);
        verify(vehiclePORepository, times(1)).deleteById(VEHICLE_ID);
    }

    @Test
    public void deleteManualVehicle_VehicleDeletionFailed() {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);
        vehiclePO.newVehicle(testVehicle);

        when(vehiclePORepository.findById(VEHICLE_ID)).thenReturn(Optional.of(vehiclePO));
        when(vehicleCleaner.deleteVehicle(VEHICLE_TYPE, VEHICLE_ID)).thenReturn(false);

        boolean successDelete = testVehiclePoolService.deleteManualVehicle(VEHICLE_ID);

        assertThat(successDelete).isFalse();
        verify(vehiclePORepository, times(1)).findById(VEHICLE_ID);
        verify(vehicleCleaner, times(1)).deleteVehicle(VEHICLE_TYPE, VEHICLE_ID);
        verify(vehiclePORepository, never()).deleteById(any());
    }

    @Test
    public void deleteManualVehicle_VehicleAlreadyDeleted() {
        when(vehiclePORepository.findById(VEHICLE_ID)).thenReturn(Optional.empty());

        boolean successDelete = testVehiclePoolService.deleteManualVehicle(VEHICLE_ID);

        assertThat(successDelete).isTrue();

        verify(vehiclePORepository, times(1)).findById(VEHICLE_ID);
        verify(vehicleCleaner, never()).deleteVehicle(any(), any());
        verify(vehiclePORepository, never()).delete(any());
    }

    @Test
    public void createVehiclePool_Success() {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(Tenant.SIXT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);

        String vehiclePoolId = testVehiclePoolService.createVehiclePool(vehiclePool);

        assertThat(vehiclePoolId).isNotNull();
        verify(vehiclePoolPORepository, times(1)).save(any(VehiclePoolPO.class));
    }

    @Test
    public void updateVehiclePool_Success() {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(Tenant.SIXT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);
        VehiclePoolPO vehiclePoolPO = VehiclePoolPO.createNew(vehiclePool.getId());
        vehiclePoolPO.newPool(vehiclePool);

        Optional<VehiclePoolPO> vehiclePoolPOOptional = Optional.of(vehiclePoolPO);
        when(vehiclePoolPORepository.findById(vehiclePool.getId())).thenReturn(vehiclePoolPOOptional);

        VehiclePool updateVehiclePool = vehiclePool.toBuilder()
                .setPoolSize(10).build();
        String vehiclePoolId = testVehiclePoolService.updateVehiclePool(updateVehiclePool);

        assertThat(vehiclePoolId).isEqualTo(vehiclePool.getId());
        verify(vehiclePoolPORepository, times(1)).save(any(VehiclePoolPO.class));
    }

    @Test
    public void updateVehiclePool_PoolNotFound() {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(Tenant.SIXT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);
        when(vehiclePoolPORepository.findById(vehiclePool.getId())).thenReturn(Optional.empty());

        VehiclePool updateVehiclePool = vehiclePool.toBuilder()
                .setPoolSize(10).build();
        String vehiclePoolId = testVehiclePoolService.updateVehiclePool(updateVehiclePool);

        assertThat(vehiclePoolId).isNull();
        verify(vehiclePoolPORepository, never()).save(any(VehiclePoolPO.class));
    }

    @Test
    public void getAllVehiclePools_Success() {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(Tenant.SIXT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);

        VehiclePoolPO vehiclePoolPO = VehiclePoolPO.createNew(vehiclePool.getId());
        vehiclePoolPO.newPool(vehiclePool);

        List<VehiclePoolPO> vehiclePoolPOList = new ArrayList<>();
        vehiclePoolPOList.add(vehiclePoolPO);
        when(vehiclePoolPORepository.findAll()).thenReturn(vehiclePoolPOList);

        List<VehiclePool> vehiclePools = testVehiclePoolService.getAllVehiclePools();

        assertThat(vehiclePools).isNotEmpty();
        assertThat(vehiclePools.get(0)).isEqualTo(vehiclePool);
        verify(vehiclePoolPORepository, times(1)).findAll();
    }

    @Test
    public void getAllVehiclePools_NoPool() {
        List<VehiclePoolPO> vehiclePoolPOList = new ArrayList<>();
        when(vehiclePoolPORepository.findAll()).thenReturn(vehiclePoolPOList);

        List<VehiclePool> vehiclePools = testVehiclePoolService.getAllVehiclePools();

        assertThat(vehiclePools).isEmpty();
        verify(vehiclePoolPORepository, times(1)).findAll();
    }

    @Test
    public void getVehiclePool_Success() {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(Tenant.SIXT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);

        VehiclePoolPO vehiclePoolPO = VehiclePoolPO.createNew(vehiclePool.getId());
        vehiclePoolPO.newPool(vehiclePool);

        Optional<VehiclePoolPO> vehiclePoolPOOptional = Optional.of(vehiclePoolPO);
        when(vehiclePoolPORepository.findById(vehiclePool.getId())).thenReturn(vehiclePoolPOOptional);

        VehiclePool foundVehiclePool = testVehiclePoolService.getVehiclePool(vehiclePool.getId());

        assertThat(foundVehiclePool).isNotNull();
        assertThat(foundVehiclePool).isEqualTo(vehiclePool);
        verify(vehiclePoolPORepository, times(1)).findById(vehiclePool.getId());
    }

    @Test
    public void getVehiclePool_PoolNotFound() {
        String vehiclePoolId = "NotExistingPoolId";
        when(vehiclePoolPORepository.findById(vehiclePoolId)).thenReturn(Optional.empty());

        VehiclePool foundVehiclePool = testVehiclePoolService.getVehiclePool(vehiclePoolId);

        assertThat(foundVehiclePool).isNull();
        verify(vehiclePoolPORepository, times(1)).findById(vehiclePoolId);
    }

    @Test
    public void deleteVehiclePool_Success() {
        VehiclePool vehiclePool = VehiclePoolProvider.getVehiclePool(Tenant.SIXT, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE);

        VehiclePoolPO vehiclePoolPO = VehiclePoolPO.createNew(vehiclePool.getId());
        vehiclePoolPO.newPool(vehiclePool);

        Optional<VehiclePoolPO> vehiclePoolPOOptional = Optional.of(vehiclePoolPO);
        when(vehiclePoolPORepository.findById(vehiclePool.getId())).thenReturn(vehiclePoolPOOptional);

        testVehiclePoolService.deleteVehiclePool(vehiclePool.getId());

        verify(vehiclePoolPORepository, times(1)).findById(vehiclePool.getId());
        verify(vehiclePoolPORepository, times(1)).deleteById(vehiclePool.getId());
    }

    @Test
    public void deleteVehiclePool_AlreadyDeleted() {
        String vehiclePoolId = "NotExistingPoolId";
        when(vehiclePoolPORepository.findById(vehiclePoolId)).thenReturn(Optional.empty());

        testVehiclePoolService.deleteVehiclePool(vehiclePoolId);

        verify(vehiclePoolPORepository, times(1)).findById(vehiclePoolId);
        verify(vehiclePoolPORepository, never()).deleteById(vehiclePoolId);
    }

    @Test
    public void deleteVehiclesFromPool_Success() {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);
        vehiclePO.newVehicle(testVehicle);

        List<VehiclePO> vehiclePOList = new ArrayList<>();
        vehiclePOList.add(vehiclePO);

        when(vehiclePORepository.findByVehiclePoolId(VEHICLE_POOL_ID)).thenReturn(vehiclePOList);
        when(vehicleCleaner.deleteVehicle(VEHICLE_TYPE, VEHICLE_ID)).thenReturn(true);

        testVehiclePoolService.deleteVehiclesFromPool(VEHICLE_POOL_ID);

        verify(vehiclePORepository, times(1)).findByVehiclePoolId(VEHICLE_POOL_ID);
        verify(vehicleCleaner, times(1)).deleteVehicle(VEHICLE_TYPE, VEHICLE_ID);
        verify(vehiclePORepository, times(1)).delete(vehiclePO);
    }

    @Test
    public void deleteVehiclesFromPool_CleanupFailed() {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);
        vehiclePO.newVehicle(testVehicle);

        List<VehiclePO> vehiclePOList = new ArrayList<>();
        vehiclePOList.add(vehiclePO);

        when(vehiclePORepository.findByVehiclePoolId(VEHICLE_POOL_ID)).thenReturn(vehiclePOList);
        when(vehicleCleaner.deleteVehicle(VEHICLE_TYPE, VEHICLE_ID)).thenReturn(false);

        testVehiclePoolService.deleteVehiclesFromPool(VEHICLE_POOL_ID);

        verify(vehiclePORepository, times(1)).findByVehiclePoolId(VEHICLE_POOL_ID);
        verify(vehicleCleaner, times(1)).deleteVehicle(VEHICLE_TYPE, VEHICLE_ID);
        verify(vehiclePORepository, never()).delete(vehiclePO);
    }

    @Test
    public void deleteVehiclesFromPool_NoVehicleFound() {
        List<VehiclePO> vehiclePOList = new ArrayList<>();

        when(vehiclePORepository.findByVehiclePoolId(VEHICLE_POOL_ID)).thenReturn(vehiclePOList);
        when(vehicleCleaner.deleteVehicle(VEHICLE_TYPE, VEHICLE_ID)).thenReturn(false);

        testVehiclePoolService.deleteVehiclesFromPool(VEHICLE_POOL_ID);

        verify(vehiclePORepository, times(1)).findByVehiclePoolId(VEHICLE_POOL_ID);
        verify(vehicleCleaner, never()).deleteVehicle(VEHICLE_TYPE, VEHICLE_ID);
        verify(vehiclePORepository, never()).delete(any());
    }

    @Test
    public void clearVehiclePool_Success() {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);
        vehiclePO.newVehicle(testVehicle);
        List<VehiclePO> vehiclePOList = new ArrayList<>();
        vehiclePOList.add(vehiclePO);

        when(vehiclePORepository.findByVehiclePoolId(VEHICLE_POOL_ID)).thenReturn(vehiclePOList);

        testVehiclePoolService.clearVehiclePool(VEHICLE_POOL_ID);

        verify(vehiclePORepository, times(1)).findByVehiclePoolId(VEHICLE_POOL_ID);
        verify(vehiclePORepository, times(1)).save(vehiclePO);
    }

    @Test
    public void getAvailableSacVehiclesByLocation_Success() {
        TestLocation testLocation = TestLocation.MUNICH;
        TestVehicle testVehicleMan = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VehicleType.SAC_MANUAL, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        VehiclePO vehiclePOMan = VehiclePO.createNew(VEHICLE_ID);
        vehiclePOMan.newVehicle(testVehicleMan);
        List<VehiclePO> vehiclePOManList = new ArrayList<>();
        vehiclePOManList.add(vehiclePOMan);
        when(vehiclePORepository.findByVehicleTypeAndTestLocationAndBlockedFalse(VehicleType.SAC_MANUAL.name(), testLocation.name()))
                .thenReturn(vehiclePOManList);

        String vehicleIdSimVehicle = UUID.randomUUID().toString();
        TestVehicle testVehicleSim = TestVehicleProvider.getTestVehicle(vehicleIdSimVehicle, VIN, VehicleType.SAC_SIMULATOR, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        VehiclePO vehiclePOSim = VehiclePO.createNew(vehicleIdSimVehicle);
        vehiclePOSim.newVehicle(testVehicleSim);
        List<VehiclePO> vehiclePOSimList = new ArrayList<>();
        vehiclePOSimList.add(vehiclePOSim);
        when(vehiclePORepository.findByVehicleTypeAndTestLocationAndBlockedFalse(VehicleType.SAC_SIMULATOR.name(), testLocation.name()))
                .thenReturn(vehiclePOSimList);

        List<AvailableVehicle> availableVehicles = testVehiclePoolService.getAvailableSacVehiclesByLocation(testLocation);

        verify(vehiclePORepository, times(1)).findByVehicleTypeAndTestLocationAndBlockedFalse(VehicleType.SAC_MANUAL.name(), testLocation.name());
        verify(vehiclePORepository, times(1)).findByVehicleTypeAndTestLocationAndBlockedFalse(VehicleType.SAC_SIMULATOR.name(), testLocation.name());

        assertThat(availableVehicles.size()).isEqualTo(2);
    }

    private void waitUntilAsyncCallDone(CompletableFuture completableFuture) {
        while (true) {
            if (completableFuture.isDone()) {
                break;
            }
        }
    }

}
