package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.Transmission;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({CreateManSacSimulatorVehicleHandler.class})
public class CreateManSacSimulatorVehicleHandlerTest {

    @Autowired
    private CreateManSacSimulatorVehicleHandler handler;

    @MockBean
    private TestVehiclePoolService service;

    private static final VehicleType VEHICLE_TYPE = VehicleType.SAC_SIMULATOR;
    private static final TestLocation TEST_LOCATION = TestLocation.MUNICH;
    private static final VehicleFuelType VEHICLE_FUEL_TYPE = VehicleFuelType.GASOLINE;
    private static final boolean IS_KEYLESS = true;
    private static final Transmission TRANSMISSION = Transmission.AUTOMATIC;


    @Test
    public void handleRequest_Success() throws RpcCallException {
        CreateManSacSimulatorVehicleRequest request = CreateManSacSimulatorVehicleRequest.newBuilder()
                .setTestLocation(TEST_LOCATION)
                .setVehicleFuelType(VEHICLE_FUEL_TYPE)
                .setKeyless(IS_KEYLESS)
                .setTransmission(TRANSMISSION.name())
                .build();

        CreateManSacSimulatorVehicleResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response.getVehicleId()).isNotNull();
        assertThat(response.getVin()).isNotNull();

        verify(service, times(1)).asyncCreateManSacVehicle(eq(VEHICLE_TYPE), anyString(), anyString(), eq(TEST_LOCATION),
                eq(VEHICLE_FUEL_TYPE), eq(IS_KEYLESS), eq(TRANSMISSION));
    }

    @Test
    public void handleRequest_Fail_InvalidTransmission() {
        CreateManSacSimulatorVehicleRequest request = CreateManSacSimulatorVehicleRequest.newBuilder()
                .setTestLocation(TEST_LOCATION)
                .setVehicleFuelType(VEHICLE_FUEL_TYPE)
                .setKeyless(IS_KEYLESS)
                .setTransmission("InvalidTransmission")
                .build();

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);

        verifyZeroInteractions(service);
    }
}
