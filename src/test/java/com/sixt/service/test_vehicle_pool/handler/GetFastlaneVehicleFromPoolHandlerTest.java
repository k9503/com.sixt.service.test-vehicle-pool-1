package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.domain.OutOfTestVehicleException;
import com.sixt.service.test_vehicle_pool.util.TestVehicleProvider;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import java.util.UUID;

import static com.sixt.service.test_vehicle_pool.api.GetFastlaneVehicleFromPoolResponse.Error.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({GetFastlaneVehicleFromPoolHandler.class})
public class GetFastlaneVehicleFromPoolHandlerTest {

    @Autowired
    private GetFastlaneVehicleFromPoolHandler handler;

    @MockBean
    private TestVehiclePoolService service;

    private static final VehicleType VEHICLE_TYPE = VehicleType.FASTLANE_AUTOMATION;
    private static final String VEHICLE_ID = UUID.randomUUID().toString();
    private static final String VIN = "RANDOMVIN01234567";
    private static final int BRANCH_ID = 11;
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;
    private static final String VEHICLE_POOL_ID = UUID.randomUUID().toString();

    @Test
    public void handleRequest_Success() throws Exception {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        String testCaseId = "TestCaseIdSuccess";

        GetFastlaneVehicleFromPoolRequest request = GetFastlaneVehicleFromPoolRequest.newBuilder()
                .setBranchId(BRANCH_ID)
                .setAcrissCode(ACRISS_CODE.name())
                .setTcId(testCaseId)
                .build();

        when(service.getVehicleFromPool(eq(VEHICLE_TYPE), eq(BRANCH_ID), eq(ACRISS_CODE), eq(testCaseId)))
                .thenReturn(testVehicle);

        GetFastlaneVehicleFromPoolResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response.getVehicleId()).isEqualTo(testVehicle.getStaticVehicleData().getVehicleId());
        assertThat(response.getVin()).isEqualTo(testVehicle.getStaticVehicleData().getBasic().getVin());
        assertThat(response.getInternalNumber()).isEqualTo(testVehicle.getStaticVehicleData().getBasic().getInternalNumber());

        verify(service, times(1)).getVehicleFromPool(VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, testCaseId);
    }

    @Test
    public void handleRequest_Fail_RequestNull() {
        GetFastlaneVehicleFromPoolRequest request = null;

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(BAD_REQUEST.name());

        verifyZeroInteractions(service);
    }

    @Test
    public void handleRequest_Fail_InvalidBranchId() {
        int branchId = 0;
        String testCaseId = "TestCaseIdSuccess";

        GetFastlaneVehicleFromPoolRequest request = GetFastlaneVehicleFromPoolRequest.newBuilder()
                .setBranchId(branchId)
                .setAcrissCode(ACRISS_CODE.name())
                .setTcId(testCaseId)
                .build();

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(INVALID_BRANCH_ID.name());

        verifyZeroInteractions(service);
    }

    @Test
    public void handleRequest_Fail_InvalidAcrissCode() {
        String acrissCode = "Invalid";
        String testCaseId = "TestCaseIdSuccess";

        GetFastlaneVehicleFromPoolRequest request = GetFastlaneVehicleFromPoolRequest.newBuilder()
                .setBranchId(BRANCH_ID)
                .setAcrissCode(acrissCode)
                .setTcId(testCaseId)
                .build();

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(INVALID_ACRISS_CODE.name());

        verifyZeroInteractions(service);
    }

    @Test
    public void handleRequest_Fail_InvalidTestCaseId() {
        String testCaseId = "";

        GetFastlaneVehicleFromPoolRequest request = GetFastlaneVehicleFromPoolRequest.newBuilder()
                .setBranchId(BRANCH_ID)
                .setAcrissCode(ACRISS_CODE.name())
                .setTcId(testCaseId)
                .build();

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.BadRequest);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(UNDEFINED_TCID.name());

        verifyZeroInteractions(service);
    }

    @Test
    public void handleRequest_Fail_NoVehicleInPool() {
        String testCaseId = "TestCaseNoVehicleInPool";

        GetFastlaneVehicleFromPoolRequest request = GetFastlaneVehicleFromPoolRequest.newBuilder()
                .setBranchId(BRANCH_ID)
                .setAcrissCode(ACRISS_CODE.name())
                .setTcId(testCaseId)
                .build();

        doThrow(new OutOfTestVehicleException()).when(service).getVehicleFromPool(VEHICLE_TYPE, BRANCH_ID, AcrissCode.CLMR, testCaseId);

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.ResourceNotFound);
        assertThat(rpcCallException.getErrorCode()).isEqualTo(OUT_OF_TEST_VEHICLES.name());

        verify(service, times(1)).getVehicleFromPool(VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, testCaseId);
    }

    @Test
    public void handleRequest_Fail_TestVehicleNull() {
        String testCaseId = "TestCaseNoVehicleInPool";

        GetFastlaneVehicleFromPoolRequest request = GetFastlaneVehicleFromPoolRequest.newBuilder()
                .setBranchId(BRANCH_ID)
                .setAcrissCode(ACRISS_CODE.name())
                .setTcId(testCaseId)
                .build();

        when(service.getVehicleFromPool(eq(VEHICLE_TYPE), eq(BRANCH_ID), eq(ACRISS_CODE), eq(testCaseId)))
                .thenReturn(null);

        RpcCallException rpcCallException = Assertions.catchThrowableOfType(
                () -> handler.handleRequest(request, new OrangeContext()), RpcCallException.class);
        assertThat(rpcCallException.getCategory()).isEqualTo(RpcCallException.Category.InternalServerError);

        verify(service, times(1)).getVehicleFromPool(VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE, testCaseId);
    }

}
