package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.GetNumVehiclesInPoolRequest;
import com.sixt.service.test_vehicle_pool.api.GetNumVehiclesInPoolResponse;
import com.sixt.service.test_vehicle_pool.api.VehicleState;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({GetNumVehiclesInPoolHandler.class})
public class GetNumVehiclesInPoolHandlerTest {

    @Autowired
    private GetNumVehiclesInPoolHandler handler;

    @MockBean
    private TestVehiclePoolService service;

    private static final String VEHICLE_POOL_ID = UUID.randomUUID().toString();

    @Test
    public void handleRequest_VehicleStateAvailable_Success() {
        long numAvailableVehicles = 10;
        GetNumVehiclesInPoolRequest request = GetNumVehiclesInPoolRequest.newBuilder()
                .setVehiclePoolId(VEHICLE_POOL_ID)
                .setVehicleState(VehicleState.AVAILABLE)
                .build();

        when(service.getNumAvailableVehiclesInPool(eq(VEHICLE_POOL_ID)))
                .thenReturn(numAvailableVehicles);

        GetNumVehiclesInPoolResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response.getNumVehicles()).isEqualTo(numAvailableVehicles);

        verify(service, times(1)).getNumAvailableVehiclesInPool(VEHICLE_POOL_ID);
        verify(service, never()).getNumBlockedVehiclesInPool(VEHICLE_POOL_ID);
    }

    @Test
    public void handleRequest_VehicleStateBlocked_Success() {
        long numBlockedVehicles = 10;
        GetNumVehiclesInPoolRequest request = GetNumVehiclesInPoolRequest.newBuilder()
                .setVehiclePoolId(VEHICLE_POOL_ID)
                .setVehicleState(VehicleState.BLOCKED)
                .build();

        when(service.getNumBlockedVehiclesInPool(eq(VEHICLE_POOL_ID)))
                .thenReturn(numBlockedVehicles);

        GetNumVehiclesInPoolResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response.getNumVehicles()).isEqualTo(numBlockedVehicles);

        verify(service, times(1)).getNumBlockedVehiclesInPool(VEHICLE_POOL_ID);
        verify(service, never()).getNumAvailableVehiclesInPool(VEHICLE_POOL_ID);
    }

}
