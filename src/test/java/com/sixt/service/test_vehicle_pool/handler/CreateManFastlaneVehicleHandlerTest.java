package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.branch.api.BranchAddress;
import com.sixt.service.branch.api.BranchOuterClass;
import com.sixt.service.test_vehicle_pool.api.CreateManFastlaneVehicleRequest;
import com.sixt.service.test_vehicle_pool.api.CreateManFastlaneVehicleResponse;
import com.sixt.service.test_vehicle_pool.api.VehicleFuelType;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.BasicRacVehicle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({CreateManFastlaneVehicleHandler.class})
public class CreateManFastlaneVehicleHandlerTest {

    @Autowired
    private CreateManFastlaneVehicleHandler handler;

    @MockBean
    private TestVehiclePoolService service;

    private static final VehicleFuelType VEHICLE_FUEL_TYPE = VehicleFuelType.GASOLINE;
    private static final String LICENSE_PLATE = "M- TS 1000";
    private static final int BRANCH_ID = 11;
    private static final String COUNTRY = "DE";
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;
    private static final boolean WITH_WINTER_TYRES = true;
    private static final boolean FOUR_WHEELS = true;
    private static final boolean TRAILER_COUPLING = true;

    @Test
    public void handleRequest_Success() {
        String vehicleId = UUID.randomUUID().toString();
        BranchAddress.Country country = BranchAddress.Country.newBuilder()
                .setIso2Code(COUNTRY).build();
        BranchAddress.Address address = BranchAddress.Address.newBuilder()
                .setCountry(country).build();
        BranchOuterClass.BranchObject branchObject = BranchOuterClass.BranchObject.newBuilder()
                .addAddresses(address)
                .build();
        BasicRacVehicle basicRacVehicle = new BasicRacVehicle(vehicleId, 1, branchObject, LICENSE_PLATE);

        when(service.asyncCreateManFastlaneVehicle(BRANCH_ID, ACRISS_CODE, VEHICLE_FUEL_TYPE, WITH_WINTER_TYRES,
                FOUR_WHEELS, TRAILER_COUPLING))
                .thenReturn(basicRacVehicle);

        CreateManFastlaneVehicleRequest request = CreateManFastlaneVehicleRequest.newBuilder()
                .setBranchId(BRANCH_ID)
                .setAcrissCode(ACRISS_CODE.name())
                .setVehicleFuelType(VEHICLE_FUEL_TYPE)
                .setWinterTyres(WITH_WINTER_TYRES)
                .setFourWheels(FOUR_WHEELS)
                .setTrailerCoupling(TRAILER_COUPLING)
                .build();

        CreateManFastlaneVehicleResponse response = handler.handleRequest(request, new OrangeContext());
        assertThat(response.getVehicleId()).isEqualTo(vehicleId);
        assertThat(response.getBranchId()).isEqualTo(BRANCH_ID);
        assertThat(response.getAcrissCode()).isEqualTo(ACRISS_CODE.name());
        assertThat(response.getCountry()).isEqualTo(COUNTRY);
        assertThat(response.getLicensePlate()).isEqualTo(LICENSE_PLATE);

        verify(service, times(1)).asyncCreateManFastlaneVehicle(eq(BRANCH_ID), eq(ACRISS_CODE), eq(VEHICLE_FUEL_TYPE),
                eq(WITH_WINTER_TYRES), eq(FOUR_WHEELS), eq(TRAILER_COUPLING));
    }

}
