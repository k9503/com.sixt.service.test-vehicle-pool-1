package com.sixt.service.test_vehicle_pool.infrastructure;


import com.sixt.service.salesboost_branch_vehicle_availability.api.Dynamic;
import com.sixt.service.salesboost_branch_vehicle_availability.api.GetVehicleByVehicleIDRequest;
import com.sixt.service.salesboost_branch_vehicle_availability.api.VehicleDetailResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class SalesboostBranchVehicleAvailabilityServiceProxyTest {

    @Mock
    private RpcClient<VehicleDetailResponse> getStatusByVehicleIdRpcClient;

    private SalesboostBranchVehicleAvailabilityServiceProxy serviceProxy;

    private String vehicleId;

    private String VEHICLE_AVAILABILITY_STATUS = "AVAILABLE";
    private String VEHICLE_AVAILABILITY_SUB_STATUS = "FULLY_AVAILABLE";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        serviceProxy = new SalesboostBranchVehicleAvailabilityServiceProxy(getStatusByVehicleIdRpcClient);
        vehicleId = UUID.randomUUID().toString();
    }

    @Test
    public void getStatusByVehicleId_Success() throws RpcCallException {
        Dynamic dynamic = Dynamic.newBuilder().
                setVehicleAvailabilityStatus(VEHICLE_AVAILABILITY_STATUS).
                setVehicleAvailabilitySubStatus(VEHICLE_AVAILABILITY_SUB_STATUS).build();
        VehicleDetailResponse vehicleDetailResponse = VehicleDetailResponse.newBuilder().
                setVehicleId(vehicleId).
                setDynamic(dynamic).build();

        when(getStatusByVehicleIdRpcClient.callSynchronous(any(GetVehicleByVehicleIDRequest.class), any(OrangeContext.class))).thenReturn(vehicleDetailResponse);

        VehicleDetailResponse returnedVehicleDetailResponse = serviceProxy.getStatusByVehicleId(vehicleId);
        assertThat(returnedVehicleDetailResponse).isNotNull();
        verify(getStatusByVehicleIdRpcClient, times(1))
                .callSynchronous(any(GetVehicleByVehicleIDRequest.class), any(OrangeContext.class));
    }

    @Test
    public void getStatusByVehicleId_Fail_RpcException() throws RpcCallException {
        when(getStatusByVehicleIdRpcClient.callSynchronous(any(GetVehicleByVehicleIDRequest.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "Couldn't find service"));

        VehicleDetailResponse vehicleDetailResponse = serviceProxy.getStatusByVehicleId(vehicleId);
        assertThat(vehicleDetailResponse).isNull();
        verify(getStatusByVehicleIdRpcClient, times(1))
                .callSynchronous(any(GetVehicleByVehicleIDRequest.class), any(OrangeContext.class));
    }

}
