package com.sixt.service.test_vehicle_pool.infrastructure;


import com.sixt.service.one_fake_vehicles.api.*;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.util.TestVehicleProvider;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;


import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class OneFakeVehiclesServiceProxyTest {

    @Mock
    private RpcClient<GetVehicleResponse> getVehicleByVinRpcClient;

    @Mock
    private RpcClient<ChangeVehicleStateResponse> changeVehicleStateRpcClient;

    private OneFakeVehiclesServiceProxy serviceProxy;

    private String vin;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        serviceProxy = new OneFakeVehiclesServiceProxy(getVehicleByVinRpcClient, changeVehicleStateRpcClient);
        vin = "TESTVIN";
    }

    @Test
    public void getByVehicleByVin_Success() throws RpcCallException {
        Vehicle vehicle = Vehicle.newBuilder().
                setVin(vin).build();
        GetVehicleResponse getVehicleResponse = GetVehicleResponse.newBuilder().
                setVehicle(vehicle).build();
        when(getVehicleByVinRpcClient.callSynchronous(any(GetVehicleByVinQuery.class), any(OrangeContext.class))).thenReturn(getVehicleResponse);

        Vehicle returnedVehicle = serviceProxy.getVehicleByVin(vin);
        assertThat(returnedVehicle).isNotNull();
        assertThat(returnedVehicle.getVin()).isEqualTo(vin);
        verify(getVehicleByVinRpcClient, times(1))
                .callSynchronous(any(GetVehicleByVinQuery.class), any(OrangeContext.class));
    }

    @Test
    public void getByVehicleByVin_Fail_RpcException() throws RpcCallException {
        when(getVehicleByVinRpcClient.callSynchronous(any(GetVehicleByVinQuery.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "Couldn't find service"));

        Vehicle returnedVehicle = serviceProxy.getVehicleByVin(vin);
        assertThat(returnedVehicle).isNull();
        verify(getVehicleByVinRpcClient, times(1))
                .callSynchronous(any(GetVehicleByVinQuery.class), any(OrangeContext.class));
    }

    @Test
    public void setState_Success() throws RpcCallException {
        String vehicleId = UUID.randomUUID().toString();
        String vehiclePoolId = UUID.randomUUID().toString();
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, vin, VehicleType.SAC_AUTOMATION, 0, AcrissCode.CLMR,
                vehiclePoolId);

        VehicleState vehicleState = VehicleState.newBuilder().
                setIgnition(Ignition.valueOf(testVehicle.getDynamicVehicleData().getIgnitionState())).build();

        Vehicle vehicle = Vehicle.newBuilder().
                setVin(testVehicle.getStaticVehicleData().getBasic().getVin()).
                setVehicleState(vehicleState).build();

        GetVehicleResponse getVehicleResponse = GetVehicleResponse.newBuilder().
                setVehicle(vehicle).build();
        when(getVehicleByVinRpcClient.callSynchronous(any(GetVehicleByVinQuery.class), any(OrangeContext.class))).thenReturn(getVehicleResponse);

        serviceProxy.setState(testVehicle);

        verify(changeVehicleStateRpcClient, times(1)).callSynchronous((ChangeVehicleStateCommand) argThat(request ->
                verifyChangeVehicleStateCommand((ChangeVehicleStateCommand) request, testVehicle)), any());
    }

    private boolean verifyChangeVehicleStateCommand(ChangeVehicleStateCommand request, TestVehicle testVehicle) {
        if (!request.getVin().equals(testVehicle.getStaticVehicleData().getBasic().getVin())) {
            return false;
        }

        if (request.getSkipFireEvent()) {
            return false;
        }

        if (!request.getVehicleState().getIgnition().equals(Ignition.valueOf(testVehicle.getDynamicVehicleData().getIgnitionState()))) {
            return false;
        }

        return true;
    }

}
