package com.sixt.service.test_vehicle_pool.infrastructure;


import com.sixt.service.fleet.api.*;
import com.sixt.service.test_vehicle_pool.application.dto.FleetName;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class FleetServiceProxyTest {

    @Mock
    private RpcClient<GetVehicleResponseV2> getByVehicleIdV2RpcClient;
    @Mock
    private RpcClient<MoveToFleetResponse> moveToFleetRpcClient;
    @Mock
    private RpcClient<DeleteVehicleResponse> deleteVehicleRpcClient;

    private FleetServiceProxy serviceProxy;

    private String vehicleId;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        serviceProxy = new FleetServiceProxy(getByVehicleIdV2RpcClient, moveToFleetRpcClient, deleteVehicleRpcClient);
        vehicleId = UUID.randomUUID().toString();
    }

    @Test
    public void getByVehicleIdV2_Success() throws RpcCallException {
        VehicleV2 vehicleV2 = VehicleV2.newBuilder().
                setVehicleId(vehicleId).build();
        GetVehicleResponseV2 getVehicleResponseV2 = GetVehicleResponseV2.newBuilder().
                setVehicle(vehicleV2).build();
        when(getByVehicleIdV2RpcClient.callSynchronous(any(GetByVehicleIdQuery.class), any(OrangeContext.class))).thenReturn(getVehicleResponseV2);

        VehicleV2 returnedVehicleV2 = serviceProxy.getByVehicleIdV2(vehicleId);
        assertThat(returnedVehicleV2).isNotNull();
        assertThat(returnedVehicleV2.getVehicleId()).isEqualTo(vehicleId);
        verify(getByVehicleIdV2RpcClient, times(1))
                .callSynchronous(any(GetByVehicleIdQuery.class), any(OrangeContext.class));
    }

    @Test
    public void getByVehicleIdV2_Fail_RpcException() throws RpcCallException {
        when(getByVehicleIdV2RpcClient.callSynchronous(any(GetByVehicleIdQuery.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "Couldn't find service"));

        VehicleV2 returnedVehicleV2 = serviceProxy.getByVehicleIdV2(vehicleId);
        assertThat(returnedVehicleV2).isNull();
        verify(getByVehicleIdV2RpcClient, times(1))
                .callSynchronous(any(GetByVehicleIdQuery.class), any(OrangeContext.class));
    }

    @Test
    public void moveToFleet_Success() throws RpcCallException {
        MoveToFleetResponse moveToFleetResponse = MoveToFleetResponse.getDefaultInstance();
        when(moveToFleetRpcClient.callSynchronous(any(MoveToFleetCommand.class), any(OrangeContext.class))).thenReturn(moveToFleetResponse);

        boolean moveToFleetStatus = serviceProxy.moveToFleet(vehicleId, FleetName.ONE);
        assertThat(moveToFleetStatus).isTrue();
        verify(moveToFleetRpcClient, times(1))
                .callSynchronous(any(MoveToFleetCommand.class), any(OrangeContext.class));
    }

    @Test
    public void moveToFleet_Fail_RpcException() throws RpcCallException {
        when(moveToFleetRpcClient.callSynchronous(any(MoveToFleetCommand.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "Couldn't find service"));

        boolean moveToFleetStatus = serviceProxy.moveToFleet(vehicleId, FleetName.ONE);
        assertThat(moveToFleetStatus).isFalse();
        verify(moveToFleetRpcClient, times(1))
                .callSynchronous(any(MoveToFleetCommand.class), any(OrangeContext.class));
    }

    @Test
    public void deleteVehicle_Success() throws RpcCallException {
        DeleteVehicleResponse deleteVehicleResponse = DeleteVehicleResponse.getDefaultInstance();
        when(deleteVehicleRpcClient.callSynchronous(any(DeleteVehicleCommand.class), any(OrangeContext.class))).thenReturn(deleteVehicleResponse);

        boolean deleteSuccess = serviceProxy.deleteVehicle(vehicleId);
        assertThat(deleteSuccess).isTrue();
        verify(deleteVehicleRpcClient, times(1))
                .callSynchronous(any(DeleteVehicleCommand.class), any(OrangeContext.class));
    }

    @Test
    public void deleteVehicle_Fail_RpcException() throws RpcCallException {
        when(deleteVehicleRpcClient.callSynchronous(any(DeleteVehicleCommand.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "Couldn't find service"));

        boolean deleteSuccess = serviceProxy.deleteVehicle(vehicleId);
        assertThat(deleteSuccess).isFalse();
        verify(deleteVehicleRpcClient, times(1))
                .callSynchronous(any(DeleteVehicleCommand.class), any(OrangeContext.class));
    }

}
