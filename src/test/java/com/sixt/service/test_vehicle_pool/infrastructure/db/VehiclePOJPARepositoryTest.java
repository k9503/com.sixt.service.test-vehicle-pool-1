package com.sixt.service.test_vehicle_pool.infrastructure.db;

import com.sixt.service.test_vehicle_pool.api.TestLocation;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.domain.VehiclePO;
import com.sixt.service.test_vehicle_pool.util.TestVehicleProvider;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest()
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
@ContextConfiguration(initializers = {VehiclePOJPARepositoryTest.Initializer.class})
public class VehiclePOJPARepositoryTest {

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer();

    private static final VehicleType VEHICLE_TYPE = VehicleType.SAC_AUTOMATION;
    private static final String VEHICLE_ID = UUID.randomUUID().toString();
    private static final String VIN = "RANDOMVIN01234567";
    private static final int BRANCH_ID = 11;
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;
    private static final String VEHICLE_POOL_ID = UUID.randomUUID().toString();

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(applicationContext.getEnvironment());
        }
    }

    @Autowired
    private VehiclePOJPARepository vehiclePOJPARepository;

    @Before
    public void beforeTest() {
        vehiclePOJPARepository.deleteAll();
    }

    @Test
    public void addNewVehicleAndSave_Success() {
        VehiclePO vehiclePO = createDefaultVehiclePO(VEHICLE_ID);

        assertThat(vehiclePO.getVehicleType()).isEqualTo(VEHICLE_TYPE);
        assertThat(vehiclePO.getId()).isEqualTo(VEHICLE_ID);
        assertThat(vehiclePO.getAcrissCode()).isEqualTo(ACRISS_CODE.name());
        assertThat(vehiclePO.getBlockedTime()).isNull();
        assertThat(vehiclePO.getCreationTime()).isNotNull();
        assertThat(vehiclePO.getBranchId()).isEqualTo(BRANCH_ID);
        assertThat(vehiclePO.getVin()).isEqualTo(VIN);
        assertThat(vehiclePO.getTcId()).isEqualTo("");
        assertThat(vehiclePO.isBlocked()).isFalse();
        assertThat(vehiclePO.getVehiclePoolId()).isEqualTo(VEHICLE_POOL_ID);
    }

    @Test
    public void blockVehicle_Success() {
        VehiclePO vehiclePO = createDefaultVehiclePO(VEHICLE_ID);

        assertThat(vehiclePO.isBlocked()).isFalse();
        assertThat(vehiclePO.getBlockedTime()).isNull();
        assertThat(vehiclePO.getTcId()).isEmpty();

        // Block vehicle and save
        String testCaseId = "TestCaseId";
        vehiclePO.block(testCaseId);
        vehiclePOJPARepository.save(vehiclePO);

        Optional<VehiclePO> updatedVehiclePOOptional = vehiclePOJPARepository.findById(VEHICLE_ID);

        assertThat(updatedVehiclePOOptional.isPresent()).isTrue();
        VehiclePO updatedVehiclePO = updatedVehiclePOOptional.get();

        assertThat(updatedVehiclePO.isBlocked()).isTrue();
        assertThat(updatedVehiclePO.getBlockedTime()).isNotNull();
        assertThat(updatedVehiclePO.getTcId()).isEqualTo(testCaseId);
    }

    @Test
    public void setDeleted_Success() {
        VehiclePO vehiclePO = createDefaultVehiclePO(VEHICLE_ID);

        assertThat(vehiclePO.isBlocked()).isFalse();
        assertThat(vehiclePO.isDeleted()).isFalse();

        // Set vehicle deleted and save
        vehiclePO.setDeleted();
        vehiclePOJPARepository.save(vehiclePO);

        Optional<VehiclePO> updatedVehiclePOOptional = vehiclePOJPARepository.findById(VEHICLE_ID);

        assertThat(updatedVehiclePOOptional.isPresent()).isTrue();
        VehiclePO updatedVehiclePO = updatedVehiclePOOptional.get();

        assertThat(updatedVehiclePO.isDeleted()).isTrue();
    }

    @Test
    public void findFirstByVehicleTypeAndVehiclePoolIdAndBlockedFalseOrderByCreationTimeAsc_Success() {
        createDefaultVehiclePO(VEHICLE_ID);
        createDefaultVehiclePO(UUID.randomUUID().toString());

        Optional<VehiclePO> vehiclePOOptional = vehiclePOJPARepository.findFirstByVehicleTypeAndVehiclePoolIdAndBlockedFalseOrderByCreationTimeAsc(VEHICLE_TYPE.name(),
                VEHICLE_POOL_ID);

        assertThat(vehiclePOOptional.isPresent()).isTrue();
        VehiclePO foundVehiclePO = vehiclePOOptional.get();

        assertThat(foundVehiclePO.getId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc_Success() {
        createDefaultVehiclePO(VEHICLE_ID);
        createDefaultVehiclePO(UUID.randomUUID().toString());

        Optional<VehiclePO> vehiclePOOptional = vehiclePOJPARepository.findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(VEHICLE_TYPE.name(),
                BRANCH_ID, ACRISS_CODE.name());

        assertThat(vehiclePOOptional.isPresent()).isTrue();
        VehiclePO foundVehiclePO = vehiclePOOptional.get();

        assertThat(foundVehiclePO.getId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void findByVehicleTypeAndBlockedFalse_Success() {
        createDefaultVehiclePO(VEHICLE_ID);

        List<VehiclePO> vehiclePOList = vehiclePOJPARepository.findByVehicleTypeAndBlockedFalse(VEHICLE_TYPE.name());

        assertThat(vehiclePOList.isEmpty()).isFalse();
        VehiclePO foundVehiclePO = vehiclePOList.get(0);

        assertThat(foundVehiclePO.getId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void findByVehiclePoolId_Success() {
        createDefaultVehiclePO(VEHICLE_ID);

        List<VehiclePO> vehiclePOList = vehiclePOJPARepository.findByVehiclePoolId(VEHICLE_POOL_ID);

        assertThat(vehiclePOList.isEmpty()).isFalse();
        VehiclePO foundVehiclePO = vehiclePOList.get(0);

        assertThat(foundVehiclePO.getId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void findByVehicleTypeAndBranchIdAndBlockedFalse_Success() {
        createDefaultVehiclePO(VEHICLE_ID);

        List<VehiclePO> vehiclePOList = vehiclePOJPARepository.findByVehicleTypeAndBranchIdAndBlockedFalse(VEHICLE_TYPE.name(),
                BRANCH_ID);

        assertThat(vehiclePOList.isEmpty()).isFalse();
        VehiclePO foundVehiclePO = vehiclePOList.get(0);

        assertThat(foundVehiclePO.getId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void findByVehicleTypeAndTestLocationAndBlockedFalse_Success() {
        createDefaultVehiclePO(VEHICLE_ID);

        List<VehiclePO> vehiclePOList = vehiclePOJPARepository.findByVehicleTypeAndTestLocationAndBlockedFalse(VEHICLE_TYPE.name(),
                TestLocation.MUNICH.name());

        assertThat(vehiclePOList.isEmpty()).isFalse();
        VehiclePO foundVehiclePO = vehiclePOList.get(0);

        assertThat(foundVehiclePO.getId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void countByVehiclePoolIdAndBlockedFalse_Success() {
        createDefaultVehiclePO(VEHICLE_ID);

        long countMatchingVehicles = vehiclePOJPARepository.countByVehiclePoolIdAndBlockedFalse(VEHICLE_POOL_ID);

        assertThat(countMatchingVehicles).isEqualTo(1);
    }

    @Test
    public void countByVehiclePoolIdAndBlockedTrue_Success() {
        VehiclePO vehiclePO = createDefaultVehiclePO(VEHICLE_ID);
        // Block vehicle and save
        String testCaseId = "TestCaseId";
        vehiclePO.block(testCaseId);
        vehiclePOJPARepository.save(vehiclePO);

        long countMatchingVehicles = vehiclePOJPARepository.countByVehiclePoolIdAndBlockedTrue(VEHICLE_POOL_ID);

        assertThat(countMatchingVehicles).isEqualTo(1);
    }

    @Test
    public void findByVehicleTypeAndBlockedTrueAndDeletedFalseWithinTimeFrame_Success() {
        Instant startDate = Instant.now().minus(2, ChronoUnit.DAYS);
        Instant endDate = Instant.now().plus(1, ChronoUnit.DAYS);
        VehiclePO vehiclePO = createDefaultVehiclePO(VEHICLE_ID);
        vehiclePO.block("TestCaseBlock");
        vehiclePOJPARepository.save(vehiclePO);

        List<VehiclePO> vehiclePOList = vehiclePOJPARepository.findByVehicleTypeAndBlockedTrueAndDeletedFalseWithinTimeFrame(VEHICLE_TYPE.name(),
                startDate, endDate);

        assertThat(vehiclePOList.isEmpty()).isFalse();
        VehiclePO foundVehiclePO = vehiclePOList.get(0);

        assertThat(foundVehiclePO.getId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void findByVehicleTypeAndBlockedTrueAndDeletedFalseWithinTimeFrame_AlreadyDeleted() {
        Instant startDate = Instant.now().minus(2, ChronoUnit.DAYS);
        Instant endDate = Instant.now().plus(1, ChronoUnit.DAYS);
        VehiclePO vehiclePO = createDefaultVehiclePO(VEHICLE_ID);
        vehiclePO.block("TestCaseBlock");
        vehiclePO.setDeleted();
        vehiclePOJPARepository.save(vehiclePO);

        List<VehiclePO> vehiclePOList = vehiclePOJPARepository.findByVehicleTypeAndBlockedTrueAndDeletedFalseWithinTimeFrame(VEHICLE_TYPE.name(),
                startDate, endDate);

        assertThat(vehiclePOList.isEmpty()).isTrue();
    }

    @Test
    public void findByVehicleTypeAndDeletedTrueWithinTimeFrame_Success() {
        Instant startDate = Instant.now().minus(2, ChronoUnit.DAYS);
        Instant endDate = Instant.now().plus(1, ChronoUnit.DAYS);
        VehiclePO vehiclePO = createDefaultVehiclePO(VEHICLE_ID);
        vehiclePO.block("TestCaseBlock");
        vehiclePO.setDeleted();
        vehiclePOJPARepository.save(vehiclePO);

        List<VehiclePO> vehiclePOList = vehiclePOJPARepository.findByVehicleTypeAndDeletedTrueWithinTimeFrame(VEHICLE_TYPE.name(),
                startDate, endDate);

        assertThat(vehiclePOList.isEmpty()).isFalse();
        VehiclePO foundVehiclePO = vehiclePOList.get(0);

        assertThat(foundVehiclePO.getId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void findByVehicleTypeAndCreatedWithinTimeFrame_Success() {
        Instant startDate = Instant.now().minus(2, ChronoUnit.DAYS);
        Instant endDate = Instant.now().plus(1, ChronoUnit.DAYS);
        VehiclePO vehiclePO = createDefaultVehiclePO(VEHICLE_ID);
        vehiclePOJPARepository.save(vehiclePO);

        List<VehiclePO> vehiclePOList = vehiclePOJPARepository.findByVehicleTypeAndCreatedWithinTimeFrame(VEHICLE_TYPE.name(),
                startDate, endDate);

        assertThat(vehiclePOList.isEmpty()).isFalse();
        VehiclePO foundVehiclePO = vehiclePOList.get(0);

        assertThat(foundVehiclePO.getId()).isEqualTo(VEHICLE_ID);
    }

    @Test
    public void findByVehicleTypeAndCreatedWithinTimeFrame_NotFound() {
        Instant startDate = Instant.now().minus(10, ChronoUnit.DAYS);
        Instant endDate = Instant.now().minus(5, ChronoUnit.DAYS);
        VehiclePO vehiclePO = createDefaultVehiclePO(VEHICLE_ID);
        vehiclePOJPARepository.save(vehiclePO);

        List<VehiclePO> vehiclePOList = vehiclePOJPARepository.findByVehicleTypeAndCreatedWithinTimeFrame(VEHICLE_TYPE.name(),
                startDate, endDate);

        assertThat(vehiclePOList.isEmpty()).isTrue();
    }

    @Test
    public void delete_Success() {
        VehiclePO vehiclePO = createDefaultVehiclePO(VEHICLE_ID);

        vehiclePOJPARepository.delete(vehiclePO);

        Optional<VehiclePO> deletedVehiclePOOptional = vehiclePOJPARepository.findById(VEHICLE_ID);
        assertThat(deletedVehiclePOOptional.isPresent()).isFalse();
    }

    /**
     * Helper methods
     */

    @NotNull
    private VehiclePO createDefaultVehiclePO(String vehicleId) {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(vehicleId, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);

        VehiclePO vehiclePOToCreate = VehiclePO.createNew(vehicleId);
        vehiclePOToCreate.newVehicle(testVehicle);
        vehiclePOJPARepository.save(vehiclePOToCreate);

        Optional<VehiclePO> vehiclePOOptional = vehiclePOJPARepository.findById(vehicleId);

        assertThat(vehiclePOOptional.isPresent()).isTrue();
        return vehiclePOOptional.get();
    }

}
