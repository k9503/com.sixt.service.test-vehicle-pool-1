package com.sixt.service.test_vehicle_pool.infrastructure;


import com.sixt.lib.event_schema.TelematicStateChanged;
import com.sixt.service.dynamic_vehicle_data.api.GetDataQuery;
import com.sixt.service.dynamic_vehicle_data.api.GetDataResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class DynamicVehicleDataServiceProxyTest {

    @Mock
    private RpcClient<GetDataResponse> getDataRpcClient;

    private DynamicVehicleDataServiceProxy serviceProxy;

    private String vehicleId;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        serviceProxy = new DynamicVehicleDataServiceProxy(getDataRpcClient);
        vehicleId = UUID.randomUUID().toString();
    }

    @Test
    public void getData_Success() throws RpcCallException {
        TelematicStateChanged vehicle = TelematicStateChanged.newBuilder()
                .setVehicleId(vehicleId).build();
        GetDataResponse getDataResponse = GetDataResponse.newBuilder().
                setVehicle(vehicle).build();
        when(getDataRpcClient.callSynchronous(any(GetDataQuery.class), any(OrangeContext.class))).thenReturn(getDataResponse);

        TelematicStateChanged telematicStateChanged = serviceProxy.getData(vehicleId);
        assertThat(telematicStateChanged).isNotNull();
        assertThat(telematicStateChanged.getVehicleId()).isEqualTo(vehicleId);
        verify(getDataRpcClient, times(1))
                .callSynchronous(any(GetDataQuery.class), any(OrangeContext.class));
    }

    @Test
    public void getData_Fail_RpcException() throws RpcCallException {
        when(getDataRpcClient.callSynchronous(any(GetDataQuery.class), any(OrangeContext.class)))
                .thenThrow(new RpcCallException(RpcCallException.Category.InternalServerError, "Couldn't find service"));

        TelematicStateChanged telematicStateChanged = serviceProxy.getData(vehicleId);
        assertThat(telematicStateChanged).isNull();
        verify(getDataRpcClient, times(1))
                .callSynchronous(any(GetDataQuery.class), any(OrangeContext.class));
    }

}
