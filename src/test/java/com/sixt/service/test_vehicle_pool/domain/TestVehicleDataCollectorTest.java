package com.sixt.service.test_vehicle_pool.domain;

import com.sixt.lib.event_schema.*;
import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.infrastructure.DynamicVehicleDataServiceProxy;
import com.sixt.service.test_vehicle_pool.infrastructure.FleetServiceProxy;
import com.sixt.service.test_vehicle_pool.infrastructure.db.VehiclePOJPARepository;
import com.sixt.service.test_vehicle_pool.util.TestVehicleProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@Import({TestVehicleDataCollector.class})
public class TestVehicleDataCollectorTest {

    @Autowired
    private TestVehicleDataCollector testVehicleDataCollector;

    @MockBean
    private VehiclePOJPARepository vehiclePORepository;

    @MockBean
    private FleetServiceProxy fleetServiceProxy;

    @MockBean
    private DynamicVehicleDataServiceProxy dynamicVehicleDataServiceProxy;

    private static final VehicleType VEHICLE_TYPE = VehicleType.SAC_MANUAL;
    private static final String VEHICLE_ID = UUID.randomUUID().toString();
    private static final String VIN = "RANDOMVIN01234567";
    private static final int BRANCH_ID = 11;
    private static final AcrissCode ACRISS_CODE = AcrissCode.CLMR;
    private static final String VEHICLE_POOL_ID = UUID.randomUUID().toString();

    @Test
    public void getCurrentTestVehicleData_Success() {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        testVehicle.setTcId("");
        testVehicle.setCreationTime(Instant.now());
        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);
        vehiclePO.newVehicle(testVehicle);

        when(vehiclePORepository.findById(VEHICLE_ID)).thenReturn(Optional.of(vehiclePO));

        VehicleV2.Basic basic = VehicleV2.Basic.newBuilder().setVin(VIN).build();
        VehicleV2 vehicleV2 = VehicleV2.newBuilder().setBasic(basic)
                .setVehicleId(VEHICLE_ID).build();
        when(fleetServiceProxy.getByVehicleIdV2(VEHICLE_ID))
                .thenReturn(vehicleV2);

        TelematicStateChanged telematicStateChanged = getTelematicStateChanged();
        when(dynamicVehicleDataServiceProxy.getData(VEHICLE_ID))
                .thenReturn(telematicStateChanged);

        TestVehicle updatedTestVehicle = testVehicleDataCollector.getCurrentTestVehicleData(VEHICLE_ID);

        // Verify dynamic vehicle data
        assertThat(updatedTestVehicle.getDynamicVehicleData().getLockState()).isEqualTo(TelematicLockState.TLS_LOCKED.toString());
        assertThat(updatedTestVehicle.getDynamicVehicleData().getIgnitionState()).isEqualTo(IgnitionFlagOuterClass.IgnitionFlag.IGNITION_OFF.toString());
        assertThat(updatedTestVehicle.getDynamicVehicleData().getChargeLevel()).isEqualTo(40);
        assertThat(updatedTestVehicle.getDynamicVehicleData().getFuelLevelTotal()).isEqualTo(telematicStateChanged.getVehicleData().getFuelLevelMilliliters());
        assertThat(updatedTestVehicle.getDynamicVehicleData().getOdometer()).isEqualTo(telematicStateChanged.getVehicleData().getOdometer());
        assertThat(updatedTestVehicle.getDynamicVehicleData().getPosition().getLatitude()).isEqualTo(47.1f);
        assertThat(updatedTestVehicle.getDynamicVehicleData().getPosition().getLongitude()).isEqualTo(11.2f);
        assertThat(updatedTestVehicle.getDynamicVehicleData().getPickupAddress().getStreet()).isEqualTo(telematicStateChanged.getVehicleData().getAddress().getStreet());
        assertThat(updatedTestVehicle.getDynamicVehicleData().getPickupAddress().getHouseNumber()).isEqualTo(telematicStateChanged.getVehicleData().getAddress().getHouseNumber());
        assertThat(updatedTestVehicle.getDynamicVehicleData().getPickupAddress().getPostcode()).isEqualTo(telematicStateChanged.getVehicleData().getAddress().getPostcode());
        assertThat(updatedTestVehicle.getDynamicVehicleData().getPickupAddress().getCity()).isEqualTo(telematicStateChanged.getVehicleData().getAddress().getCity());
        assertThat(updatedTestVehicle.getDynamicVehicleData().getPickupAddress().getCountryCode()).isEqualTo(telematicStateChanged.getVehicleData().getAddress().getCountryCode());

        // Verify static vehicle data
        assertThat(updatedTestVehicle.getStaticVehicleData().getVehicleId()).isEqualTo(VEHICLE_ID);
        assertThat(updatedTestVehicle.getStaticVehicleData().getBasic().getVin()).isEqualTo(VIN);

        // Verify common test vehicle data
        assertThat(updatedTestVehicle.getCreationTime()).isNotNull();
        assertThat(updatedTestVehicle.getVehicleType()).isEqualTo(VEHICLE_TYPE);
        assertThat(updatedTestVehicle.isBlocked()).isFalse();
        assertThat(updatedTestVehicle.getBlockedTime()).isNull();
        assertThat(updatedTestVehicle.getTcId()).isEqualTo("");
        assertThat(updatedTestVehicle.getVehiclePoolId()).isEqualTo(VEHICLE_POOL_ID);

        verify(vehiclePORepository, times(1)).findById(VEHICLE_ID);
        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(VEHICLE_ID);
        verify(dynamicVehicleDataServiceProxy, times(1)).getData(VEHICLE_ID);
    }

    @Test
    public void getCurrentTestVehicleData_GetStaticDataFails() {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        testVehicle.setTcId("");
        testVehicle.setCreationTime(Instant.now());
        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);
        vehiclePO.newVehicle(testVehicle);

        when(vehiclePORepository.findById(VEHICLE_ID)).thenReturn(Optional.of(vehiclePO));

        when(fleetServiceProxy.getByVehicleIdV2(VEHICLE_ID))
                .thenReturn(null);

        TestVehicle updatedTestVehicle = testVehicleDataCollector.getCurrentTestVehicleData(VEHICLE_ID);
        assertThat(updatedTestVehicle).isNull();

        verify(vehiclePORepository, times(1)).findById(VEHICLE_ID);
        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(VEHICLE_ID);
        verify(dynamicVehicleDataServiceProxy, never()).getData(VEHICLE_ID);
    }

    @Test
    public void getCurrentTestVehicleData_GetDynamicDataFails() {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        testVehicle.setTcId("");
        testVehicle.setCreationTime(Instant.now());
        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);
        vehiclePO.newVehicle(testVehicle);

        when(vehiclePORepository.findById(VEHICLE_ID)).thenReturn(Optional.of(vehiclePO));

        VehicleV2.Basic basic = VehicleV2.Basic.newBuilder().setVin(VIN).build();
        VehicleV2 vehicleV2 = VehicleV2.newBuilder().setBasic(basic)
                .setVehicleId(VEHICLE_ID).build();
        when(fleetServiceProxy.getByVehicleIdV2(VEHICLE_ID))
                .thenReturn(vehicleV2);

        when(dynamicVehicleDataServiceProxy.getData(VEHICLE_ID))
                .thenReturn(null);

        TestVehicle updatedTestVehicle = testVehicleDataCollector.getCurrentTestVehicleData(VEHICLE_ID);
        assertThat(updatedTestVehicle).isNull();

        verify(vehiclePORepository, times(1)).findById(VEHICLE_ID);
        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(VEHICLE_ID);
        verify(dynamicVehicleDataServiceProxy, times(1)).getData(VEHICLE_ID);
    }

    @Test
    public void getCurrentTestVehicleStaticData_Success() {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        testVehicle.setTcId("");
        testVehicle.setCreationTime(Instant.now());
        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);
        vehiclePO.newVehicle(testVehicle);

        when(vehiclePORepository.findById(VEHICLE_ID)).thenReturn(Optional.of(vehiclePO));

        VehicleV2.Basic basic = VehicleV2.Basic.newBuilder().setVin(VIN).build();
        VehicleV2 vehicleV2 = VehicleV2.newBuilder().setBasic(basic)
                .setVehicleId(VEHICLE_ID).build();
        when(fleetServiceProxy.getByVehicleIdV2(VEHICLE_ID))
                .thenReturn(vehicleV2);

        TestVehicle testVehicleStaticData = testVehicleDataCollector.getCurrentTestVehicleStaticData(VEHICLE_ID);

        // Verify static vehicle data
        assertThat(testVehicleStaticData.getStaticVehicleData().getVehicleId()).isEqualTo(VEHICLE_ID);
        assertThat(testVehicleStaticData.getStaticVehicleData().getBasic().getVin()).isEqualTo(VIN);

        // Verify common test vehicle data
        assertThat(testVehicleStaticData.getCreationTime()).isNotNull();
        assertThat(testVehicleStaticData.getVehicleType()).isEqualTo(VEHICLE_TYPE);
        assertThat(testVehicleStaticData.isBlocked()).isFalse();
        assertThat(testVehicleStaticData.getBlockedTime()).isNull();
        assertThat(testVehicleStaticData.getTcId()).isEqualTo("");
        assertThat(testVehicleStaticData.getVehiclePoolId()).isEqualTo(VEHICLE_POOL_ID);

        verify(vehiclePORepository, times(1)).findById(VEHICLE_ID);
        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(VEHICLE_ID);
    }

    @Test
    public void getCurrentTestVehicleStaticData_GetStaticDataFails() {
        TestVehicle testVehicle = TestVehicleProvider.getTestVehicle(VEHICLE_ID, VIN, VEHICLE_TYPE, BRANCH_ID, ACRISS_CODE,
                VEHICLE_POOL_ID);
        testVehicle.setTcId("");
        testVehicle.setCreationTime(Instant.now());
        VehiclePO vehiclePO = VehiclePO.createNew(VEHICLE_ID);
        vehiclePO.newVehicle(testVehicle);

        when(vehiclePORepository.findById(VEHICLE_ID)).thenReturn(Optional.of(vehiclePO));

        when(fleetServiceProxy.getByVehicleIdV2(VEHICLE_ID))
                .thenReturn(null);

        TestVehicle testVehicleStaticData = testVehicleDataCollector.getCurrentTestVehicleStaticData(VEHICLE_ID);
        assertThat(testVehicleStaticData).isNull();

        verify(vehiclePORepository, times(1)).findById(VEHICLE_ID);
        verify(fleetServiceProxy, times(1)).getByVehicleIdV2(VEHICLE_ID);
    }

    private TelematicStateChanged getTelematicStateChanged() {
        PositionOuterClass.Position position = PositionOuterClass.Position.newBuilder()
                .setLatitude(47.1f)
                .setLongitude(11.2f).build();

        AddressOuterClass.Address address = AddressOuterClass.Address.newBuilder()
                .setStreet("Hauptstr.")
                .setHouseNumber("10")
                .setPostcode("88000")
                .setCity("München")
                .setCountryCode("DE").build();

        VehicleDataSnapshot vehicleDataSnapshot = VehicleDataSnapshot.newBuilder()
                .setChargeLevel(40)
                .setFuelLevelMilliliters(50000)
                .setOdometer(5555)
                .setPosition(position)
                .setAddress(address).build();

        return TelematicStateChanged.newBuilder()
                .setLockState(TelematicLockState.TLS_LOCKED)
                .setIgnition(IgnitionFlagOuterClass.IgnitionFlag.IGNITION_OFF)
                .setVehicleData(vehicleDataSnapshot).build();
    }

}
