create table test_vehicle (
  vehicle_id text PRIMARY KEY,
  vin text,
  vehicle_type text,
  tc_id text,
  creation_time timestamp without time zone,
  blocked_time timestamp without time zone,
  blocked bool
);

CREATE INDEX IF NOT EXISTS vehicle_id_index ON test_vehicle(vehicle_id);

ALTER TABLE test_vehicle ADD CONSTRAINT name_uniq UNIQUE(vehicle_id);