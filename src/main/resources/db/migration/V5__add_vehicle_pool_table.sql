create table vehicle_poolpo (
  id text not null,
  tenant text not null,
  vehicle_type text not null,
  branch_id int4 not null,
  acriss_code text not null,
  pool_size int4 not null,
  clean_after_days int4 not null,
  version int8 not null
);

alter table vehiclepo add column vehicle_pool_id text;