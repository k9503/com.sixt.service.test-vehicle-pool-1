package com.sixt.service.test_vehicle_pool.application.dto.provider;

import com.sixt.service.test_vehicle_pool.application.dto.FuelType;
import org.apache.commons.lang3.RandomStringUtils;
import com.sixt.service.fleet.api.VehicleV2.Technical;


public class VehicleTechnicalSpecProvider {
    private final static String DEFAULT_CONSUMPTION = "4.9";
    private final static int DEFAULT_MODEL_YEAR = 2017;
    private final static String DEFAULT_GEARS = "6";
    private final static String DEFAULT_STEERING = "L";
    private final static String DEFAULT_SPEEDOMETER = "K";
    private final static String TANK_UNIT_GASOLINE = "ML";
    private final static String TANK_UNIT_ELECTRIC = "KWH";
    private final static String DEFAULT_TANK_VOLUME_GASOLINE = "80000";
    private final static String DEFAULT_TANK_VOLUME_ELECTRIC = "40";
    private final static String DEFAULT_EMISSION_CLASS = "EURO5";
    private final static String DEFAULT_CAPACITY = "3.0";

    public static Technical createTechnicalSpec(FuelType fuelType1, FuelType fuelType2) {
        final Technical.Builder technicalBuilder = Technical.newBuilder();
        technicalBuilder.setConsumption(DEFAULT_CONSUMPTION)
                .setModelYear(DEFAULT_MODEL_YEAR)
                .setGears(DEFAULT_GEARS)
                .setSteering(DEFAULT_STEERING)
                .setSpeedometer(DEFAULT_SPEEDOMETER);

        if (fuelType1 == FuelType.ELECTRIC) {
            technicalBuilder.setTankUnit(TANK_UNIT_ELECTRIC)
                    .setTankVolume(DEFAULT_TANK_VOLUME_ELECTRIC)
                    .setCapacityLiter("0")
                    .setPlugInHybrid(true);
        } else {
            technicalBuilder.setTankUnit(TANK_UNIT_GASOLINE)
                    .setTankVolume(DEFAULT_TANK_VOLUME_GASOLINE)
                    .setCapacityLiter(DEFAULT_CAPACITY)
                    .setPlugInHybrid(false);
        }

        if (fuelType2 == FuelType.GASOLINE || fuelType2 == FuelType.DIESEL) {
            technicalBuilder.setTankUnit2(TANK_UNIT_GASOLINE)
                    .setTankVolume2(DEFAULT_TANK_VOLUME_GASOLINE);
        }

        technicalBuilder.setEmissionClass(DEFAULT_EMISSION_CLASS);

        technicalBuilder.setManufacturersCode(RandomStringUtils.randomAlphabetic(20));

        return technicalBuilder.build();
    }

}
