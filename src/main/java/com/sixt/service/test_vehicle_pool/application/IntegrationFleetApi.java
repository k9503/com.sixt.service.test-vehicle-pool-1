package com.sixt.service.test_vehicle_pool.application;

import com.google.gson.JsonObject;
import okhttp3.Credentials;
import okhttp3.Headers;
import okhttp3.Headers.Builder;
import okhttp3.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;

import static net.logstash.logback.marker.Markers.append;

@Component
public class IntegrationFleetApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationFleetApi.class);
    private final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    private HttpClientBaseTlsV1 client;
    
    @Value("${integrationfleetapi.host}")
    private String host;
    @Value("${app.integrationfleetapi.basicauthuser}")
    private String basicAuthUser;
    @Value("${app.integrationfleetapi.basicauthpassword}")
    private String basicAuthPassword;
    @Value("${environment.env}")
    private String environment;

    @Autowired
    HttpClientBaseTlsV1 httpClientBaseTlsV1;

    @Autowired
    public IntegrationFleetApi(
            HttpClientBaseTlsV1 httpClientBaseTlsV1) {
        this.client = httpClientBaseTlsV1;
    }

    public void addTelematicUnit(String vehicleId, String vin) {
        final String PATH = "/vehicle/vin/" + vin + "/setTelematicStatus";
        String url = host + PATH;

        JsonObject jsonBody = new JsonObject();
        jsonBody.addProperty("status", "I");
        jsonBody.addProperty("type", "Ruptela");
        jsonBody.addProperty("updated", Instant.now().toString());

        try {
            String basicAuthToken = Credentials.basic(basicAuthUser, basicAuthPassword);
            Headers headers = new Builder()
                    .set("Authorization", basicAuthToken)
                    .build();

            client.post(url, jsonBody.toString(), headers, JSON);
            LOGGER.info(append("vehicleId", vehicleId), "Added telematic unit");
        } catch (Exception e) {
            LOGGER.error(append("vehicleId", vehicleId), "Failed to add telematic unit: {}", e.getMessage());
        }
    }

}
