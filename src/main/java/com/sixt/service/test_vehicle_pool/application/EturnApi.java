package com.sixt.service.test_vehicle_pool.application;

import okhttp3.Headers;
import okhttp3.Headers.Builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static net.logstash.logback.marker.Markers.append;

@Component
public class EturnApi {
    private static final Logger LOGGER = LoggerFactory.getLogger(EturnApi.class);
    private HttpClientBaseTlsV1 client;

    @Value("${eturn.host}")
    private String host;
    @Value("${eturn.cookiename}")
    private String cookieName;
    @Value("${eturn.cookievalue}")
    private String cookieValue;
    @Value("${environment.env}")
    private String environment;
    @Value("${app.eturn.token}")
    private String eturnToken;

    @Autowired
    HttpClientBaseTlsV1 httpClientBaseTlsV1;

    @Autowired
    public EturnApi(
            HttpClientBaseTlsV1 httpClientBaseTlsV1) {
        this.client = httpClientBaseTlsV1;
    }

    public void checkIn(String vehicleId, String licensePlate) {
        // ETurn is currently not supported on DEV
        if (environment.equals("dev")) {
            LOGGER.error("ETurn currently not supported on DEV, skipping ETurn checking");
            return;
        }

        // Eturn data for checkin using credentials working only on branch 1
        String data = "{\"protocol\":{\"email\":\"\",\"spra\":\"0\",\"print\":\"0\",\"send\":\"0\"},\"text\":[],\"aktk\":\"6514\",\"spra\":\"0\",\"imei\":\"999999999900001\",\"etnd\":\"20180926\",\"equipment\":{\"MUS\":\"0\",\"NAV\":\"0\"},\"sign\":\"2\",\"din\":\"2\",\"amt\":\""
                + licensePlate
                + "\",\"damages\":[],\"ettk\":\"" + eturnToken + "\",\"etun\":\"et25730\",\"aktb\":\"8\",\"etnt\":\"132630\",\"ettp\":\"2\",\"repair\":\"0\",\"aktg\":\"0\",\"dout\":\"3\"}";

        final String PATH = "/movement.create";
        String url = host + PATH;

        try {
            // Set cookie
            Headers headers = new Builder()
                    .set("Cookie", cookieName + "=" + cookieValue)
                    .build();

            Map<String, String> queryParamMap = new HashMap<>();
            queryParamMap.put("data", data);
            client.getQueryParams(url, queryParamMap, headers);
            LOGGER.info(append("vehicleId", vehicleId), "Successful ETurn check-in");
        } catch (Exception e) {
            LOGGER.error(append("vehicleId", vehicleId), "ETurn check-in failed: {}", e.getMessage());
        }
    }

}
