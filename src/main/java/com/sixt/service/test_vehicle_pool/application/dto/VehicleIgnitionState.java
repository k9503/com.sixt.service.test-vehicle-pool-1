package com.sixt.service.test_vehicle_pool.application.dto;


public enum VehicleIgnitionState {
    IGNITION_OFF,
    IGNITION_ON,
    UNKNOWN
}
