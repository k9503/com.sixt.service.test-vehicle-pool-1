package com.sixt.service.test_vehicle_pool.application.dto.racvehicle;

public enum Country {
    DE,
    US,
    IT,
    NL,
    ES,
    FR,
    UK,
    GB,
    AT
}
