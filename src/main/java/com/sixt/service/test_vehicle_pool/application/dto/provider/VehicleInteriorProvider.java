package com.sixt.service.test_vehicle_pool.application.dto.provider;

import com.sixt.service.fleet.api.VehicleV2.Interior;

public class VehicleInteriorProvider {
    private final static String DEFAULT_COLOR = "Black";
    private final static String DEFAULT_UPHOLSTERY = "Leather";
    private final static String DEFAULT_UPHOLSTERY_COLOR_BASE = "White";
    private final static String DEFAULT_UPHOLSTERY_COLOR_TYPE = "Dark";
    private final static String DEFAULT_UPHOLSTERY_COLOR_DESCR = "Snow white";

    public static Interior createDefaultInterior() {
        return Interior.newBuilder()
                .setColor(DEFAULT_COLOR)
                .setUpholstery(DEFAULT_UPHOLSTERY)
                .setUpholsteryColorBase(DEFAULT_UPHOLSTERY_COLOR_BASE)
                .setUpholsteryColorType(DEFAULT_UPHOLSTERY_COLOR_TYPE)
                .setUpholsteryColorDescription(DEFAULT_UPHOLSTERY_COLOR_DESCR)
                .build();
    }

}