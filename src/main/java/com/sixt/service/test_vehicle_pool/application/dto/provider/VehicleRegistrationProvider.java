package com.sixt.service.test_vehicle_pool.application.dto.provider;

import com.sixt.service.fleet.api.VehicleV2.Registration;

public class VehicleRegistrationProvider {
    private final static String DEFAULT_HOLDER = "Sixt SE";
    private final static String DEFAULT_PERMIT_SERVICE = "Permit Service";

    public static Registration createDefaultRegistration() {
        return Registration.newBuilder()
                .setHolder(DEFAULT_HOLDER)
                .setPermitService(DEFAULT_PERMIT_SERVICE)
                .build();
    }

}