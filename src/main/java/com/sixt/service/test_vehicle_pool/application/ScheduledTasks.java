package com.sixt.service.test_vehicle_pool.application;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

import com.sixt.service.test_vehicle_pool.api.VehicleFuelType;
import com.sixt.service.test_vehicle_pool.api.VehiclePool;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.domain.VehiclePO;

import com.sixt.service.test_vehicle_pool.domain.VehiclePoolPO;
import com.sixt.service.test_vehicle_pool.domain.vehiclecreation.FastlaneVehicleCreator;
import com.sixt.service.test_vehicle_pool.domain.vehiclecreation.RacTelematicVehicleCreator;
import com.sixt.service.test_vehicle_pool.domain.vehiclecreation.RacVehicleCreator;
import com.sixt.service.test_vehicle_pool.domain.vehiclecreation.SacVehicleCreator;
import com.sixt.service.test_vehicle_pool.domain.vehicledeletion.VehicleCleaner;
import com.sixt.service.test_vehicle_pool.infrastructure.db.VehiclePOJPARepository;
import com.sixt.service.test_vehicle_pool.infrastructure.db.VehiclePoolPOJPARepository;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;

import static net.logstash.logback.marker.Markers.append;

@Component
public class ScheduledTasks {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledTasks.class);

    private VehiclePOJPARepository vehiclePORepository;

    private VehiclePoolPOJPARepository vehiclePoolPOJPARepository;

    private SacVehicleCreator sacVehicleCreator;

    private FastlaneVehicleCreator fastlaneVehicleCreator;

    private RacTelematicVehicleCreator racTelematicVehicleCreator;

    private RacVehicleCreator racVehicleCreator;

    private VehicleCleaner vehicleCleaner;

    public ScheduledTasks(VehiclePOJPARepository vehiclePORepository, VehiclePoolPOJPARepository vehiclePoolPOJPARepository, SacVehicleCreator sacVehicleCreator, FastlaneVehicleCreator fastlaneVehicleCreator,
                          RacTelematicVehicleCreator racTelematicVehicleCreator, RacVehicleCreator racVehicleCreator, VehicleCleaner vehicleCleaner) {
        this.vehiclePORepository = vehiclePORepository;
        this.vehiclePoolPOJPARepository = vehiclePoolPOJPARepository;
        this.sacVehicleCreator = sacVehicleCreator;
        this.fastlaneVehicleCreator = fastlaneVehicleCreator;
        this.racTelematicVehicleCreator = racTelematicVehicleCreator;
        this.racVehicleCreator = racVehicleCreator;
        this.vehicleCleaner = vehicleCleaner;
    }

    public void createVehiclesInPool(VehicleType expectedVehicleType) {
        MDC.put(OrangeContext.CORRELATION_ID, UUID.randomUUID().toString());
        LOGGER.info("Running vehicle in pool creation scheduler for expected vehicle type <{}>", expectedVehicleType);
        List<VehiclePoolPO> vehiclePoolPOList = vehiclePoolPOJPARepository.findAll();

        for (VehiclePoolPO vehiclePoolPO : vehiclePoolPOList) {
            try {
                VehiclePool vehiclePool = vehiclePoolPO.getVehiclePool();
                VehicleType vehicleType = vehiclePool.getVehicleType();
                VehicleFuelType vehicleFuelType = vehiclePool.getFuelType();

                if (vehicleType == expectedVehicleType) {
                    int branchId = vehiclePool.getBranchId();
                    AcrissCode acrissCode = AcrissCode.valueOf(vehiclePool.getAcrissCode());
                    String vehiclePoolId = vehiclePool.getId();

                    long size = vehiclePORepository.countByVehiclePoolIdAndBlockedFalse(vehiclePoolId);
                    LOGGER.info("Available test vehicles of type <{}> for vehicle pool <{}>: {}", vehicleType, vehiclePoolId, size);

                    int poolSize = vehiclePool.getPoolSize();
                    if (size < poolSize) {
                        long numNewVehiclesToCreate = poolSize - size;
                        LOGGER.info("Creating {} new test vehicle(s) of type <{}> for vehicle pool <{}> ...", numNewVehiclesToCreate, vehicleType, vehiclePoolId);

                        int numCreatedTestVehicles = createNewTestVehicles(numNewVehiclesToCreate, vehicleType, branchId, acrissCode,
                                vehicleFuelType, vehiclePoolId);

                        LOGGER.info("{} {} test vehicle(s) created for vehicle pool {}.", numCreatedTestVehicles, vehicleType, vehiclePoolId);
                    }
                }
            } catch (Throwable e) {
                LOGGER.error("Unexpected exception during execution of createVehiclesInPool scheduled task.", e);
            }
        }
    }

    /**
     * Cleanup / removal of already deleted vehicles from the database which are blocked longer then cleanup after days
     */
    public void vehicleCleanup() {
        MDC.put(OrangeContext.CORRELATION_ID, UUID.randomUUID().toString());
        List<VehiclePoolPO> vehiclePoolPOList = vehiclePoolPOJPARepository.findAll();

        for (VehiclePoolPO vehiclePoolPO : vehiclePoolPOList) {
            try {
                VehiclePool vehiclePool = vehiclePoolPO.getVehiclePool();

                VehicleType vehicleType = vehiclePool.getVehicleType();
                int cleanAfterDays = vehiclePool.getCleanAfterDays();
                Instant startDate = Instant.now().minus(365, ChronoUnit.DAYS);
                Instant endDate = Instant.now().minus(cleanAfterDays, ChronoUnit.DAYS);
                List<VehiclePO> vehiclesToClean = vehiclePORepository.findByVehicleTypeAndDeletedTrueWithinTimeFrame(vehicleType.name(),
                        startDate, endDate);
                LOGGER.info("Clean up {} test vehicles of type <{}>", vehiclesToClean.size(), vehicleType);

                for (VehiclePO currentVehiclePO : vehiclesToClean) {
                    vehiclePORepository.delete(currentVehiclePO);
                    LOGGER.debug(append("vehicleId", currentVehiclePO.getId()), "Cleaned up test vehicle successfully");
                }
                LOGGER.info("Cleaned up test vehicles of type <{}>", vehicleType);
            } catch (Throwable e) {
                LOGGER.error("Unexpected exception during execution of vehicleCleanup scheduled task.", e);
            }
        }
    }

    /**
     * Delete vehicles which are blocked since > 4 hours and set them to deleted in the database
     */
    public void vehicleDeletion() {
        MDC.put(OrangeContext.CORRELATION_ID, UUID.randomUUID().toString());
        List<VehiclePoolPO> vehiclePoolPOList = vehiclePoolPOJPARepository.findAll();

        for (VehiclePoolPO vehiclePoolPO : vehiclePoolPOList) {
            try {
                VehiclePool vehiclePool = vehiclePoolPO.getVehiclePool();

                VehicleType vehicleType = vehiclePool.getVehicleType();
                Instant startDate = Instant.now().minus(365, ChronoUnit.DAYS);
                Instant endDate = Instant.now().minus(4, ChronoUnit.HOURS);
                List<VehiclePO> vehiclesToDelete = vehiclePORepository.findByVehicleTypeAndBlockedTrueAndDeletedFalseWithinTimeFrame(vehicleType.name(),
                        startDate, endDate);
                LOGGER.info("Delete {} test vehicles of type <{}>", vehiclesToDelete.size(), vehicleType);

                for (VehiclePO currentVehiclePO : vehiclesToDelete) {
                    boolean successDeleted = vehicleCleaner.deleteVehicle(currentVehiclePO.getVehicleType(), currentVehiclePO.getId());

                    if (successDeleted) {
                        // Set deleted flag in DB
                        currentVehiclePO.setDeleted();
                        vehiclePORepository.save(currentVehiclePO);
                        LOGGER.debug(append("vehicleId", currentVehiclePO.getId()), "Deleted test vehicle successfully");
                    } else {
                        LOGGER.error(append("vehicleId", currentVehiclePO.getId()), "Deletion of test vehicle failed");
                    }

                }
                LOGGER.info("Deleted test vehicles of type <{}>", vehicleType);
            } catch (Throwable e) {
                LOGGER.error("Unexpected exception during execution of vehicleDeletion scheduled task.", e);
            }
        }
    }

    public void manualRacVehicleDeletion() {
        MDC.put(OrangeContext.CORRELATION_ID, UUID.randomUUID().toString());

        try {
            deleteManualRacVehicles(VehicleType.RAC_TELEMATIC_MANUAL);
            deleteManualRacVehicles(VehicleType.FASTLANE_MANUAL);
        } catch (Throwable e) {
            LOGGER.error("Unexpected exception during execution of manualRacVehicleDeletion scheduled task.", e);
        }
    }

    private void deleteManualRacVehicles(VehicleType vehicleType) {
        // Delete manual RAC vehicles (RAC_TELEMATIC_MANUAL and FASTLANE_MANUAL) which are older than 7 days
        Instant startDate = Instant.now().minus(365, ChronoUnit.DAYS);
        Instant endDate = Instant.now().minus(7, ChronoUnit.DAYS);
        List<VehiclePO> vehiclesToClean = vehiclePORepository.findByVehicleTypeAndCreatedWithinTimeFrame(vehicleType.name(),
                startDate, endDate);
        LOGGER.info("Clean up {} manual RAC test vehicles of type <{}>", vehiclesToClean.size(), vehicleType);

        for (VehiclePO currentVehiclePO : vehiclesToClean) {
            boolean successDeleted = vehicleCleaner.deleteVehicle(currentVehiclePO.getVehicleType(), currentVehiclePO.getId());

            if (successDeleted) {
                // Delete from DB
                vehiclePORepository.delete(currentVehiclePO);
                LOGGER.debug(append("vehicleId", currentVehiclePO.getId()), "Cleaned up manual RAC test vehicle successfully");
            } else {
                LOGGER.error(append("vehicleId", currentVehiclePO.getId()), "Deletion of manual RAC test vehicle failed");
            }
        }
        LOGGER.info("Cleaned up manual RAC test vehicles of type <{}>", vehicleType);
    }

    private int createNewTestVehicles(long numNewVehiclesToCreate, VehicleType type, int branchId, AcrissCode acrissCode,
                                      VehicleFuelType vehicleFuelType, String vehiclePoolId) {
        TestVehicle testVehicle;
        int numCreatedTestVehicles = 0;

        for (int i = 0; i < numNewVehiclesToCreate; i++) {
            testVehicle = createVehicle(type, branchId, acrissCode, vehicleFuelType, vehiclePoolId);

            if (testVehicle != null) {
                if (testVehicle.isValid()) {
                    VehiclePO vehiclePO = VehiclePO.createNew(testVehicle.getStaticVehicleData().getVehicleId());
                    vehiclePO.newVehicle(testVehicle);

                    vehiclePORepository.save(vehiclePO);
                    numCreatedTestVehicles++;
                }
            }
        }

        return numCreatedTestVehicles;
    }

    private TestVehicle createVehicle(VehicleType type, int branchId, AcrissCode acrissCode, VehicleFuelType vehicleFuelType,
                                      String vehiclePoolId) {
        LOGGER.info("Creation vehicle of type {} on branch {} with acriss code {} for pool {}", type.name(), branchId, acrissCode.name(), vehiclePoolId);
        switch (type) {
            case SAC_AUTOMATION:
                return sacVehicleCreator.createSacVehicleAutomation(vehicleFuelType, vehiclePoolId);
            case SAC_PERFORMANCE:
                return sacVehicleCreator.createSacVehiclePerformance(vehicleFuelType, vehiclePoolId);
            case RAC_TELEMATIC_AUTOMATION:
                return racTelematicVehicleCreator.createRacTelematicVehicleAutomation(branchId, vehicleFuelType, vehiclePoolId);
            case RAC_AUTOMATION:
                return racVehicleCreator.createRacVehicleAutomation(branchId, acrissCode, vehicleFuelType, vehiclePoolId);
            case FASTLANE_AUTOMATION:
                return fastlaneVehicleCreator.createFastlaneVehicleAutomation(branchId, acrissCode, vehicleFuelType, vehiclePoolId);
            default:
                LOGGER.error("Tried creation of not supported vehicle type <{}>", type);
                return null;
        }
    }

}
