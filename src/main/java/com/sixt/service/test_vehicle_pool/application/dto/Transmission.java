package com.sixt.service.test_vehicle_pool.application.dto;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public enum Transmission {
    EMPTY("", ""),
    AUTOMATIC("A", "Automatic"),
    MANUAL("M", "Manual");

    private static final Logger LOGGER = LoggerFactory.getLogger(Transmission.class);

    public static final String FLEET_PREFIX = "Trans_";

    private String transmissionCode;
    private String transmissionEnglishDefault;

    Transmission(String transmissionCode, String transmissionEnglishDefault) {
        this.transmissionCode = transmissionCode;
        this.transmissionEnglishDefault = transmissionEnglishDefault;
    }

    public String getTransmissionCode() {
        return this.transmissionCode;
    }

    public String getTransmissionEnglishDefault() {
        return this.transmissionEnglishDefault;
    }

    public static Transmission getEnum(String code) {
        String transmissionCode = code.replace(Transmission.FLEET_PREFIX, "");
        for (Transmission value : Transmission.values()) {
            if (value.getTransmissionCode().equals(transmissionCode)) {
                return value;
            }
        }
        LOGGER.error("No vehicle transmission found for code: {}", code);
        return Transmission.EMPTY;
    }

    public static String getTransmissionCode(String transmission) {
        for (Transmission value : Transmission.values()) {
            if (value.getTransmissionEnglishDefault().equalsIgnoreCase(transmission)) {
                return value.getTransmissionCode();
            }
        }
        LOGGER.error("No vehicle transmission code found for transmission: {}", transmission);
        return StringUtils.EMPTY;
    }

    public static Transmission random() {
        Transmission[] allTransmissions = Transmission.values();

        // Skip empty transmission
        List<Transmission> transmissions = new ArrayList<>();
        for (Transmission currentTransmission : allTransmissions) {
            if (currentTransmission != EMPTY) {
                transmissions.add(currentTransmission);
            }
        }
        Random random = new Random();

        int index = random.nextInt(transmissions.size());
        return transmissions.get(index);
    }

}