package com.sixt.service.test_vehicle_pool.application.dto;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public enum BodyType {
    EMPTY("", ""),
    WAGON("ES", "wagon");

    private static final Logger LOGGER = LoggerFactory.getLogger(BodyType.class);

    public static final String FLEET_PREFIX = "BodyType_";

    private String bodyTypeCode;
    private String bodyTypeEnglishDefault;

    BodyType(String bodyTypeCode, String bodyTypeEnglishDefault) {
        this.bodyTypeCode = bodyTypeCode;
        this.bodyTypeEnglishDefault = bodyTypeEnglishDefault;
    }

    public String getBodyTypeCode() {
        return this.bodyTypeCode;
    }

    public String getBodyTypeEnglishDefault() {
        return this.bodyTypeEnglishDefault;
    }

    public static BodyType getEnum(String code) {
        String bodyTypeCode = code.replace(BodyType.FLEET_PREFIX, "");
        for (BodyType value : BodyType.values()) {
            if (value.getBodyTypeCode().equals(bodyTypeCode)) {
                return value;
            }
        }
        LOGGER.error("No vehicle body type found for code: {}", code);
        return BodyType.EMPTY;
    }

    public static String getBodyTypeCode(String bodyType) {
        for (BodyType value : BodyType.values()) {
            if (value.getBodyTypeEnglishDefault().equals(bodyType)) {
                return value.getBodyTypeCode();
            }
        }
        LOGGER.error("No vehicle body type code found for body type: {}", bodyType);
        return StringUtils.EMPTY;
    }

    public static BodyType random() {
        BodyType[] allBodyTypes = BodyType.values();

        // Skip empty transmission
        List<BodyType> bodyTypes = new ArrayList<>();
        for (BodyType currentBodyType : allBodyTypes) {
            if (currentBodyType != EMPTY) {
                bodyTypes.add(currentBodyType);
            }
        }
        Random random = new Random();

        int index = random.nextInt(bodyTypes.size());
        return bodyTypes.get(index);
    }

}