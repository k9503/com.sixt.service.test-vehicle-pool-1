package com.sixt.service.test_vehicle_pool.application.dto;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum Make {
    UNKNOWN("", ""),
    VW("VW", "Volkswagen"),
    AUDI("AU", "Audi"),
    MINI("M2", "MINI"),
    SKODA("SK", "Skoda"),
    SEAT("SE", "SEAT"),
    JAGUAR("JA", "Jaguar"),
    RENAULT("RE", "Renault"),
    NISSAN("NI", "Nissan"),
    BMW("BM", "BMW");

    private static final Logger LOGGER = LoggerFactory.getLogger(Make.class);

    public static final String FLEET_PREFIX = "Make_";

    private String makeCode;
    private String makeEnglishDefault;

    Make(String makeCode, String makeEnglishDefault) {
        this.makeCode = makeCode;
        this.makeEnglishDefault = makeEnglishDefault;
    }

    public String getMakeCode() {
        return this.makeCode;
    }

    public String getMakeEnglishDefault() {
        return this.makeEnglishDefault;
    }

    public static Make getEnum(String code) {
        String makeCode = code.replace(Make.FLEET_PREFIX, "");
        for (Make value : Make.values()) {
            if (value.getMakeCode().equals(makeCode)) {
                return value;
            }
        }
        LOGGER.error("No vehicle make found for code: {}", code);
        return Make.UNKNOWN;
    }

}