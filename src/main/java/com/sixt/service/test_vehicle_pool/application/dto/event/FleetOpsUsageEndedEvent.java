package com.sixt.service.test_vehicle_pool.application.dto.event;

import com.google.gson.JsonObject;
import com.sixt.service.test_vehicle_pool.application.dto.DynamicVehicleData;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.utils.UUIDCreator;

import java.time.Instant;

public class FleetOpsUsageEndedEvent {

    private TestVehicle testVehicle;

    public FleetOpsUsageEndedEvent(TestVehicle testVehicle) {
        this.testVehicle = testVehicle;
    }

    public JsonObject getAsJson() {
        Instant eventTimestamp = Instant.now();

        JsonObject meta = new JsonObject();
        meta.addProperty("name", "FleetOpsUsageEnded");
        meta.addProperty("timestamp", eventTimestamp.toString());
        meta.addProperty("correlation_id", UUIDCreator.createRandomE2EUUID());
        meta.addProperty("grouping", "VehicleUsage");
        meta.addProperty("distribution_key", testVehicle.getStaticVehicleData().getVehicleId());

        JsonObject vehicleDataSnapshot = getVehicleDataSnapshot(testVehicle, eventTimestamp);

        JsonObject jsonBody = new JsonObject();
        jsonBody.add("meta", meta);
        jsonBody.addProperty("vehicle_id", testVehicle.getStaticVehicleData().getVehicleId());
        jsonBody.addProperty("start_time", eventTimestamp.toString());
        jsonBody.addProperty("end_time", eventTimestamp.toString());
        jsonBody.add("vehicle_data", vehicleDataSnapshot);

        return jsonBody;
    }

    private static JsonObject getVehicleDataSnapshot(TestVehicle testVehicle, Instant eventTimestamp) {
        JsonObject vehicleDataSnapshot = new JsonObject();
        DynamicVehicleData dynamicVehicleData = testVehicle.getDynamicVehicleData();

        JsonObject position = new JsonObject();
        if (dynamicVehicleData.getPosition() != null) {
            position.addProperty("latitude", dynamicVehicleData.getPosition().getLatitude());
            position.addProperty("longitude", dynamicVehicleData.getPosition().getLongitude());
            vehicleDataSnapshot.add("position", position);
        }

        JsonObject address = new JsonObject();
        if (dynamicVehicleData.getPickupAddress() != null) {
            address.addProperty("house_number", dynamicVehicleData.getPickupAddress().getHouseNumber());
            address.addProperty("street", dynamicVehicleData.getPickupAddress().getStreet());
            address.addProperty("postcode", dynamicVehicleData.getPickupAddress().getPostcode());
            address.addProperty("city", dynamicVehicleData.getPickupAddress().getCity());
            address.addProperty("country_code", dynamicVehicleData.getPickupAddress().getCountryCode());
            address.addProperty("state", "");
            address.addProperty("type", "VehicleReturn");
            vehicleDataSnapshot.add("address", address);
        }

        vehicleDataSnapshot.addProperty("timestamp", eventTimestamp.toString());
        vehicleDataSnapshot.addProperty("odometer", dynamicVehicleData.getOdometer());
        vehicleDataSnapshot.addProperty("charge_level", dynamicVehicleData.getChargeLevel());
        int fuelLevelPercent = dynamicVehicleData.getFuelLevelTotal() * 100 / Math.round(Float.parseFloat(testVehicle.getStaticVehicleData()
                .getTechnical().getTankVolume()));
        vehicleDataSnapshot.addProperty("fuel_level_percent", fuelLevelPercent);

        return vehicleDataSnapshot;
    }

}
