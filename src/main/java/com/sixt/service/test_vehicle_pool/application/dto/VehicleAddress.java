package com.sixt.service.test_vehicle_pool.application.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class VehicleAddress {

    private String street;
    private String houseNumber;
    private String postcode;
    private String city;
    private String countryCode;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postCode) {
        this.postcode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof VehicleAddress)) return false;

        VehicleAddress that = (VehicleAddress) o;

        return new EqualsBuilder()
                .append(street, that.street)
                .append(houseNumber, that.houseNumber)
                .append(postcode, that.postcode)
                .append(city, that.city)
                .append(countryCode, that.countryCode)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(street)
                .append(houseNumber)
                .append(postcode)
                .append(city)
                .append(countryCode)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("street", street)
                .append("houseNumber", houseNumber)
                .append("postcode", postcode)
                .append("city", city)
                .append("countryCode", countryCode)
                .toString();
    }
}