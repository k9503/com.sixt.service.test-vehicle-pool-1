package com.sixt.service.test_vehicle_pool.application.dto;

import com.sixt.service.test_vehicle_pool.api.VehicleType;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum LicensePlateGenerator {
    EMPTY("", VehicleType.UNRECOGNIZED),
    SAC_AUTOMATION("SCA", VehicleType.SAC_AUTOMATION),
    SAC_PERFORMANCE("SCP", VehicleType.SAC_PERFORMANCE),
    SAC_MANUAL("SCM", VehicleType.SAC_MANUAL),
    SAC_SIMULATOR("VSI", VehicleType.SAC_SIMULATOR),
    FASTLANE_AUTOMATION("FLA", VehicleType.FASTLANE_AUTOMATION),
    FASTLANE_MANUAL("FLM", VehicleType.FASTLANE_MANUAL),
    RAC_TELEMATIC_AUTOMATION("RTA", VehicleType.RAC_TELEMATIC_AUTOMATION),
    RAC_TELEMATIC_MANUAL("RTM", VehicleType.RAC_TELEMATIC_MANUAL),
    RAC_AUTOMATION("RCA", VehicleType.RAC_AUTOMATION);

    private static final Logger LOGGER = LoggerFactory.getLogger(LicensePlateGenerator.class);

    private String prefix;
    private VehicleType vehicleType;

    LicensePlateGenerator(String prefix, VehicleType vehicleType) {
        this.prefix = prefix;
        this.vehicleType = vehicleType;
    }

    private VehicleType getVehicleType() {
        return this.vehicleType;
    }

    private String getPrefix() {
        return this.prefix;
    }

    public static String getLicensePlateByType(VehicleType vehicleType) {
        for (LicensePlateGenerator licensePlateGenerator : LicensePlateGenerator.values()) {
            if (licensePlateGenerator.getVehicleType().equals(vehicleType)) {
                return licensePlateGenerator.getPrefix() + "-" + RandomStringUtils.randomAlphabetic(2).toUpperCase()
                        + " " + RandomStringUtils.randomNumeric(4);
            }
        }
        LOGGER.error("No license plate prefix found for vehicle type: {}", vehicleType);
        return null;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("prefix", prefix)
                .append("vehicleType", vehicleType)
                .toString();
    }

}