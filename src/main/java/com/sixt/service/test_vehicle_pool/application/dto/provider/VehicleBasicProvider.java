package com.sixt.service.test_vehicle_pool.application.dto.provider;

import com.sixt.service.fleet.api.VehicleV2.Basic;
import com.sixt.service.test_vehicle_pool.api.VehicleFuelType;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.*;
import org.apache.commons.lang3.RandomUtils;

public final class VehicleBasicProvider {

    public static Basic createBasicAutomation(VehicleType vehicleType, String acrissCode, VehicleFuelType vehicleFuelType) {
        final String vehicleLicensePlate = LicensePlateGenerator.getLicensePlateByType(vehicleType);
        final String vin = VinGenerator.getVinByType(vehicleType);

        FuelType fuelType1;
        FuelType fuelType2;
        Model model;
        switch (vehicleFuelType) {
            case HYBRID:
                fuelType1 = FuelType.GASOLINE;
                fuelType2 = FuelType.ELECTRIC;
                model = Model.VW_GOLF;
                break;
            case ELECTRIC:
                fuelType1 = FuelType.ELECTRIC;
                fuelType2 = FuelType.NONE;
                model = Model.RENAULT_ZOE;
                break;
            case PREMIUM_UNLEADED:
                fuelType1 = FuelType.PREMIUM_UNLEADED;
                fuelType2 = FuelType.NONE;
                model = Model.VW_GOLF;
                break;
            default:
                fuelType1 = FuelType.GASOLINE;
                fuelType2 = FuelType.NONE;
                model = Model.VW_GOLF;
        }

        // For automation vehicles we use the ACRISS code which is configured in the pool and NOT the one configured for the used model
        Basic.Builder basicBuilder = getBasicBuilder(model.getMake(), model, acrissCode, fuelType1, fuelType2, vehicleLicensePlate, vin, Transmission.AUTOMATIC);

        return basicBuilder.build();
    }

    public static Basic createBasicManual(VehicleType vehicleType, String vin, VehicleFuelType vehicleFuelType,
                                          Transmission transmission) {
        final String vehicleLicensePlate = LicensePlateGenerator.getLicensePlateByType(vehicleType);

        FuelType fuelType1;
        FuelType fuelType2;
        Model model;
        switch (vehicleFuelType) {
            case HYBRID:
                fuelType1 = FuelType.GASOLINE;
                fuelType2 = FuelType.ELECTRIC;
                // Random model which is non electric
                model = Model.getRandomNonElectricModel();
                break;
            case ELECTRIC:
                fuelType1 = FuelType.ELECTRIC;
                fuelType2 = FuelType.NONE;
                // Random model which is electric
                model = Model.getRandomElectricModel();
                break;
            case PREMIUM_UNLEADED:
                fuelType1 = FuelType.PREMIUM_UNLEADED;
                fuelType2 = FuelType.NONE;
                model = Model.VW_GOLF;
                break;
            default:
                fuelType1 = FuelType.GASOLINE;
                fuelType2 = FuelType.NONE;
                // Random model which is non electric
                model = Model.getRandomNonElectricModel();
        }

        Basic.Builder basicBuilder = getBasicBuilder(model.getMake(), model, model.getAcrissCode(), fuelType1, fuelType2, vehicleLicensePlate, vin, transmission);

        return basicBuilder.build();
    }

    private static Basic.Builder getBasicBuilder(Make make, Model model, String acrissCode, FuelType fuelType1, FuelType fuelType2,
                                                 String vehicleLicensePlate, String vin, Transmission transmission) {
        Basic.Builder basicBuilder = Basic.newBuilder();
        basicBuilder.setMake(make.getMakeCode())
                .setModel(model.getModelCode())
                .setModelAnnex("Turbo Plus")
                .setJatoVehicleId(String.valueOf(RandomUtils.nextInt(10000, 20000)))
                .setJatoVersionName("Jato Version Name")
                .setSixtModel("Sixt Model")
                .setSixtType(77)
                .setVehicleType("PKW")
                .setPlatformType("Platform Type")
                .setBodyType(BodyType.WAGON.getBodyTypeCode())
                .setKw(135)
                .setPs(184)
                .setCcm(4000)
                .setVin(vin)
                .setLicensePlate(vehicleLicensePlate)
                .setCreationDate("2017-01-02")
                .setCompanyId(22)
                .setUseStatus("VN")
                .setTransmissionType(transmission.getTransmissionCode())
                .setDrivenWheels("Goodyear")
                .setFuelType(fuelType1.name())
                .setFuelType2(fuelType2.name())
                .setSeatingCapacity("5")
                .setNumberOfDoors("4");
        if (fuelType1 != FuelType.ELECTRIC) {
            basicBuilder.setCo2(130);
        }
        basicBuilder.setCo2Unit("g/km")
                .setInternalNumber(RandomUtils.nextInt(30000000, 99999999))
                .setContractType("Personal contract")
                .setInsuranceType("Full ensurance")
                .setAcrissCode(acrissCode);
        return basicBuilder;
    }

}
