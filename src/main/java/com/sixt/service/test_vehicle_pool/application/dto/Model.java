package com.sixt.service.test_vehicle_pool.application.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public enum Model {
    UNKNOWN("", "", Make.UNKNOWN, "", ""),
    VW_GOLF("VW:GOF", "GOLF", Make.VW, "https://app.rental-images.sixt.com/rental-static/vehiclepicturews/614565520181026/GRY/1050x600/60.png", "CLMR"),
    RENAULT_ZOE("RE:ZOE", "ZOE", Make.RENAULT, "https://app.rental-images.sixt.com/rental-static/vehiclepicturews/774589520171101/WHT/1050x600/60.png", "ECAE"),
    BMW_ONE_SERIES("BM:SE1", "SERIES 1", Make.BMW, "https://app.rental-images.sixt.com/rental-static/vehiclepicturews/78252920180301/ORN/1050x600/60.png", "CPMR"),
    BMW_I3("BM:BI3", "I3", Make.BMW, "https://app.rental-images.sixt.com/rental-static/vehiclepicturews/769134620190701/RED/1050x600/60.png", "ILAE"),
    JAGUAR_IPACE("JA:IPA", "I-PACE", Make.JAGUAR, "https://app.rental-images.sixt.com/rental-static/vehiclepicturews/795297120190507/GRY/1050x600/60.png", "LDAR"),
    AUDI_A3("AU:A30", "A3", Make.AUDI, "https://app.rental-images.sixt.com/rental-static/vehiclepicturews/719589720190911/WHT/1050x600/60.png", "CPAR"),
    MINI_MINI("M2:MIN", "MINI", Make.MINI, "https://app.rental-images.sixt.com/rental-static/vehiclepicturews/787214420190301/BRN/1050x600/60.png", "CTMR"),
    SKODA_OKTAVIA("SK:OCT", "OCTAVIA", Make.SKODA, "https://app.rental-images.sixt.com/rental-static/vehiclepicturews/606401520200124/RED/1050x600/60.png", "CWMR"),
    SEAT_LEON("SE:LEO", "LEON", Make.SEAT, "https://app.rental-images.sixt.com/rental-static/vehiclepicturews/775845620190509/BLK/1050x600/60.png", "CDMR"),
    VW_TOURAN("VW:TOR", "TOURAN", Make.VW, "https://app.rental-images.sixt.com/rental-static/vehiclepicturews/741774220191114/ORN/1050x600/60.png", "IVAR"),
    NISSAN_LEAF("NI:LEF", "LEAF", Make.NISSAN, "https://app.rental-images.sixt.com/rental-static/vehiclepicturews/792241920190306/WHT/1050x600/60.png", "IDAE"),
    AUDI_Q3("AU:AQ3", "Q3", Make.AUDI, "https://app.rental-images.sixt.com/rental-static/vehiclepicturews/739649020191121/BLU/1050x600/60.png", "SFAR");

    private static final List<Model> ELECTRIC_MODELS = Arrays.asList(RENAULT_ZOE, BMW_I3, JAGUAR_IPACE, NISSAN_LEAF);
    private static final List<Model> NON_ELECTRIC_MODELS = Arrays.asList(VW_GOLF, BMW_ONE_SERIES, AUDI_A3, MINI_MINI,
            SKODA_OKTAVIA, SEAT_LEON, AUDI_Q3);

    private static final Logger LOGGER = LoggerFactory.getLogger(Model.class);

    public static final String FLEET_PREFIX = "Model_";

    private String modelCode;
    private String modelEnglishDefault;
    private Make make;
    private String imageUrl;
    private String acrissCode;

    Model(String modelCode, String modelEnglishDefault, Make make, String imageUrl, String acrissCode) {
        this.modelCode = modelCode;
        this.modelEnglishDefault = modelEnglishDefault;
        this.make = make;
        this.imageUrl = imageUrl;
        this.acrissCode = acrissCode;
    }

    public String getModelCode() {
        return this.modelCode;
    }

    public String getModelEnglishDefault() {
        return this.modelEnglishDefault;
    }

    public Make getMake() {
        return this.make;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public String getAcrissCode() {
        return this.acrissCode;
    }

    public static Model getEnum(String code) {
        String modelCode = code.replace(Model.FLEET_PREFIX, "");
        for (Model value : Model.values()) {
            if (value.getModelCode().equals(modelCode)) {
                return value;
            }
        }
        LOGGER.error("No vehicle model found for code: {}", code);
        return Model.UNKNOWN;
    }

    public static Model getRandomElectricModel() {
        Random random = new Random();

        int index = random.nextInt(ELECTRIC_MODELS.size());
        return ELECTRIC_MODELS.get(index);
    }

    public static Model getRandomNonElectricModel() {
        Random random = new Random();

        int index = random.nextInt(NON_ELECTRIC_MODELS.size());
        return NON_ELECTRIC_MODELS.get(index);
    }

}