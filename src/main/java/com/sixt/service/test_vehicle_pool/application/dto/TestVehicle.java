package com.sixt.service.test_vehicle_pool.application.dto;

import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;

import static net.logstash.logback.marker.Markers.append;

public class TestVehicle {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestVehicle.class);

    private VehicleType vehicleType;
    private VehicleV2 staticVehicleData;
    private DynamicVehicleData dynamicVehicleData;
    private String tcId;
    private Instant creationTime;
    private boolean blocked;
    private Instant blockedTime;
    private String vehiclePoolId;
    private String testLocation;

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public VehicleV2 getStaticVehicleData() {
        return staticVehicleData;
    }

    public void setStaticVehicleData(VehicleV2 staticVehicleData) {
        this.staticVehicleData = staticVehicleData;
    }

    public DynamicVehicleData getDynamicVehicleData() {
        return dynamicVehicleData;
    }

    public void setDynamicVehicleData(DynamicVehicleData dynamicVehicleData) {
        this.dynamicVehicleData = dynamicVehicleData;
    }

    public String getTcId() {
        return tcId;
    }

    public void setTcId(String tcId) {
        this.tcId = tcId;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Instant creationTime) {
        this.creationTime = creationTime;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public Instant getBlockedTime() {
        return blockedTime;
    }

    public void setBlockedTime(Instant blockedTime) {
        this.blockedTime = blockedTime;
    }

    public String getVehiclePoolId() {
        return vehiclePoolId;
    }

    public void setVehiclePoolId(String vehiclePoolId) {
        this.vehiclePoolId = vehiclePoolId;
    }

    public String getTestLocation() {
        return testLocation;
    }

    public void setTestLocation(String testLocation) {
        this.testLocation = testLocation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        TestVehicle that = (TestVehicle) o;

        return new EqualsBuilder()
                .append(blocked, that.blocked)
                .append(vehicleType, that.vehicleType)
                .append(staticVehicleData, that.staticVehicleData)
                .append(dynamicVehicleData, that.dynamicVehicleData)
                .append(tcId, that.tcId)
                .append(creationTime, that.creationTime)
                .append(blockedTime, that.blockedTime)
                .append(vehiclePoolId, that.vehiclePoolId)
                .append(testLocation, that.testLocation)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(vehicleType)
                .append(staticVehicleData)
                .append(dynamicVehicleData)
                .append(tcId)
                .append(creationTime)
                .append(blocked)
                .append(blockedTime)
                .append(vehiclePoolId)
                .append(testLocation)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("vehicleType", vehicleType)
                .append("staticVehicleData", staticVehicleData)
                .append("dynamicVehicleData", dynamicVehicleData)
                .append("tcId", tcId)
                .append("creationTime", creationTime)
                .append("blocked", blocked)
                .append("blockedTime", blockedTime)
                .append("vehiclePoolId", vehiclePoolId)
                .append("testLocation", testLocation)
                .toString();
    }

    public boolean isValid() {
        if (staticVehicleData == null) {
            LOGGER.error("Test vehicle invalid because there is no static vehicle data");
            return false;
        }

        if (staticVehicleData.getVehicleId() == null) {
            LOGGER.error("Test vehicle invalid because there is no vehicle id");
            return false;
        }

        if (staticVehicleData.getBasic().getVin() == null) {
            LOGGER.error("Test vehicle invalid because there is no vin");
            return false;
        }

        if (dynamicVehicleData == null) {
            LOGGER.error(append("vehicleId", getStaticVehicleData().getVehicleId()), "Test vehicle invalid because there is no dynamic vehicle data");
            return false;
        }

        if (vehicleType == null || vehicleType == VehicleType.UNRECOGNIZED) {
            LOGGER.error(append("vehicleId", getStaticVehicleData().getVehicleId()), "Test vehicle invalid because vehicle type is not valid");
            return false;
        }

        return true;
    }

}