package com.sixt.service.test_vehicle_pool.application.dto.provider;

import com.sixt.service.fleet.api.VehicleV2.Leasing;

public final class VehicleLeasingProvider {
    private final static String DEFAULT_CURRENCY_CODE = "EUR";

    public static Leasing createDefaultLeasing() {
        return Leasing.newBuilder()
                .setCurrencyCode(DEFAULT_CURRENCY_CODE)
                .build();
    }

}