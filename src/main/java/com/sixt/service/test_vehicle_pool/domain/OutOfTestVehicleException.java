package com.sixt.service.test_vehicle_pool.domain;

public class OutOfTestVehicleException extends RuntimeException {

    public OutOfTestVehicleException() {
        super("There is no available test vehicle in the pool");
    }
}
