package com.sixt.service.test_vehicle_pool.domain.vehicledeletion;

import com.sixt.testdatamanagement.rac.vehicle.creation.config.Credentials;
import com.sixt.testdatamanagement.rac.vehicle.creation.config.TestEnvironment;
import com.sixt.testdatamanagement.rac.vehicle.creation.steps.RacVehicleDeletionSteps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RacVehicleDeletionStepsProxy implements InitializingBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(RacVehicleDeletionStepsProxy.class);

    private RacVehicleDeletionSteps racVehicleDeletionSteps;

    @Value("${environment.env}")
    private String environment;

    @Value("${app.ldap.user}")
    private String ldapUser;

    @Value("${app.ldap.password}")
    private String ldapPassword;

    @Override
    public void afterPropertiesSet() {
        Credentials credentials = new Credentials(ldapUser, ldapPassword);

        switch (environment) {
            case "dev":
                racVehicleDeletionSteps = new RacVehicleDeletionSteps(TestEnvironment.ACCEPTANCE, credentials);
                break;
            case "stage":
                racVehicleDeletionSteps = new RacVehicleDeletionSteps(TestEnvironment.STAGE, credentials);
                break;
            default:
                LOGGER.error("Not supported test environment <{}>", environment);
        }

        if (racVehicleDeletionSteps != null) {
            racVehicleDeletionSteps.logIn();
        }
    }

    public boolean cancelVehicle(String vehicleId) {
        return racVehicleDeletionSteps.cancelVehicle(vehicleId);
    }

}
