package com.sixt.service.test_vehicle_pool.domain;

public class InvalidBranchIdException extends RuntimeException {

	public InvalidBranchIdException() {
			super("Invalid branch id");
	}
}
