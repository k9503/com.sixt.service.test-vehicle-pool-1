package com.sixt.service.test_vehicle_pool.domain.vehiclecreation;

import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.fleet.api.VehicleV2.Basic;
import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.EventApi;
import com.sixt.service.test_vehicle_pool.application.dto.*;
import com.sixt.service.test_vehicle_pool.application.dto.provider.*;
import com.sixt.service.test_vehicle_pool.domain.VehiclePoolPO;
import com.sixt.service.test_vehicle_pool.infrastructure.FleetServiceProxy;
import com.sixt.service.test_vehicle_pool.infrastructure.OneFakeVehiclesServiceProxy;
import com.sixt.service.test_vehicle_pool.infrastructure.db.VehiclePoolPOJPARepository;
import com.sixt.service.test_vehicle_pool.utils.Sleeper;
import com.sixt.service.test_vehicle_pool.utils.UUIDCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static net.logstash.logback.marker.Markers.append;

@Component
public class SacVehicleCreator {
    private final static int DEFAULT_FUEL_LEVEL_TOTAL = 60000;
    private final static int DEFAULT_CHARGE_LEVEL = 100;
    private final static int DEFAULT_ODOMETER = 9999;

    private static final Logger LOGGER = LoggerFactory.getLogger(SacVehicleCreator.class);


    @Autowired
    private EventApi eventApi;

    @Autowired
    private FleetServiceProxy fleetServiceProxy;

    @Autowired
    private OneFakeVehiclesServiceProxy oneFakeVehiclesServiceProxy;

    @Autowired
    private VehiclePoolPOJPARepository vehiclePoolPORepository;


    public TestVehicle createSacVehicleAutomation(VehicleFuelType vehicleFuelType, String vehiclePoolId) {
        Optional<VehiclePoolPO> vehiclePoolPOOptional = vehiclePoolPORepository.findById(vehiclePoolId);

        if (!vehiclePoolPOOptional.isPresent()) {
            LOGGER.error("No matching vehicle pool found");
            return null;
        }

        VehiclePoolPO vehiclePoolPO = vehiclePoolPOOptional.get();
        CentralPosition centralPosition = vehiclePoolPO.getCentralPosition();
        VehiclePosition vehiclePosition = new VehiclePosition(centralPosition.getLat(), centralPosition.getLon());
        String acrissCode = vehiclePoolPO.getAcrissCode();

        final Basic vehicleBasic = VehicleBasicProvider.createBasicAutomation(VehicleType.SAC_AUTOMATION, acrissCode, vehicleFuelType);

        String vehicleId = UUIDCreator.createRandomE2EUUID();
        TestVehicle testVehicle = createSacVehicleViaEvent(vehicleId, vehicleBasic, vehiclePosition, true);

        if (testVehicle != null) {
            LOGGER.info("Created automation SAC vehicle via event: {}", testVehicle.toString());
            moveVehicle(testVehicle);
            testVehicle.setVehicleType(VehicleType.SAC_AUTOMATION);
            testVehicle.setVehiclePoolId(vehiclePoolId);
        }

        return testVehicle;
    }

    public TestVehicle createSacVehiclePerformance(VehicleFuelType vehicleFuelType, String vehiclePoolId) {
        Optional<VehiclePoolPO> vehiclePoolPOOptional = vehiclePoolPORepository.findById(vehiclePoolId);

        if (!vehiclePoolPOOptional.isPresent()) {
            LOGGER.error("No matching vehicle pool found");
            return null;
        }

        VehiclePoolPO vehiclePoolPO = vehiclePoolPOOptional.get();
        CentralPosition centralPosition = vehiclePoolPO.getCentralPosition();
        String acrissCode = vehiclePoolPO.getAcrissCode();

        // Get random position within a radius of 1000 m around the central position
        VehiclePosition vehiclePosition = RandomVehiclePositionProvider.getRandomVehiclePosition(centralPosition.getLat(), centralPosition.getLon(), 1000);

        final Basic vehicleBasic = VehicleBasicProvider.createBasicAutomation(VehicleType.SAC_PERFORMANCE, acrissCode, vehicleFuelType);

        String vehicleId = UUIDCreator.createRandomE2EUUID();
        TestVehicle testVehicle = createSacVehicleViaEvent(vehicleId, vehicleBasic, vehiclePosition, true);

        if (testVehicle != null) {
            LOGGER.info("Created performance SAC vehicle via event: {}", testVehicle.toString());
            moveVehicle(testVehicle);
            testVehicle.setVehicleType(VehicleType.SAC_PERFORMANCE);
            testVehicle.setVehiclePoolId(vehiclePoolId);
        }

        return testVehicle;
    }

    public TestVehicle createSacVehicleManual(VehicleType vehicleType, String vehicleId, String vin, TestLocation testLocation,
                                              VehicleFuelType vehicleFuelType, boolean isKeyless, Transmission transmission) {
        final VehiclePosition vehiclePosition = RandomVehiclePositionProvider.createPosition(testLocation);

        final Basic vehicleBasic = VehicleBasicProvider.createBasicManual(vehicleType, vin, vehicleFuelType, transmission);

        TestVehicle testVehicle = createSacVehicleViaEvent(vehicleId, vehicleBasic, vehiclePosition, isKeyless);

        if (testVehicle != null) {
            LOGGER.info("Created manual SAC vehicle via event: {}", testVehicle.toString());
            testVehicle.setVehicleType(vehicleType);
            testVehicle.setTestLocation(testLocation.name());
            moveVehicle(testVehicle);
        }

        return testVehicle;
    }

    private TestVehicle createSacVehicleViaEvent(String vehicleId, Basic vehicleBasic, VehiclePosition vehiclePosition,
                                                 boolean keyless) {

        TestVehicle testVehicle = new TestVehicle();

        final String color = Color.PINK.getColorCode();
        final String imageUrl = Model.getEnum(vehicleBasic.getModel()).getImageUrl();
        final FuelType fuelType1 = FuelType.getEnum(vehicleBasic.getFuelType());
        final FuelType fuelType2 = FuelType.getEnum(vehicleBasic.getFuelType2());

        VehicleV2 staticVehicleData = VehicleV2.newBuilder()
                .setVehicleId(vehicleId)
                .setBasic(vehicleBasic)
                .setExterior(VehicleExteriorProvider.createExterior(color, imageUrl))
                .setTechnical(VehicleTechnicalSpecProvider.createTechnicalSpec(fuelType1, fuelType2))
                .setInterior(VehicleInteriorProvider.createDefaultInterior())
                .setRegistration(VehicleRegistrationProvider.createDefaultRegistration())
                .setContract(VehicleContractProvider.createDefaultContract())
                .setEquipment(VehicleEquipmentProvider.createEquipment(keyless, fuelType1))
                .setInvoice(VehicleInvoiceProvider.createDefaultInvoice())
                .setLeasing(VehicleLeasingProvider.createDefaultLeasing())
                .build();

        eventApi.publishFleetVehicleCreatedEvent(staticVehicleData);

        // Wait 3 seconds until vehicle was created
        Sleeper.sleepForSeconds(3);

        final VehicleV2 createdStaticVehicleData = fleetServiceProxy.getByVehicleIdV2(staticVehicleData.getVehicleId());
        if (createdStaticVehicleData == null) {
            LOGGER.error(append("vehicleId", staticVehicleData.getVehicleId()), "Could not create static data for vehicle");
            return null;
        }

        testVehicle.setStaticVehicleData(createdStaticVehicleData);

        DynamicVehicleData dynamicVehicleData = new DynamicVehicleData();
        dynamicVehicleData.setDrivingPrice(new VehiclePrice(10, 10));
        dynamicVehicleData.setResExtendPrice(new VehiclePrice(10, 10));
        dynamicVehicleData.setPosition(vehiclePosition);
        dynamicVehicleData.setIgnitionState(VehicleIgnitionState.IGNITION_OFF);
        dynamicVehicleData.setLockState(VehicleLockState.LOCKED);
        dynamicVehicleData.setFuelLevelTotal(DEFAULT_FUEL_LEVEL_TOTAL);
        dynamicVehicleData.setChargeLevel(DEFAULT_CHARGE_LEVEL);
        dynamicVehicleData.setOdometer(DEFAULT_ODOMETER);
        dynamicVehicleData.setBranchId(0);

        testVehicle.setDynamicVehicleData(dynamicVehicleData);

        return testVehicle;
    }

    private void moveVehicle(TestVehicle testVehicle) {
        moveVehicleToFleetWithFleetAssignment(testVehicle);

        // SAC simulator vehicle does not exist in one-fake-vehicles service
        if (testVehicle.getVehicleType() != VehicleType.SAC_SIMULATOR) {
            setFakeVehicleBasicState(testVehicle);
        }

        // SAC simulator vehicle does not exist in one-fake-vehicles service
        if (testVehicle.getVehicleType() != VehicleType.SAC_SIMULATOR) {
            triggerGeoCodingReliable(testVehicle);
        }
    }

    private void moveVehicleToFleetWithFleetAssignment(TestVehicle testVehicle) {
        fleetServiceProxy.moveToFleet(testVehicle.getStaticVehicleData().getVehicleId(), FleetName.ONE);
        String fleetAssignmentId = UUIDCreator.createRandomE2EUUID();
        eventApi.publishFleetOpsUsageStartedEvent(testVehicle);
        eventApi.publishFleetAssignmentStartedEvent(testVehicle, fleetAssignmentId);
        eventApi.publishFleetAssignmentEndedEvent(testVehicle, fleetAssignmentId);
        eventApi.publishFleetOpsUsageEndedEvent(testVehicle);

        Sleeper.sleepForSeconds(2);
    }

    private void triggerGeoCodingReliable(TestVehicle testVehicle) {
        // Ignition ON / OFF to trigger geo coding reliable
        testVehicle.getDynamicVehicleData().setIgnitionState(VehicleIgnitionState.IGNITION_ON);
        try {
            oneFakeVehiclesServiceProxy.setState(testVehicle);
        } catch (Exception e) {
            throw new RuntimeException("Issue with setting a new vehicle state", e);
        }
        testVehicle.getDynamicVehicleData().setIgnitionState(VehicleIgnitionState.IGNITION_OFF);
        try {
            oneFakeVehiclesServiceProxy.setState(testVehicle);
        } catch (Exception e) {
            throw new RuntimeException("Issue with setting a new vehicle state", e);
        }
    }

    private void setFakeVehicleBasicState(TestVehicle testVehicle) {
        try {
            oneFakeVehiclesServiceProxy.setState(testVehicle);
        } catch (Exception e) {
            throw new RuntimeException("Issue with setting a new vehicle state", e);
        }
    }

}
