package com.sixt.service.test_vehicle_pool.domain.vehiclecreation;

import com.sixt.service.test_vehicle_pool.api.VehicleFuelType;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.BasicRacVehicle;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.VehicleState;
import com.sixt.service.test_vehicle_pool.domain.vehicledeletion.VehicleCleaner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import static net.logstash.logback.marker.Markers.append;

@Component
public class RacTelematicVehicleCreator {

    @Autowired
    private CommonRacVehicleCreator commonRacVehicleCreator;

    @Autowired
    private VehicleCleaner vehicleCleaner;

    private static final Logger LOGGER = LoggerFactory.getLogger(RacTelematicVehicleCreator.class);

    public TestVehicle createRacTelematicVehicleAutomation(int branchId, VehicleFuelType vehicleFuelType, String vehiclePoolId) {
        BasicRacVehicle basicRacVehicle = commonRacVehicleCreator.createCommonRacVehicleBasic(VehicleType.RAC_TELEMATIC_AUTOMATION,
                branchId, AcrissCode.IWMR, vehicleFuelType, true, false, false);

        TestVehicle testVehicle = commonRacVehicleCreator.finalizeCommonRacVehicleCreation(basicRacVehicle, VehicleState.AVAILABLE);

        if (testVehicle != null) {
            commonRacVehicleCreator.addTelematicUnit(testVehicle.getStaticVehicleData().getVehicleId(), testVehicle.getStaticVehicleData().getBasic().getVin());
            commonRacVehicleCreator.eturnCheckIn(testVehicle.getStaticVehicleData().getVehicleId(), testVehicle.getStaticVehicleData().getBasic().getLicensePlate());
            testVehicle.setVehicleType(VehicleType.RAC_TELEMATIC_AUTOMATION);
            testVehicle.setVehiclePoolId(vehiclePoolId);

            LOGGER.info("Created automation RAC vehicle with telematic unit: {}", testVehicle.toString());
        } else {
            // If test vehicle could not be created successfully delete it from GoOrange / Sixt Classic
            vehicleCleaner.deleteVehicle(VehicleType.RAC_TELEMATIC_AUTOMATION, basicRacVehicle.getVehicleId());
            LOGGER.info(append("vehicleId", basicRacVehicle.getVehicleId()), "Deleted not successfully added vehicle from GoOrange and Sixt Classic");
        }

        return testVehicle;
    }

    public BasicRacVehicle createRacTelematicVehicleManualBasic(int branchId, AcrissCode acrissCode, VehicleFuelType vehicleFuelType,
                                                                boolean withWinterTyres) {
        return commonRacVehicleCreator.createCommonRacVehicleBasic(VehicleType.RAC_TELEMATIC_MANUAL,
                branchId, acrissCode, vehicleFuelType, withWinterTyres, false, false);
    }

    public TestVehicle finishCreateRacTelematicVehicleManual(BasicRacVehicle basicRacVehicle) {
        TestVehicle testVehicle = commonRacVehicleCreator.finalizeCommonRacVehicleCreation(basicRacVehicle, VehicleState.AVAILABLE);

        if (testVehicle != null) {
            commonRacVehicleCreator.addTelematicUnit(testVehicle.getStaticVehicleData().getVehicleId(), testVehicle.getStaticVehicleData().getBasic().getVin());
            commonRacVehicleCreator.eturnCheckIn(testVehicle.getStaticVehicleData().getVehicleId(), testVehicle.getStaticVehicleData().getBasic().getLicensePlate());
            testVehicle.setVehicleType(VehicleType.RAC_TELEMATIC_MANUAL);
            LOGGER.info("Created manual RAC vehicle with telematic unit: {}", testVehicle.toString());
        } else {
            // If test vehicle could not be created successfully delete it from GoOrange / Sixt Classic
            vehicleCleaner.deleteVehicle(VehicleType.RAC_TELEMATIC_MANUAL, basicRacVehicle.getVehicleId());
            LOGGER.info(append("vehicleId", basicRacVehicle.getVehicleId()), "Deleted not successfully added vehicle from GoOrange and Sixt Classic");
        }

        return testVehicle;
    }

}
