package com.sixt.service.test_vehicle_pool.domain.vehiclecreation;

import com.sixt.service.test_vehicle_pool.api.VehicleFuelType;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.BasicRacVehicle;
import com.sixt.service.test_vehicle_pool.application.dto.racvehicle.VehicleState;
import com.sixt.service.test_vehicle_pool.domain.vehicledeletion.VehicleCleaner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import static net.logstash.logback.marker.Markers.append;

@Component
public class RacVehicleCreator {

    @Autowired
    private CommonRacVehicleCreator commonRacVehicleCreator;

    @Autowired
    private VehicleCleaner vehicleCleaner;

    private static final Logger LOGGER = LoggerFactory.getLogger(RacVehicleCreator.class);

    public TestVehicle createRacVehicleAutomation(int branchId, AcrissCode acrissCode, VehicleFuelType vehicleFuelType,
                                                  String vehiclePoolId) {
        BasicRacVehicle basicRacVehicle = commonRacVehicleCreator.createCommonRacVehicleBasic(VehicleType.RAC_AUTOMATION,
                branchId, acrissCode, vehicleFuelType, true, false, false);

        TestVehicle testVehicle = commonRacVehicleCreator.finalizeCommonRacVehicleCreation(basicRacVehicle, VehicleState.READY_FOR_RENTAL);

        if (testVehicle != null) {
            testVehicle.setVehicleType(VehicleType.RAC_AUTOMATION);
            testVehicle.setVehiclePoolId(vehiclePoolId);

            LOGGER.info("Created automation RAC vehicle: {}", testVehicle.toString());
        } else {
            // If test vehicle could not be created successfully delete it from GoOrange / Sixt Classic
            vehicleCleaner.deleteVehicle(VehicleType.RAC_AUTOMATION, basicRacVehicle.getVehicleId());
            LOGGER.info(append("vehicleId", basicRacVehicle.getVehicleId()), "Deleted not successfully added vehicle from GoOrange and Sixt Classic");
        }

        return testVehicle;
    }

}
