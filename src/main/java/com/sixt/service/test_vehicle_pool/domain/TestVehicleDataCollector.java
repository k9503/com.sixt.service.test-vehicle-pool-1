package com.sixt.service.test_vehicle_pool.domain;

import com.sixt.lib.event_schema.TelematicStateChanged;
import com.sixt.lib.event_schema.VehicleDataSnapshot;
import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.test_vehicle_pool.application.dto.*;
import com.sixt.service.test_vehicle_pool.infrastructure.DynamicVehicleDataServiceProxy;
import com.sixt.service.test_vehicle_pool.infrastructure.FleetServiceProxy;
import com.sixt.service.test_vehicle_pool.infrastructure.db.VehiclePOJPARepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static net.logstash.logback.marker.Markers.append;

@Component
public class TestVehicleDataCollector {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestVehicleDataCollector.class);

    @Autowired
    private DynamicVehicleDataServiceProxy dynamicVehicleDataServiceProxy;

    @Autowired
    private FleetServiceProxy fleetServiceProxy;

    @Autowired
    private VehiclePOJPARepository vehiclePORepository;


    public TestVehicle getCurrentTestVehicleData(String vehicleId) {
        TestVehicle testVehicle = getBasicTestVehicleById(vehicleId);

        // Get vehicle static data
        VehicleV2 staticVehicleData = getVehicleStaticData(vehicleId);
        if (staticVehicleData == null) {
            return null;
        }
        testVehicle.setStaticVehicleData(staticVehicleData);

        DynamicVehicleData dynamicVehicleData = getVehicleDynamicData(vehicleId);
        if (dynamicVehicleData == null) {
            return null;
        }
        testVehicle.setDynamicVehicleData(dynamicVehicleData);

        return testVehicle;
    }

    public TestVehicle getCurrentTestVehicleStaticData(String vehicleId) {
        TestVehicle testVehicle = getBasicTestVehicleById(vehicleId);

        // Get vehicle static data
        VehicleV2 staticVehicleData = getVehicleStaticData(vehicleId);
        if (staticVehicleData == null) {
            return null;
        }
        testVehicle.setStaticVehicleData(staticVehicleData);

        return testVehicle;
    }

    private VehicleV2 getVehicleStaticData(String vehicleId) {
        // Get complete static vehicle data
        final VehicleV2 staticVehicleData = fleetServiceProxy.getByVehicleIdV2(vehicleId);
        if (staticVehicleData == null) {
            LOGGER.error(append("vehicleId", vehicleId), "Could not get static data for vehicle");
        }

        return staticVehicleData;
    }

    private DynamicVehicleData getVehicleDynamicData(String vehicleId) {
        TelematicStateChanged vehicle = dynamicVehicleDataServiceProxy.getData(vehicleId);
        if (vehicle == null) {
            LOGGER.error(append("vehicleId", vehicleId), "Could not get dynamic data for vehicle");
            return null;
        }

        VehicleDataSnapshot vehicleDataSnapshot = vehicle.getVehicleData();
        DynamicVehicleData dynamicVehicleData = new DynamicVehicleData();
        // Note: Prices (drivingPrice and resExtendedPrice) can currently not be retrieved
        VehiclePosition vehiclePosition = new VehiclePosition((float) vehicleDataSnapshot.getPosition().getLatitude(),
                (float) vehicleDataSnapshot.getPosition().getLongitude());

        dynamicVehicleData.setPosition(vehiclePosition);
        VehicleAddress vehicleAddress = new VehicleAddress();
        vehicleAddress.setStreet(vehicleDataSnapshot.getAddress().getStreet());
        vehicleAddress.setHouseNumber(vehicleDataSnapshot.getAddress().getHouseNumber());
        vehicleAddress.setPostcode(vehicleDataSnapshot.getAddress().getPostcode());
        vehicleAddress.setCity(vehicleDataSnapshot.getAddress().getCity());
        vehicleAddress.setCountryCode(vehicleDataSnapshot.getAddress().getCountryCode());

        dynamicVehicleData.setPickupAddress(vehicleAddress);
        dynamicVehicleData.setIgnitionState(vehicle.getIgnition().name());
        dynamicVehicleData.setLockState(vehicle.getLockState().name());
        dynamicVehicleData.setChargeLevel((int) vehicleDataSnapshot.getChargeLevel());
        dynamicVehicleData.setOdometer(vehicleDataSnapshot.getOdometer());
        dynamicVehicleData.setFuelLevelTotal(vehicleDataSnapshot.getFuelLevelMilliliters());

        return dynamicVehicleData;
    }

    @Transactional
    private TestVehicle getBasicTestVehicleById(String vehicleId) {
        TestVehicle testVehicle = new TestVehicle();

        Optional<VehiclePO> vehiclePOOptional = vehiclePORepository.findById(vehicleId);
        if (vehiclePOOptional.isPresent()) {
            VehiclePO vehiclePO = vehiclePOOptional.get();
            testVehicle.setCreationTime(vehiclePO.getCreationTime());
            testVehicle.setVehicleType(vehiclePO.getVehicleType());
            testVehicle.setBlocked(vehiclePO.isBlocked());
            testVehicle.setBlockedTime(vehiclePO.getBlockedTime());
            testVehicle.setTcId(vehiclePO.getTcId());
            if (vehiclePO.getVehiclePoolId() == null) {
                testVehicle.setVehiclePoolId("");
            } else {
                testVehicle.setVehiclePoolId(vehiclePO.getVehiclePoolId());
            }
        }

        return testVehicle;
    }

}
