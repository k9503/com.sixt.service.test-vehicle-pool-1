package com.sixt.service.test_vehicle_pool.domain.vehicledeletion;

import com.sixt.service.journey.api.JourneyOuterClass.GetVehicleJourneyResponse;
import com.sixt.service.journey.api.JourneyOuterClass.PublicJourneyState;
import com.sixt.service.one_vehicle_availability.api.GetAvailabilityResponse;
import com.sixt.service.test_vehicle_pool.api.TestLocation;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.EventApi;
import com.sixt.service.test_vehicle_pool.application.dto.RandomVehiclePositionProvider;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.application.dto.VehiclePosition;
import com.sixt.service.test_vehicle_pool.domain.TestVehicleDataCollector;
import com.sixt.service.test_vehicle_pool.infrastructure.*;
import com.sixt.service.test_vehicle_pool.utils.Sleeper;
import com.sixt.service.test_vehicle_pool.utils.UUIDCreator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static net.logstash.logback.marker.Markers.append;

@Component
public class VehicleCleaner {

    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleCleaner.class);

    @Autowired
    private EventApi eventApi;
    @Autowired
    private FleetServiceProxy fleetServiceProxy;
    @Autowired
    private JourneyServiceProxy journeyServiceProxy;
    @Autowired
    private OneVehicleAvailabilityServiceProxy oneVehicleAvailabilityServiceProxy;
    @Autowired
    private OneVehicleMaintenanceServiceProxy oneVehicleMaintenanceServiceProxy;
    @Autowired
    private TestVehicleDataCollector testVehicleDataCollector;
    @Autowired
    private RacVehicleDeletionStepsProxy racVehicleDeletionStepsProxy;

    public boolean deleteVehicle(VehicleType vehicleType, String vehicleId) {
        boolean successDeleted = false;

        switch (vehicleType) {
            case SAC_AUTOMATION:
            case SAC_MANUAL:
            case SAC_SIMULATOR:
                successDeleted = deleteSacVehicle(vehicleId);
                break;
            case RAC_TELEMATIC_AUTOMATION:
            case RAC_TELEMATIC_MANUAL:
            case FASTLANE_AUTOMATION:
            case FASTLANE_MANUAL:
            case RAC_AUTOMATION:
                successDeleted = deleteRacVehicle(vehicleId);
                break;
            default:
                LOGGER.error("Tried deletion of not supported vehicle type <{}>", vehicleType);
        }

        return successDeleted;
    }

    private boolean deleteSacVehicle(String vehicleId) {
        return deleteVehicleOne(vehicleId);
    }

    private boolean deleteRacVehicle(String vehicleId) {
        if (!deleteVehicleOne(vehicleId)) {
            return false;
        }

        return deleteVehicleClassic(vehicleId);
    }

    private boolean deleteVehicleOne(String vehicleId) {
        cleanupVehicleBeforeDelete(vehicleId);

        // Get updated Test Vehicle
        TestVehicle testVehicle = testVehicleDataCollector.getCurrentTestVehicleData(vehicleId);

        if (testVehicle == null) {
            LOGGER.error(append("vehicleId", vehicleId), "Could not get current test vehicle data");
            return false;
        }

        if (testVehicle.getStaticVehicleData() == null) {
            LOGGER.error(append("vehicleId", vehicleId), "Could not get static data for vehicle");
            return false;
        }

        if (testVehicle.getDynamicVehicleData() == null) {
            LOGGER.error(append("vehicleId", vehicleId), "Could not get dynamic data for vehicle");
            return false;
        }

        // Temp workaround for de-fleeting vehicle
        String fleetAssignmentId = UUIDCreator.createRandomE2EUUID();
        // Workaround setting any position to avoid WARN messages in one-vehicle-enricher
        VehiclePosition vehiclePosition = RandomVehiclePositionProvider.createPosition(TestLocation.UNRECOGNIZED);
        testVehicle.getDynamicVehicleData().setPosition(vehiclePosition);
        eventApi.publishFleetOpsUsageStartedEvent(testVehicle);
        eventApi.publishFleetAssignmentStartedDefleetEvent(testVehicle, fleetAssignmentId);

        boolean deleteSuccess = fleetServiceProxy.deleteVehicle(vehicleId);
        if (!deleteSuccess) {
            LOGGER.error(append("vehicleId", vehicleId), "Error when deleting vehicle from fleet");
            return false;
        }

        // Verify if vehicle was deleted from fleet service
        Sleeper.sleepForSeconds(2);

        if (!fleetServiceProxy.getByVehicleIdV2(vehicleId).getIsDeleted()) {
            LOGGER.error(append("vehicleId", vehicleId), "Failed to delete vehicle from fleet, vehicle still found");
            return false;
        }

        return true;
    }

    private boolean deleteVehicleClassic(String vehicleId) {
        boolean deletedRacVehicleSuccess = racVehicleDeletionStepsProxy.cancelVehicle(vehicleId);
        if (!deletedRacVehicleSuccess) {
            LOGGER.error(append("vehicleId", vehicleId), "Deletion / cancel of RAC vehicle in Sixt Classic failed");
        }
        return deletedRacVehicleSuccess;
    }

    private void cleanupVehicleBeforeDelete(String vehicleId) {
        // Release vehicle just in case it is still in use by vehicle maintenance
        oneVehicleMaintenanceServiceProxy.releaseVehicle(vehicleId);

        // Verify if there is still a journey for the vehicle and force end it if there is one
        GetVehicleJourneyResponse getVehicleResponse = journeyServiceProxy.getVehicleJourney(vehicleId);
        if (getVehicleResponse != null) {
            if (getVehicleResponse.getCurrentState() != PublicJourneyState.PJS_ENDED) {
                LOGGER.debug(append("vehicleId", vehicleId), "Cleaning up hanging journey for vehicle");
                journeyServiceProxy.forceEndJourney(getVehicleResponse.getJourneyId());
            }
        }

        // End usage in vehicle availability service if vehicle is not available
        GetAvailabilityResponse getAvailabilityResponse = oneVehicleAvailabilityServiceProxy.getAvailability(vehicleId);

        if (getAvailabilityResponse != null) {
            if (!getAvailabilityResponse.getCurrentState().equals("AVAILABLE")) {
                LOGGER.debug(append("vehicleId", vehicleId), "Cleaning up non available vehicle");
                oneVehicleAvailabilityServiceProxy.endUsage(getAvailabilityResponse.getVehicleId(), getAvailabilityResponse.getOwner(),
                        getAvailabilityResponse.getReferenceId());
            }
        }

        LOGGER.info(append("vehicleId", vehicleId), "Successfully cleaned up vehicle before delete");
    }

}
