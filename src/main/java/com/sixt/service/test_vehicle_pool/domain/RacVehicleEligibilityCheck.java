package com.sixt.service.test_vehicle_pool.domain;

import com.sixt.service.branch.api.BranchOuterClass;
import com.sixt.service.test_vehicle_pool.infrastructure.BranchServiceProxy;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.VehicleCountryAcrissMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RacVehicleEligibilityCheck {

    private static final Logger LOGGER = LoggerFactory.getLogger(RacVehicleEligibilityCheck.class);

    @Autowired
    private BranchServiceProxy branchServiceProxy;

    public boolean isRacVehicleEligible(int branchId, String acrissCode) {
        BranchOuterClass.BranchObject branch = branchServiceProxy.get(branchId);
        if (branch != null) {
            String branchCountryCode = branch.getAddresses(0).getCountry().getIso2Code();
            try {
                List<String> supportedAcrissCode = VehicleCountryAcrissMapping.valueOf(branchCountryCode).getSupportedAcrissCodes();
                if (!supportedAcrissCode.contains(acrissCode)) {
                    LOGGER.error("Acriss code {} is not supported for branch id {} in country {}", acrissCode, branchId, branchCountryCode);
                    return false;
                }
            } catch (IllegalArgumentException e) {
                LOGGER.error("Country code {} is not supported", branchCountryCode);
                return false;
            }
        } else {
            LOGGER.error("Can not check if acriss code {} is supported for branch {}", acrissCode, branch);
            return false;
        }

        return true;
    }
    
}