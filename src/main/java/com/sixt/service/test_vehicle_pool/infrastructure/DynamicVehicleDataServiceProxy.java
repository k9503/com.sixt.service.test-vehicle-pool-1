package com.sixt.service.test_vehicle_pool.infrastructure;


import com.sixt.lib.event_schema.TelematicStateChanged;
import com.sixt.service.dynamic_vehicle_data.api.GetDataQuery;
import com.sixt.service.dynamic_vehicle_data.api.GetDataResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import static net.logstash.logback.marker.Markers.append;


@Component
public class DynamicVehicleDataServiceProxy {

    private static final Logger LOGGER = LoggerFactory.getLogger(DynamicVehicleDataServiceProxy.class);

    private RpcClient<GetDataResponse> getDataRpcClient;

    @Autowired
    public DynamicVehicleDataServiceProxy(RpcClient<GetDataResponse> getDataRpcClient) {
        this.getDataRpcClient = getDataRpcClient;
    }

    public TelematicStateChanged getData(String vehicleId) {
        GetDataQuery request = GetDataQuery.newBuilder()
                .setVehicleId(vehicleId)
                .setResolveAddress(true)
                .build();

        GetDataResponse getDataResponse;
        try {
            LOGGER.debug(append("vehicleId", vehicleId), "Calling dynamic-vehicle-data service getData");
            getDataResponse = getDataRpcClient.callSynchronous(request, new OrangeContext());
        } catch (RpcCallException e) {
            LOGGER.error(append("vehicleId", vehicleId), "Failed to get dynamic vehicle data by vehicle id.", e);
            return null;
        }

        return getDataResponse.getVehicle();
    }

}
