package com.sixt.service.test_vehicle_pool.infrastructure;


import com.sixt.service.fleet.api.*;
import com.sixt.service.test_vehicle_pool.application.dto.FleetName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import static net.logstash.logback.marker.Markers.append;


@Component
public class FleetServiceProxy {

    private static final Logger LOGGER = LoggerFactory.getLogger(FleetServiceProxy.class);

    private RpcClient<GetVehicleResponseV2> getByVehicleIdV2RpcClient;
    private RpcClient<MoveToFleetResponse> moveToFleetRpcClient;
    private RpcClient<DeleteVehicleResponse> deleteVehicleRpcClient;

    @Autowired
    public FleetServiceProxy(RpcClient<GetVehicleResponseV2> getByVehicleIdV2RpcClient, RpcClient<MoveToFleetResponse> moveToFleetRpcClient,
                             RpcClient<DeleteVehicleResponse> deleteVehicleRpcClient) {
        this.getByVehicleIdV2RpcClient = getByVehicleIdV2RpcClient;
        this.moveToFleetRpcClient = moveToFleetRpcClient;
        this.deleteVehicleRpcClient = deleteVehicleRpcClient;
    }

    public VehicleV2 getByVehicleIdV2(String vehicleId) {
        GetByVehicleIdQuery request = GetByVehicleIdQuery.newBuilder()
                .setVehicleId(vehicleId)
                .build();

        GetVehicleResponseV2 getVehicleResponseV2;
        try {
            LOGGER.debug(append("vehicleId", vehicleId), "Calling fleet service getByVehicleV2");
            getVehicleResponseV2 = getByVehicleIdV2RpcClient.callSynchronous(request, new OrangeContext());
        } catch (RpcCallException e) {
            LOGGER.debug(append("vehicleId", vehicleId), "No vehicle found");
            return null;
        }

        return getVehicleResponseV2.getVehicle();
    }

    public boolean moveToFleet(String vehicleId, FleetName fleetName) {
        MoveToFleetCommand request = MoveToFleetCommand.newBuilder()
                .setVehicleId(vehicleId)
                .setNewFleet(fleetName.getFleetName())
                .build();

        try {
            LOGGER.debug(append("vehicleId", vehicleId), "Calling fleet service moveToFleet moving to fleet {}", fleetName);
            moveToFleetRpcClient.callSynchronous(request, new OrangeContext());
        } catch (RpcCallException e) {
            LOGGER.error(append("vehicleId", vehicleId), "Failed to move vehicle to fleet.", e);
            return false;
        }

        return true;
    }

    public boolean deleteVehicle(String vehicleId) {
        DeleteVehicleCommand request = DeleteVehicleCommand.newBuilder()
                .setVehicleId(vehicleId)
                .build();

        try {
            LOGGER.debug(append("vehicleId", vehicleId), "Calling fleet service deleteVehicle");
            deleteVehicleRpcClient.callSynchronous(request, new OrangeContext());
        } catch (RpcCallException e) {
            LOGGER.error(append("vehicleId", vehicleId), "Failed to delete vehicle by vehicle id.", e);
            return false;
        }

        return true;
    }

}
