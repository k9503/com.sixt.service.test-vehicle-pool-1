package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.dynamic_vehicle_data.api.GetDataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClientFactory;

@Configuration
public class DynamicVehicleDataServiceConfig {

    private static final String SERVICE_NAME = "com.sixt.service.dynamic-vehicle-data";
    private static final String METHOD_NAME_GET_DATA = "DynamicVehicleData.GetData";


    @Value("${dynamic-vehicle-data.rpc-timeout-ms}")
    private int timeoutMs;

    @Value("${dynamic-vehicle-data.rpc-retries}")
    private int retries;

    @Autowired
    private RpcClientFactory rpcClientFactory;

    @Bean
    public RpcClient<GetDataResponse> getDataRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_GET_DATA, GetDataResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

}
