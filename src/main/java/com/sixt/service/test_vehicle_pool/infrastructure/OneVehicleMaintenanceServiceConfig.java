package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.one_vehicle_maintenance.api.ReleaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClientFactory;

@Configuration
public class OneVehicleMaintenanceServiceConfig {

    private static final String SERVICE_NAME = "com.sixt.service.one-vehicle-maintenance";
    private static final String METHOD_NAME_RELEASE_VEHICLE = "OneVehicleMaintenance.ReleaseVehicle";


    @Value("${one-vehicle-maintenance.rpc-timeout-ms}")
    private int timeoutMs;

    @Value("${one-vehicle-maintenance.rpc-retries}")
    private int retries;

    @Autowired
    private RpcClientFactory rpcClientFactory;

    @Bean
    public RpcClient<ReleaseResponse> releaseVehicleRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_RELEASE_VEHICLE, ReleaseResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

}
