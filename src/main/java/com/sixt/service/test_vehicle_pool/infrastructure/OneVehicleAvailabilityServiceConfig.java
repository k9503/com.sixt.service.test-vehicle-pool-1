package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.one_vehicle_availability.api.EndUsageResponse;
import com.sixt.service.one_vehicle_availability.api.GetAvailabilityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClientFactory;

@Configuration
public class OneVehicleAvailabilityServiceConfig {

    private static final String SERVICE_NAME = "com.sixt.service.one-vehicle-availability";
    private static final String METHOD_NAME_GET_AVAILABILITY = "VehicleAvailability.GetAvailability";
    private static final String METHOD_NAME_END_USAGE = "VehicleAvailability.EndUsage";


    @Value("${one-vehicle-availability.rpc-timeout-ms}")
    private int timeoutMs;

    @Value("${one-vehicle-availability.rpc-retries}")
    private int retries;

    @Autowired
    private RpcClientFactory rpcClientFactory;

    @Bean
    public RpcClient<GetAvailabilityResponse> getAvailabilityRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_GET_AVAILABILITY, GetAvailabilityResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

    @Bean
    public RpcClient<EndUsageResponse> endUsageRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_END_USAGE, EndUsageResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

}
