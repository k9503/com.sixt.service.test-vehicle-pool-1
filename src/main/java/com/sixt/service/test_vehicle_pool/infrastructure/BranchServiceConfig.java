package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.branch.api.BranchOuterClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClientFactory;

@Configuration
public class BranchServiceConfig {

    private static final String SERVICE_NAME = "com.sixt.service.branch";
    private static final String METHOD_NAME_GET = "Branch.Get";


    @Value("${branch.rpc-timeout-ms}")
    private int timeoutMs;

    @Value("${branch.rpc-retries}")
    private int retries;

    @Autowired
    private RpcClientFactory rpcClientFactory;

    @Bean
    public RpcClient<BranchOuterClass.BranchGetResponse> getRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_GET, BranchOuterClass.BranchGetResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

}
