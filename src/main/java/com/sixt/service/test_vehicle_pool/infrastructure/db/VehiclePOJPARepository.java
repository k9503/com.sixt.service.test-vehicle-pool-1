package com.sixt.service.test_vehicle_pool.infrastructure.db;

import com.sixt.service.test_vehicle_pool.domain.VehiclePO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface VehiclePOJPARepository extends JpaRepository<VehiclePO, String> {

    Optional<VehiclePO> findFirstByVehicleTypeAndBranchIdAndAcrissCodeAndBlockedFalseOrderByCreationTimeAsc(String vehicleType, int branchId, String acrissCode);

    Optional<VehiclePO> findFirstByVehicleTypeAndVehiclePoolIdAndBlockedFalseOrderByCreationTimeAsc(String vehicleType, String vehiclePoolId);

    List<VehiclePO> findByVehicleTypeAndBlockedFalse(String vehicleType);

    List<VehiclePO> findByVehicleTypeAndBranchIdAndBlockedFalse(String vehicleType, int branchId);

    List<VehiclePO> findByVehicleTypeAndTestLocationAndBlockedFalse(String vehicleType, String testLocation);

    List<VehiclePO> findByVehiclePoolId(String vehiclePoolId);

    long countByVehiclePoolIdAndBlockedFalse(String vehiclePoolId);

    long countByVehiclePoolIdAndBlockedTrue(String vehiclePoolId);

    @Query("SELECT v FROM VehiclePO v WHERE v.vehicleType = :vehicleType AND v.blocked = TRUE AND v.deleted = FALSE AND v.blockedTime BETWEEN :startDate AND :endDate")
    List<VehiclePO> findByVehicleTypeAndBlockedTrueAndDeletedFalseWithinTimeFrame(@Param("vehicleType") String vehicleType, @Param("startDate") Instant startDate, @Param("endDate") Instant endDate);

    @Query("SELECT v FROM VehiclePO v WHERE v.vehicleType = :vehicleType AND v.deleted = TRUE AND v.blockedTime BETWEEN :startDate AND :endDate")
    List<VehiclePO> findByVehicleTypeAndDeletedTrueWithinTimeFrame(@Param("vehicleType") String vehicleType, @Param("startDate") Instant startDate, @Param("endDate") Instant endDate);

    @Query("SELECT v FROM VehiclePO v WHERE v.vehicleType = :vehicleType AND v.creationTime BETWEEN :startDate AND :endDate")
    List<VehiclePO> findByVehicleTypeAndCreatedWithinTimeFrame(@Param("vehicleType") String vehicleType, @Param("startDate") Instant startDate, @Param("endDate") Instant endDate);

}