package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.one_vehicle_availability.api.EndUsageRequest;
import com.sixt.service.one_vehicle_availability.api.EndUsageResponse;
import com.sixt.service.one_vehicle_availability.api.GetAvailabilityQuery;
import com.sixt.service.one_vehicle_availability.api.GetAvailabilityResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;

import static net.logstash.logback.marker.Markers.append;


@Component
public class OneVehicleAvailabilityServiceProxy {

    private static final Logger LOGGER = LoggerFactory.getLogger(OneVehicleAvailabilityServiceProxy.class);

    private RpcClient<GetAvailabilityResponse> getAvailabilityRpcClient;

    private RpcClient<EndUsageResponse> endUsageRpcClient;

    @Autowired
    public OneVehicleAvailabilityServiceProxy(RpcClient<GetAvailabilityResponse> getAvailabilityRpcClient,
                                              RpcClient<EndUsageResponse> endUsageRpcClient) {
        this.getAvailabilityRpcClient = getAvailabilityRpcClient;
        this.endUsageRpcClient = endUsageRpcClient;
    }

    public GetAvailabilityResponse getAvailability(String vehicleId) {
        GetAvailabilityQuery request = GetAvailabilityQuery.newBuilder()
                .setVehicleId(vehicleId)
                .build();

        try {
            LOGGER.debug(append("vehicleId", vehicleId), "Calling one-vehicle-availability service get availability");
            return getAvailabilityRpcClient.callSynchronous(request, new OrangeContext());
        } catch (RpcCallException rpcCallException) {
            if (!rpcCallException.getCategory().equals(RpcCallException.Category.ResourceNotFound)) {
                LOGGER.error(append("vehicleId", vehicleId), "Failed to get availability state", rpcCallException);
            }

            return null;
        }
    }

    public void endUsage(String vehicleId, String owner, String referenceId) {
        EndUsageRequest request = EndUsageRequest.newBuilder()
                .setVehicleId(vehicleId)
                .setOwner(owner)
                .setReferenceId(referenceId)
                .build();

        try {
            LOGGER.debug(append("vehicleId", vehicleId), "Calling one-vehicle-availability service end usage with owner {} and reference id {}",
                    owner, referenceId);
            endUsageRpcClient.callSynchronous(request, new OrangeContext());
        } catch (RpcCallException e) {
            LOGGER.error(append("vehicleId", vehicleId), "Failed to end availability usage", e);
        }
    }

}
