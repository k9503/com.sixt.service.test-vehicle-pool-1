package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.journey.api.JourneyOuterClass.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClientFactory;

@Configuration
public class JourneyServiceConfig {

    private static final String SERVICE_NAME = "com.sixt.service.journey";
    private static final String METHOD_NAME_GET_VEHICLE_JOURNEY = "Journey.GetVehicleJourney";
    private static final String METHOD_NAME_FORCE_END_JOURNEY = "Journey.ForceEndJourney";


    @Value("${journey.rpc-timeout-ms}")
    private int timeoutMs;

    @Value("${journey.rpc-retries}")
    private int retries;

    @Autowired
    private RpcClientFactory rpcClientFactory;

    @Bean
    public RpcClient<GetVehicleJourneyResponse> getVehicleJourneyRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_GET_VEHICLE_JOURNEY, GetVehicleJourneyResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

    @Bean
    public RpcClient<ForceEndJourneyResponse> forceEndJourneyRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_FORCE_END_JOURNEY, ForceEndJourneyResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

}
