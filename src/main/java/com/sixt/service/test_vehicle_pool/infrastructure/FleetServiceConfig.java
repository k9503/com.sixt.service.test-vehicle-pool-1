package com.sixt.service.test_vehicle_pool.infrastructure;

import com.sixt.service.fleet.api.AssignOneRegionsResponse;
import com.sixt.service.fleet.api.DeleteVehicleResponse;
import com.sixt.service.fleet.api.GetVehicleResponseV2;
import com.sixt.service.fleet.api.MoveToFleetResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClient;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.rpc.RpcClientFactory;

@Configuration
public class FleetServiceConfig {

    private static final String SERVICE_NAME = "com.sixt.service.fleet";
    private static final String METHOD_NAME_GET_BY_VEHICLE_ID_V2 = "Fleet.GetByVehicleIdV2";
    private static final String METHOD_NAME_MOVE_TO_FLEET = "Fleet.MoveToFleet";
    private static final String METHOD_NAME_ASSIGN_ONE_REGIONS = "Fleet.AssignOneRegions";
    private static final String METHOD_NAME_DELETE_VEHICLE = "Fleet.DeleteVehicle";


    @Value("${fleet.rpc-timeout-ms}")
    private int timeoutMs;

    @Value("${fleet.rpc-retries}")
    private int retries;

    @Autowired
    private RpcClientFactory rpcClientFactory;

    @Bean
    public RpcClient<GetVehicleResponseV2> getByVehicleIdV2RpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_GET_BY_VEHICLE_ID_V2, GetVehicleResponseV2.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

    @Bean
    public RpcClient<MoveToFleetResponse> moveToFleetRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_MOVE_TO_FLEET, MoveToFleetResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

    @Bean
    public RpcClient<AssignOneRegionsResponse> assignOneRegionsRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_ASSIGN_ONE_REGIONS, AssignOneRegionsResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

    @Bean
    public RpcClient<DeleteVehicleResponse> deleteVehicleRpcClient() {
        return rpcClientFactory.newClient(SERVICE_NAME, METHOD_NAME_DELETE_VEHICLE, DeleteVehicleResponse.class)
                .withTimeout(timeoutMs)
                .withRetries(retries)
                .build();
    }

}
