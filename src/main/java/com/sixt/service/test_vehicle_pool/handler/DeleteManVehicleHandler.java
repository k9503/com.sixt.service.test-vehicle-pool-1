package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.DeleteManVehicleRequest;
import com.sixt.service.test_vehicle_pool.api.DeleteManVehicleResponse;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

import static com.sixt.service.test_vehicle_pool.api.DeleteManVehicleResponse.Error.DELETE_FAILED;
import static net.logstash.logback.marker.Markers.append;

@Component
@RpcHandler("TestVehiclePool.DeleteManVehicle")
public class DeleteManVehicleHandler implements ServiceMethodHandler<DeleteManVehicleRequest, DeleteManVehicleResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteManVehicleHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Override
    public DeleteManVehicleResponse handleRequest(DeleteManVehicleRequest request, OrangeContext orangeContext) throws RpcCallException {

        LOGGER.info(append("vehicleId", request.getVehicleId()), "Handling DeleteManVehicle");

        boolean successDelete = service.deleteManualVehicle(request.getVehicleId());

        if (!successDelete) {
            throw new RpcCallException(RpcCallException.Category.BackendError, "Manual test vehicle deletion failed")
                    .withErrorCode(DELETE_FAILED.name());
        }

        LOGGER.info(append("vehicleId", request.getVehicleId()), "Successfully processed deleted manual vehicle request");

        return DeleteManVehicleResponse.getDefaultInstance();
    }

}
