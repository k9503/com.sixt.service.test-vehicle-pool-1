package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

import java.util.List;

@Component
@RpcHandler("TestVehiclePool.GetAvailableSacVehiclesByLocation")
public class GetAvailableSacVehiclesByLocationHandler implements ServiceMethodHandler<GetAvailableSacVehiclesByLocationRequest,
        GetAvailableSacVehiclesByLocationResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetAvailableSacVehiclesByLocationHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Override
    public GetAvailableSacVehiclesByLocationResponse handleRequest(GetAvailableSacVehiclesByLocationRequest request, OrangeContext orangeContext) throws RpcCallException {

        LOGGER.info("Handling GetAvailableSacVehiclesByLocation");

        List<AvailableVehicle> availableSacVehicles;

        try {
            availableSacVehicles = service.getAvailableSacVehiclesByLocation(request.getTestLocation());
        } catch (Exception e) {
            LOGGER.error("Failed to get available SAC vehicles for test location {}: {}", request.getTestLocation().name(), e.getMessage());
            throw new RpcCallException(RpcCallException.Category.InternalServerError, "Get vehicles for test location failed");
        }

        LOGGER.info("Successfully retrieved available SAC vehicles for test location {}", request.getTestLocation().name());

        return GetAvailableSacVehiclesByLocationResponse.newBuilder()
                .addAllAvailableVehicles(availableSacVehicles)
                .build();
    }

}
