package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.GetFastlaneVehicleFromPoolRequest;
import com.sixt.service.test_vehicle_pool.api.GetFastlaneVehicleFromPoolResponse;
import com.sixt.service.test_vehicle_pool.api.VehicleType;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.domain.*;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import static com.sixt.service.test_vehicle_pool.api.GetFastlaneVehicleFromPoolResponse.Error.*;

@Component
@RpcHandler("TestVehiclePool.GetFastlaneVehicleFromPool")
public class GetFastlaneVehicleFromPoolHandler extends ExceptionMappingHandler
        implements ServiceMethodHandler<GetFastlaneVehicleFromPoolRequest, GetFastlaneVehicleFromPoolResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetFastlaneVehicleFromPoolHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Autowired
    public GetFastlaneVehicleFromPoolHandler() {
        // Mapping proto exception to rpc exception
        register(InvalidBranchIdException.class, RpcCallException.Category.BadRequest, INVALID_BRANCH_ID.name());
        register(InvalidAcrissCodeException.class, RpcCallException.Category.BadRequest, INVALID_ACRISS_CODE.name());
        register(UndefinedTestCaseIdException.class, RpcCallException.Category.BadRequest, UNDEFINED_TCID.name());
        register(OutOfTestVehicleException.class, RpcCallException.Category.ResourceNotFound, OUT_OF_TEST_VEHICLES.name());
    }

    @Override
    public GetFastlaneVehicleFromPoolResponse handleRequest(GetFastlaneVehicleFromPoolRequest request, OrangeContext orangeContext) throws RpcCallException {

        LOGGER.info("Handling GetFastlaneVehicleFromPool");

        try {
            validate(request);
        } catch (Exception e) {
            LOGGER.error("Validation of GetFastlaneVehicleFromPool request failed: {}", e.getMessage());
            throw mapException(e);
        }

        TestVehicle testVehicle;
        try {
            testVehicle = service.getVehicleFromPool(VehicleType.FASTLANE_AUTOMATION, request.getBranchId(), AcrissCode.valueOf(request.getAcrissCode()),
                    request.getTcId());
        } catch (Exception e) {
            LOGGER.error("Failed getting test vehicle: {}", e.getMessage());
            throw mapException(e);
        }

        if (testVehicle == null) {
            throw new RpcCallException(RpcCallException.Category.InternalServerError, "Test vehicle is null");
        }

        LOGGER.info("Got Fastlane vehicle {} from pool", testVehicle.getStaticVehicleData().getVehicleId());

        return GetFastlaneVehicleFromPoolResponse.newBuilder()
                .setVehicleId(testVehicle.getStaticVehicleData().getVehicleId())
                .setVin(testVehicle.getStaticVehicleData().getBasic().getVin())
                .setInternalNumber(testVehicle.getStaticVehicleData().getBasic().getInternalNumber())
                .build();
    }

    private void validate(GetFastlaneVehicleFromPoolRequest getFastlaneVehicleFromPoolRequest) throws
            InvalidBranchIdException,
            InvalidAcrissCodeException,
            UndefinedTestCaseIdException,
            RpcCallException {
        if (getFastlaneVehicleFromPoolRequest == null) {
            throw new RpcCallException(RpcCallException.Category.BadRequest, "Invalid request: null.").withErrorCode(BAD_REQUEST.name());
        }
        if (getFastlaneVehicleFromPoolRequest.getBranchId() == 0) {
            throw new InvalidBranchIdException();
        }
        if (getFastlaneVehicleFromPoolRequest.getAcrissCode() == null || !EnumUtils.isValidEnum(AcrissCode.class, getFastlaneVehicleFromPoolRequest.getAcrissCode())) {
            throw new InvalidAcrissCodeException();
        }
        if (StringUtils.isBlank(getFastlaneVehicleFromPoolRequest.getTcId())) {
            throw new UndefinedTestCaseIdException();
        }
    }
}
