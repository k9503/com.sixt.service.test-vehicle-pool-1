package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.domain.InvalidAcrissCodeException;
import com.sixt.service.test_vehicle_pool.domain.InvalidBranchIdException;
import com.sixt.service.test_vehicle_pool.domain.OutOfTestVehicleException;
import com.sixt.service.test_vehicle_pool.domain.UndefinedTestCaseIdException;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;
import com.sixt.testdatamanagement.rac.vehicle.creation.dto.AcrissCode;

import static com.sixt.service.test_vehicle_pool.api.GetRacVehicleFromPoolResponse.Error.*;

@Component
@RpcHandler("TestVehiclePool.GetRacVehicleFromPool")
public class GetRacVehicleFromPoolHandler extends ExceptionMappingHandler
        implements ServiceMethodHandler<GetRacVehicleFromPoolRequest, GetRacVehicleFromPoolResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetRacVehicleFromPoolHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Autowired
    public GetRacVehicleFromPoolHandler() {
        // Mapping proto exception to rpc exception
        register(InvalidBranchIdException.class, RpcCallException.Category.BadRequest, INVALID_BRANCH_ID.name());
        register(InvalidAcrissCodeException.class, RpcCallException.Category.BadRequest, INVALID_ACRISS_CODE.name());
        register(UndefinedTestCaseIdException.class, RpcCallException.Category.BadRequest, UNDEFINED_TCID.name());
        register(OutOfTestVehicleException.class, RpcCallException.Category.ResourceNotFound, OUT_OF_TEST_VEHICLES.name());
    }

    @Override
    public GetRacVehicleFromPoolResponse handleRequest(GetRacVehicleFromPoolRequest request, OrangeContext orangeContext) throws RpcCallException {

        LOGGER.info("Handling GetRacVehicleFromPool");

        try {
            validate(request);
        } catch (Exception e) {
            LOGGER.error("Validation of GetRacVehicleFromPool request failed: {}", e.getMessage());
            throw mapException(e);
        }

        TestVehicle testVehicle;
        try {
            testVehicle = service.getVehicleFromPool(VehicleType.RAC_AUTOMATION, request.getBranchId(), AcrissCode.valueOf(request.getAcrissCode()),
                    request.getTcId());
        } catch (Exception e) {
            LOGGER.error("Failed getting test vehicle: {}", e.getMessage());
            throw mapException(e);
        }

        if (testVehicle == null) {
            throw new RpcCallException(RpcCallException.Category.InternalServerError, "Test vehicle is null");
        }

        LOGGER.info("Got RAC vehicle {} from pool", testVehicle.getStaticVehicleData().getVehicleId());

        return GetRacVehicleFromPoolResponse.newBuilder()
                .setVehicleId(testVehicle.getStaticVehicleData().getVehicleId())
                .setVin(testVehicle.getStaticVehicleData().getBasic().getVin())
                .setInternalNumber(testVehicle.getStaticVehicleData().getBasic().getInternalNumber())
                .build();
    }

    private void validate(GetRacVehicleFromPoolRequest getRacVehicleFromPoolRequest) throws
            InvalidBranchIdException,
            InvalidAcrissCodeException,
            UndefinedTestCaseIdException,
            RpcCallException {
        if (getRacVehicleFromPoolRequest == null) {
            throw new RpcCallException(RpcCallException.Category.BadRequest, "Invalid request: null.").withErrorCode(BAD_REQUEST.name());
        }
        if (getRacVehicleFromPoolRequest.getBranchId() == 0) {
            throw new InvalidBranchIdException();
        }
        if (getRacVehicleFromPoolRequest.getAcrissCode() == null || !EnumUtils.isValidEnum(AcrissCode.class, getRacVehicleFromPoolRequest.getAcrissCode())) {
            throw new InvalidAcrissCodeException();
        }
        if (StringUtils.isBlank(getRacVehicleFromPoolRequest.getTcId())) {
            throw new UndefinedTestCaseIdException();
        }
    }
}
