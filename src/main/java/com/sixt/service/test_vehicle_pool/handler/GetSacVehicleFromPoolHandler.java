package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import com.sixt.service.test_vehicle_pool.application.dto.TestVehicle;
import com.sixt.service.test_vehicle_pool.domain.OutOfTestVehicleException;
import com.sixt.service.test_vehicle_pool.domain.UndefinedTenantException;
import com.sixt.service.test_vehicle_pool.domain.UndefinedTestCaseIdException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

import static com.sixt.service.test_vehicle_pool.api.GetSacVehicleFromPoolResponse.Error.*;

@Component
@RpcHandler("TestVehiclePool.GetSacVehicleFromPool")
public class GetSacVehicleFromPoolHandler extends ExceptionMappingHandler
        implements ServiceMethodHandler<GetSacVehicleFromPoolRequest, GetSacVehicleFromPoolResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetSacVehicleFromPoolHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Autowired
    public GetSacVehicleFromPoolHandler() {
        // Mapping proto exception to rpc exception
        register(UndefinedTestCaseIdException.class, RpcCallException.Category.BadRequest, UNDEFINED_TCID.name());
        register(OutOfTestVehicleException.class, RpcCallException.Category.ResourceNotFound, OUT_OF_TEST_VEHICLES.name());
        register(UndefinedTenantException.class, RpcCallException.Category.BadRequest, UNDEFINED_TENANT.name());
    }

    @Override
    public GetSacVehicleFromPoolResponse handleRequest(GetSacVehicleFromPoolRequest request, OrangeContext orangeContext) throws RpcCallException {

        LOGGER.info("Handling GetSacVehicleFromPool");

        try {
            validate(request);
        } catch (Exception e) {
            LOGGER.error("Validation of GetSacVehicleFromPool request failed: {}", e.getMessage());
            throw mapException(e);
        }

        TestVehicle testVehicle;
        try {
            testVehicle = service.getVehicleFromPool(VehicleType.SAC_AUTOMATION, request.getTcId(), request.getTenant());
        } catch (Exception e) {
            LOGGER.error("Failed getting test vehicle: {}", e.getMessage());
            throw mapException(e);
        }

        if (testVehicle == null) {
            throw new RpcCallException(RpcCallException.Category.InternalServerError, "Test vehicle is null");
        }

        LOGGER.info("Got SAC vehicle {} from pool", testVehicle.getStaticVehicleData().getVehicleId());

        long availableVehiclesInPool = service.getNumAvailableVehiclesInPool(testVehicle.getVehiclePoolId());
        LOGGER.info("There are {} available vehicles in the pool {}", availableVehiclesInPool, testVehicle.getVehiclePoolId());

        return GetSacVehicleFromPoolResponse.newBuilder()
                .setVehicleId(testVehicle.getStaticVehicleData().getVehicleId())
                .setVin(testVehicle.getStaticVehicleData().getBasic().getVin())
                .build();
    }

    private void validate(GetSacVehicleFromPoolRequest getSacVehicleFromPoolRequest) throws
            UndefinedTestCaseIdException,
            UndefinedTenantException,
            RpcCallException {
        if (getSacVehicleFromPoolRequest == null) {
            throw new RpcCallException(RpcCallException.Category.BadRequest, "Invalid request: null.").withErrorCode(BAD_REQUEST.name());
        }
        if (StringUtils.isBlank(getSacVehicleFromPoolRequest.getTcId())) {
            throw new UndefinedTestCaseIdException();
        }
        if (getSacVehicleFromPoolRequest.getTenant() == Tenant.INVALID_TENANT || getSacVehicleFromPoolRequest.getTenant() == Tenant.UNRECOGNIZED) {
            throw new UndefinedTenantException();
        }
    }

}
