package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

import static com.sixt.service.test_vehicle_pool.api.GetVehiclePoolResponse.Error.INTERNAL_ERROR;
import static com.sixt.service.test_vehicle_pool.api.GetVehiclePoolResponse.Error.NOT_FOUND;

@Component
@RpcHandler("TestVehiclePool.GetVehiclePool")
public class GetVehiclePoolHandler implements ServiceMethodHandler<GetVehiclePoolRequest,
        GetVehiclePoolResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetVehiclePoolHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Override
    public GetVehiclePoolResponse handleRequest(GetVehiclePoolRequest request, OrangeContext orangeContext) throws RpcCallException {

        LOGGER.info("Handling GetVehiclePool");

        VehiclePool vehiclePool;

        try {
            vehiclePool = service.getVehiclePool(request.getVehiclePoolId());
        } catch (Exception e) {
            LOGGER.error("Failed to get vehicle pool with id {}: {}", request.getVehiclePoolId(), e.getMessage());
            throw new RpcCallException(RpcCallException.Category.ResourceNotFound, "Get vehicle pool failed").
                    withErrorCode(INTERNAL_ERROR.name());
        }

        if (vehiclePool == null) {
            LOGGER.error("Vehicle pool with id {} not found", request.getVehiclePoolId());
            throw new RpcCallException(RpcCallException.Category.ResourceNotFound, "Vehicle pool not found")
                    .withErrorCode(NOT_FOUND.name());
        }

        LOGGER.info("Got vehicle pool with id {}", vehiclePool.getId());

        return GetVehiclePoolResponse.newBuilder()
                .setVehiclePool(vehiclePool)
                .build();
    }

}
