package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.test_vehicle_pool.api.*;
import com.sixt.service.test_vehicle_pool.application.TestVehiclePoolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

import static com.sixt.service.test_vehicle_pool.api.UpdateVehiclePoolResponse.Error.UPDATE_FAILED;

@Component
@RpcHandler("TestVehiclePool.UpdateVehiclePool")
public class UpdateVehiclePoolHandler implements ServiceMethodHandler<UpdateVehiclePoolRequest,
        UpdateVehiclePoolResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateVehiclePoolHandler.class);

    @Autowired
    private TestVehiclePoolService service;

    @Override
    public UpdateVehiclePoolResponse handleRequest(UpdateVehiclePoolRequest request, OrangeContext orangeContext) throws RpcCallException {

        LOGGER.info("Handling UpdateVehiclePool");

        String vehiclePoolId;
        CentralPosition centralPosition = request.getVehiclePool().getCentralPosition();

        // Verify if valid central position is provided for vehicle type SAC_AUTOMATION
        if (request.getVehiclePool().getVehicleType() == VehicleType.SAC_AUTOMATION) {
            if (centralPosition == null) {
                LOGGER.error("Central position has to be provided for vehicle type SAC_AUTOMATION");
                throw new RpcCallException(RpcCallException.Category.BadRequest, "Central point has to be provided for vehicle type SAC_AUTOMATION");
            } else if (centralPosition.getLat() == 0.0F || centralPosition.getLon() == 0.0F) {
                LOGGER.error("Central position lat / long has to be provided for vehicle type SAC_AUTOMATION");
                throw new RpcCallException(RpcCallException.Category.BadRequest, "Central point lat / long must be set");
            }
        }

        try {
            vehiclePoolId = service.updateVehiclePool(request.getVehiclePool());
        } catch (Exception e) {
            LOGGER.error("Failed to update vehicle pool: {}", e.getMessage());
            throw new RpcCallException(RpcCallException.Category.BackendError, "Test vehicle pool update failed")
                    .withErrorCode(UPDATE_FAILED.name());
        }

        if (vehiclePoolId == null) {
            LOGGER.error("Failed to update vehicle pool");
            throw new RpcCallException(RpcCallException.Category.ResourceNotFound, "Test vehicle pool to be updated not found")
                    .withErrorCode(UPDATE_FAILED.name());
        }

        LOGGER.info("Successfully updated vehicle pool with id {}", vehiclePoolId);

        VehiclePool updatedVehiclePool = service.getVehiclePool(vehiclePoolId);

        if (updatedVehiclePool == null) {
            LOGGER.error("Failed to get updated vehicle pool");
            throw new RpcCallException(RpcCallException.Category.BackendError, "Could not get updated vehicle pool");
        }

        return UpdateVehiclePoolResponse.newBuilder()
                .setVehiclePool(updatedVehiclePool)
                .build();
    }

}
