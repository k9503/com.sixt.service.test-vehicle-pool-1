package com.sixt.service.test_vehicle_pool.handler;

import com.sixt.service.fleet.api.VehicleV2;
import com.sixt.service.test_vehicle_pool.api.AddTelematicManRacVehicleRequest;
import com.sixt.service.test_vehicle_pool.api.AddTelematicManRacVehicleResponse;
import com.sixt.service.test_vehicle_pool.domain.vehiclecreation.CommonRacVehicleCreator;
import com.sixt.service.test_vehicle_pool.infrastructure.FleetServiceProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.OrangeContext;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.RpcCallException;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.ServiceMethodHandler;
import spring.boot.starter.sixtgoorange.copiedfromjamicro.webservice.RpcHandler;

import static com.sixt.service.test_vehicle_pool.api.AddTelematicManRacVehicleResponse.Error.INTERNAL_ERROR;
import static com.sixt.service.test_vehicle_pool.api.AddTelematicManRacVehicleResponse.Error.VEHICLE_NOT_FOUND;
import static net.logstash.logback.marker.Markers.append;

@Component
@RpcHandler("TestVehiclePool.AddTelematicManRacVehicle")
public class AddTelematicManRacVehicleHandler implements ServiceMethodHandler<AddTelematicManRacVehicleRequest, AddTelematicManRacVehicleResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddTelematicManRacVehicleHandler.class);

    @Autowired
    private CommonRacVehicleCreator commonRacVehicleCreator;
    @Autowired
    private FleetServiceProxy fleetServiceProxy;

    @Override
    public AddTelematicManRacVehicleResponse handleRequest(AddTelematicManRacVehicleRequest request, OrangeContext orangeContext) throws RpcCallException {

        String vehicleId = request.getVehicleId();
        LOGGER.info(append("vehicleId", vehicleId), "Handling AddTelematicManRacVehicle");

        VehicleV2 vehicleV2 = fleetServiceProxy.getByVehicleIdV2(vehicleId);
        if (vehicleV2 == null) {
            LOGGER.error(append("vehicleId", request.getVehicleId()), "Telematic unit could not successfully be added as vehicle was not found");
            throw new RpcCallException(RpcCallException.Category.ResourceNotFound, "Adding telematic unit failed")
                    .withErrorCode(VEHICLE_NOT_FOUND.name());
        }

        String vin = vehicleV2.getBasic().getVin();
        try {
            commonRacVehicleCreator.addTelematicUnit(vehicleId, vin);
        } catch (Exception e) {
            LOGGER.error(append("vehicleId", request.getVehicleId()), "Failed adding telematic unit: {}", e.getMessage());
            throw new RpcCallException(RpcCallException.Category.InternalServerError, "Adding telematic unit failed")
                    .withErrorCode(INTERNAL_ERROR.name());
        }
        LOGGER.info(append("vehicleId", request.getVehicleId()), "Successfully added telematic unit");

        return AddTelematicManRacVehicleResponse.getDefaultInstance();
    }

}
