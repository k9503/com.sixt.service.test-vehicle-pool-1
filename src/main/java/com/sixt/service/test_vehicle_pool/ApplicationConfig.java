package com.sixt.service.test_vehicle_pool;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import okhttp3.OkHttpClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
@EnableJpaRepositories(basePackages = "com.sixt.service.test_vehicle_pool")
@EnableRetry
@EnableScheduling
public class ApplicationConfig {

    @Bean
    public OkHttpClient okHttpClient() {
        return new OkHttpClient();
    }

    @Bean
    public ExecutorService executor() {
        return Executors.newFixedThreadPool(5,
                new ThreadFactoryBuilder().setNameFormat("async-executor-%d").build());
    }

    @Bean
    public TaskScheduler threadPoolTaskScheduler() {
        return new ConcurrentTaskScheduler(Executors.newScheduledThreadPool(
                80, new ThreadFactoryBuilder().setNameFormat("scheduler-thread-%d").build()));
    }

}
